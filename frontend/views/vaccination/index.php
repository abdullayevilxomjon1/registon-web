<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VaccinationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vaccinations';
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="add-balance-area pd-top-40">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vaccination', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vilsoato',
            'tumsoato',
            'uchid',
            'vid',
            //'vresid',
            //'vvilid',
            //'vtumid',
            //'animalid',
            //'animalinfo:ntext',
            //'animalbirka',
            //'vac_all',
            //'owner_type',
            //'owner_pinfl',
            //'owner_tin',
            //'status',
            //'owner_info:ntext',
            //'phone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>