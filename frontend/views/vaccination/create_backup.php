<?php
use yii\helpers\Html;
use app\models\Profiles;
use app\models\Vuch;
use app\models\Vinfo;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccination */

$this->title = 'Вакцинация';
$this->params['breadcrumbs'][] = ['label' => 'Vaccinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$profiles = Profiles::findOne(Yii::$app->user->identity->id);

// Список вакцин		
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 2])->all();

?>
 <div class="add-balance-area pd-top-40">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
	
	<div id="owner_type"><br/>
		<select id="owner_type_select" class="form-control custom-select" style="height: 35px;">
			<option value="">Мулкчилик шаклини танланг</option>
			<option value="fiz">Жисмоний шахс</option>
			<option value="yur">Юридик шахс</option>
		</select>
	</div>
	
	<div id="fiz" style="display:none;"><br/>
	<input type="text" id="pinfl" class="form-control" placeholder="ЖШШИР"><br/>
	<input type="text" id="serial" class="form-control" placeholder="паспорт серияси ва рақами">
	<input type="hidden" id="owner" class="form-control">
	</div>
	
	<div id="yur" style="display:none;"><br/>
	<input type="text" id="TIN" class="form-control" placeholder="СТИР"><br/>	
	<input type="hidden" id="owner" class="form-control">
	</div>
	<div id='linktext'>
	</div>
</div>
</div>

<?php 
$this->registerJs(<<<JS
	
	$('#owner_type_select').on('change', function() { 		
	    if(this.value == 'fiz'){
			$("#fiz").show();	
			$("#yur").hide();	
			document.getElementById("linktext").innerHTML = "";
		}
		
		if(this.value == 'yur'){
			$("#yur").show();	
			$("#fiz").hide();	
			document.getElementById("linktext").innerHTML = "";
		}
		
		if(this.value == ''){
			$("#yur").hide();	
			$("#fiz").hide();	
			document.getElementById("linktext").innerHTML = "";
		}
		
	});
	
	$('#serial').on('change', function() { 
		var pinfl = document.getElementById("pinfl").value;
		var serial = document.getElementById("serial").value;	 
		if(pinfl.length == 14 && serial.length == 9){			
			checkPinfl();	
		}		
	});
	$('#pinfl').on('change', function() { 		
		var pinfl = document.getElementById("pinfl").value;
		var serial = document.getElementById("serial").value;	 		
		if(pinfl.length == 14 && serial.length == 9){			
			checkPinfl();	
		}		
	});
	
	// check pinfl
	function checkPinfl(){
		$(".preloader").show();
		var pinfl = document.getElementById("pinfl");
		var serial = document.getElementById("serial");	 	
		$.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){			
			if(status == 'success'){
				if(data == ''){
					$(".preloader").hide();	
					document.getElementById("linktext").innerHTML = "<br/>маълумот топилмади";	
				}
				else{					
					var jsondata = JSON.parse(data);		
					$("#owner").val(jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin);
					var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;
					var url = "/vaccination/process?pinfl=" + pinfl.value + "&serial=" + serial.value;
					document.getElementById("linktext").innerHTML = "<br/><a href='" + url + "' class='btn btn-purple' style='width:100%;'>" + text + "</>";						
					$(".preloader").hide();	
				}
			}
			else{
				$(".preloader").hide();
			}				
        });	
	}
	
	$('#TIN').on('change', function() { 
		var tin = document.getElementById("TIN");		
	 		  
		$.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data){				 	
            $("#vvil-driver").val(data);
        });
	});
	
	$('#vvil-carnumber').on('change', function() { 
		var cartype = document.getElementById("vvil-cartype");
		var carnumber = document.getElementById("vvil-carnumber");
            $("#vvil-car").val(cartype.value + ", " + carnumber.value);            
	});
	
	function wait(ms){
	   var start = new Date().getTime();
	   var end = start;
	   while(end < start + ms) {
		 end = new Date().getTime();
	  }
	}
JS
);
?>
