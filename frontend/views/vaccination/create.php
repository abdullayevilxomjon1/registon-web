<?php
use yii\helpers\Html;
use app\models\Profiles;
use app\models\Vuch;
use app\models\Vinfo;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccination */

$this->title = 'Вакцинация';
$this->params['breadcrumbs'][] = ['label' => 'Vaccinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$profiles = Profiles::findOne(Yii::$app->user->identity->id);

// Список вакцин		
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 2])->all();

?>
 <div class="add-balance-area pd-top-40">
        <div class="container">
    <h1><?= Html::encode($this->title) ?></h1>
    <a href="/address/" class="btn btn-success" style='width:100%;'>Жисмоний шахслар</a><br/><br/>
	<a href="/address/jur/" class="btn btn-success" style='width:100%;'>Юридик шахслар</a>
</div>
</div>

