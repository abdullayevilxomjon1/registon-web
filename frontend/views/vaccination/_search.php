<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VaccinationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vaccination-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vilsoato') ?>

    <?= $form->field($model, 'tumsoato') ?>

    <?= $form->field($model, 'uchid') ?>

    <?= $form->field($model, 'vid') ?>

    <?php // echo $form->field($model, 'vresid') ?>

    <?php // echo $form->field($model, 'vvilid') ?>

    <?php // echo $form->field($model, 'vtumid') ?>

    <?php // echo $form->field($model, 'animalid') ?>

    <?php // echo $form->field($model, 'animalinfo') ?>

    <?php // echo $form->field($model, 'animalbirka') ?>

    <?php // echo $form->field($model, 'vac_all') ?>

    <?php // echo $form->field($model, 'owner_type') ?>

    <?php // echo $form->field($model, 'owner_pinfl') ?>

    <?php // echo $form->field($model, 'owner_tin') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'owner_info') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
