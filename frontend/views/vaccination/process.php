<?php
use yii\helpers\Html;
use app\models\Profiles;
use app\models\Vuch;
use app\models\Vinfo;

$this->title = 'Вакцинация';
$this->params['breadcrumbs'][] = ['label' => 'Vaccinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$profiles = Profiles::findOne(Yii::$app->user->identity->id);

// Список вакцин		
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 2])->all();
$pinfl =  Yii::$app->request->get('pinfl'); 
$serial =  Yii::$app->request->get('serial'); 
$ownerdata = file_get_contents('https://m.registon.vetgov.uz/reference/getfizdata?pinfl='.$pinfl.'&serial'.$serial);

?>
 <div class="add-balance-area pd-top-40">
        <div class="container">
    <h1><?= Html::encode($this->title) ?></h1>
	<a href="/animals/create?pinfl=<?=$pinfl?>&serial=<?= $serial; ?>" class="btn btn-purple">+ Ҳайвон киритиш</a><br/>
	<br/>
	<select class="form-control custom-select" style="height: 35px;" id="vaccine">
	<option>Вакцинани танланг</option>
	<?php foreach($vuch as $val): ?>
		<?php $vinfo = Vinfo::findOne($val->vresp['vac_doz']); ?>		
		<option value="<?= $val->vid; ?>" ><?= $val->vaccine['name']; ?> [ <?= $val->vaccine['code']; ?> ]- <?= $val->vac_all; ?> та <?= $vinfo->containern['name_uz'] ?>(<?= $vinfo->quantity ?> <?= $vinfo->unitn['name_uz'] ?>)</option>
	<?php endforeach; ?>
	</select>	
	<div id='datacontent'>
	
	</div>
</div>
</div>

<?php 
$this->registerJs(<<<JS
	
	$('#vaccine').on('change', function() {				
		if(this.value > 0){						
			$.get("/vaccination/getanimallist?vaccine="+this.value+"&pinfl={$pinfl}&serial={$serial}", function(data){					
				document.getElementById("datacontent").innerHTML = data;						
			});	
		}
		else{
			document.getElementById("datacontent").innerHTML = "";			
		}
	    		
	});
	
	$('#vaccine').on('click', function() {						
	    		
	});
	
JS
);
?>
