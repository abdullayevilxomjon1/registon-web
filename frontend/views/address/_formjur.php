<?php
use yii\helpers\Html;
use yii\helpers\Arrayhelper;
use app\models\Addresstype;
use app\models\Address;
use yii\widgets\ActiveForm;
if(!isset($topmodel->id)){
	$tpid = 0;
	$tptype = 0;
}
else{
	$tpid = $topmodel->id;
	$tptype = $topmodel->type;
}

if($model->id > 0){
	$tpid = $model->top;
}
?>

<div class="address-form">	
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'top')->hiddenInput(['default' => $tpid])->Label(false) ?>
    <?= $form->field($model, 'status')->hiddenInput()->Label(false) ?>
	<?= $form->field($model, 'soato')->hiddenInput()->Label(false) ?>
    <?= $form->field($model, 'user')->hiddenInput()->Label(false) ?>
    <?= $form->field($model, 'type')->hiddenInput(['value' => 16])->Label(false) ?>	
	
	<?= $form->field($model, 'tin')->textInput(['maxlength' => true]) ?>
	<div id="linktext"></div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
	
    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$this->registerJs(<<<JS
	$('#address-tin').on('change', function() { 		
		var tin = this.value;			
		if(tin.length == 9){			
			checkTin();	
		}
		else{
			document.getElementById("linktext").innerHTML = "Стир рақами нотўғри киритилган";	
		}		
	});
	
	// check Tin
	function checkTin(){
		$(".preloader").show();
		var tin = this.value;		 	
		$.get("/reference/getjurdata?inn="+tin+"", function(data, status){			
			if(status == 'success'){
				if(data == ''){
					$(".preloader").hide();	
					document.getElementById("linktext").innerHTML = "<br/>маълумот топилмади";	
				}
				else{					
					var jsondata = JSON.parse(data);		
					$("#owner").val(jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin);
					var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;
					document.getElementById("linktext").innerHTML = "";	
					document.getElementById("btn").innerHTML = "<br/><button class='btn btn-purple' style='width:100%;'>" + text + "</>";						
					document.getElementById("ownersfiz-data").value = data;						
					document.getElementById("ownersfiz-full_name").value = text;						
					$(".preloader").hide();	
				}
			}
			else{
				$(".preloader").hide();
			}				
        });	
	}	

JS
);
?>