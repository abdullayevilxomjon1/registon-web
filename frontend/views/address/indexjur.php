<?php

use yii\helpers\Html;
use yii\grid\GridView;

if(isset($topmodel->id)){
	$this->title = 'Манзиллар: '.$topmodel->name;
}
else{
	$this->title = 'Юридик шахслар манзиллари';	
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="balance-area">
        <div class="container">

    <h1><?= $this->title ?></h1>
	<div>
    <div class="row">
		<div class="col-7">
			<?= Html::a('Манзилни киритиш', ['createjur'], ['class' => 'btn btn-success']) ?>
		</div>
		<div class="col-5">			
			<?= Html::a('Бош саҳифа', ['/'], ['class' => 'btn btn-danger']) ?>
		</div>        
    </div>
	</div>
	

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,        
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',            
			[
				'attribute'=>'type',
				'label' => Yii::t('app', 'Тури'),
				'filter' => false,
				'content'=>function($data){					
					return $data->atype['name'];
				},
			],
			[
				'attribute'=>'name',
				'label' => Yii::t('app', 'Номи'),
				//'filter' => false,
				'content'=>function($data){	
					if($data->type == 5){
						return "<a href='/address/view?id=".$data->id."'><b>".$data->name."</b></a>";
					}
					else{
						return "<a href='/address?top=".$data->id."'><b>".$data->name."</b></a>";	
					}
					
				},
			],
            //'top',
            //'status',
            //'soato',            			
			[
				'attribute'=>'user',
				'label' => Yii::t('app', 'Киритди'),
				'filter' => false,
				'content'=>function($data){					
					return $data->author['full_name']."<br/>".$data->created;
				},
			],
            //'updated',
        ],
    ]); ?>


</div></div>
