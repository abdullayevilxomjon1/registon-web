<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
if(isset($topmodel->id)){	
	$this->title = $topmodel->name."да жойлашган манзилларни киритиш";
	$tpmid = $topmodel->type;
}
else{
	$this->title = 'Янги манзил қўшиш';	
	$tpmid = 0;
}


$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
 <div class="add-balance-area pd-top-40">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1><br/>

    <?= $this->render('_form', [
        'model' => $model,
        'topmodel' => $topmodel,
    ]) ?>

</div>
</div>
