<?php
use yii\helpers\Html;
use yii\helpers\Arrayhelper;
use app\models\Addresstype;
use app\models\Address;
use yii\widgets\ActiveForm;
if(!isset($topmodel->id)){
	$tpid = 0;
	$tptype = 0;
}
else{
	$tpid = $topmodel->id;
	$tptype = $topmodel->type;
}

if($model->id > 0){
	$tpid = $model->top;
}
?>

<div class="address-form">	
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'top')->hiddenInput(['default' => $tpid])->Label(false) ?>
    <?= $form->field($model, 'status')->hiddenInput()->Label(false) ?>
	<?= $form->field($model, 'soato')->hiddenInput()->Label(false) ?>
    <?= $form->field($model, 'user')->hiddenInput()->Label(false) ?>
	
	<?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(Addresstype::find()->where(['>', 'id', $tptype])->andWhere(['!=', 'id', 16])->all(), 'id', 'name'), 
             ['prompt'=>'Турини танланг']); ?>	

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
