<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Profiles;
use app\models\Vuch;
use app\models\Vinfo;
use app\models\Vacemlash;
use app\models\Vaccination;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Address */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 2])->all();
\yii\web\YiiAsset::register($this);
$vuchvac = Vuch::findOne($vid);
$vacid = $vuchvac->vid;
// Хайвон эгаси
$owner = $model->owner['data']; 
?> 
<?php if($model->type == 15): ?>
<div class="main" style="width:100%; height:100%; position:fixed; z-index:1000; background-color:#fff; top:0;"><center style="padding-top:150px; height:100%;" onclick="getLocation();"><br/><img src="/assets/img/icon/gps.gif"><br/><br/><b>Илтимос!</b><br/>GPSни ёқинг<br/></center></div>
<div class="balance-area">
        <div class="container">
	<?= Html::a('Орқага <i class="fa fa-arrow-left"></i>', ['index', 'top' => $model->top], ['class' => 'btn btn-danger']) ?>	
    <h1><?= Html::encode($this->title) ?></h1>

	<table class="table table-striped table-bordered detail-view">
	<tr>
	<td><b>Тури</b></td>
	<td><?= $model->atype['name']?></td>
	</tr>
	<tr>
	<td><b>Манзил номи</b></td>
	<td><?= $model->name; ?></td>
	</tr>
	<tr>
	<td><b>Киритилган сана</b></td>
	<td><?= $model->created; ?></td>
	</tr>
	<?php if($model->updated != "0000-00-00 00:00:00"):?>
	<tr>
	<td><b>Янгиланган сана</b></td>
	<td><?= $model->updated; ?></td>
	</tr>
	<?php endif; ?>
	<tr>
	<td><b>Эгаси</b></td>
	<td><?= $model->owner['full_name']; ?></td>
	</tr> 
	<tr>
	<td><b>Жойлашган жойи</b></td>
	<td>lat: <?= $model->location['latitude']; ?>,<br/>long: <?= $model->location['longitude']; ?><br/>
	<a href="https://www.google.com/maps/search/?api=1&query=<?= $model->location['latitude']; ?>,<?= $model->location['longitude']; ?>" target="_blank">Картада кўриш</a>
	</td>
	</tr>
	</table>
	<br/>
	<select class="form-control custom-select" style="height: 35px;" id="vaccine">
	<option>Вакцинани танланг</option>
	<?php foreach($vuch as $val): ?>
		<?php $vinfo = Vinfo::findOne($val->vresp['vac_doz']); ?>		
		<option value="<?= $val->id; ?>" <?php if($val->id == $vid){ echo "selected='selected'";}?>><?= $val->vaccine['name']; ?> [ <?= $val->vaccine['code']; ?> ]- <?= $val->vac_all; ?> та <?= $vinfo->containern['name_uz'] ?>(<?= $vinfo->quantity ?> <?= $vinfo->unitn['name_uz'] ?>)</option>
	<?php endforeach; ?>
	</select>	
	<br/>
	<br/>
	    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',           
            //'pinfl',
            //'tin',         
			[
				'attribute'=>'color',
				'label' => Yii::t('app', 'Ҳайвон тури'),				
				'content'=>function($data){
					switch($data->sex){
						case 0:
						$sex = "Урғочи";
						break;
						case 1:
						$sex = "Эркак";
						break;
					}
					switch($data->sex){
						default:
						return $data->animaltype['NAME_UZ'].", ".$data->animalbreed['NAME_UZ']."<br/>(Бошқа рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 1:
						return $data->animaltype['NAME_UZ'].",<br/> ".$data->animalbreed['NAME_UZ']."<br/>(Қора-ола рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 2:
						return $data->animaltype['NAME_UZ'].", ".$data->animalbreed['NAME_UZ']."<br/>(Қизил-ола рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 3:
						return $data->animaltype['NAME_UZ']." ".$data->animalbreed['NAME_UZ']."<br/>(Қизил рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 4:
						return $data->animaltype['NAME_UZ']." ".$data->animalbreed['NAME_UZ']."<br/>(Қора рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 5:
						return $data->animaltype['NAME_UZ']." ".$data->animalbreed['NAME_UZ']."<br/>(Қўнғир рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 6:
						return $data->animaltype['NAME_UZ']." ".$data->animalbreed['NAME_UZ']."<br/>(Кулранг рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						case 7:
						return $data->animaltype['NAME_UZ']." ".$data->animalbreed['NAME_UZ']."<br/>(Оқ-кулранг рангли)<br/>".$data->klichka."<br/>".$sex;
						break;
						
					}
				},
			],           			
            //'ns10_code',
            //'ns_11_code',            			 
			[
				'attribute'=>'birth',
				'label' => Yii::t('app', 'эмланган санаси'),				
				'content'=>function($data)use($vacid, $profiles, $vid, $model){
					$vaccination = Vaccination::find()->where(['animalid' => $data->id])->andWhere(['vid' => $vacid])->One();
                    if(isset($vaccination->time)){
						return $vaccination->time;
					}
					else{
						return "Эмланмаган";
					}						
				},
			],  
			[
				'attribute'=>'birth',
				'label' => Yii::t('app', 'Вакцинация'),				
				'content'=>function($data)use($vacid, $profiles, $vid, $model){	
					$vaccination = Vaccination::find()->where(['animalid' => $data->id])->andWhere(['vid' => $vacid])->One();
                    if(isset($vaccination->time)){
						$date = new DateTime($data->birth);
						$now = new DateTime();
						$interval = $now->diff($date);
						$months = $interval->y*12+$interval->m;												
						$vaccine = Vacemlash::find()->where(['vid' => $vacid])->andWhere(['<=', 'ege_from', $months])->andWhere(['>=', 'ege_to', $months])->andWhere(['animal' => $data->atype])->All();												
						
						if($months > 0){
							$vl = "Ёши ".$months." ой<br/>";							
						}
						else{
							$vl = $interval->d." кун<br/>";							
						}
					}
					else{
				
					$date = new DateTime($data->birth);
                    $now = new DateTime();
					$jsondata = json_encode($data, true);
					if($now > $date){
						$interval = $now->diff($date);
						$months = $interval->y*12+$interval->m;												
						$vaccine = Vacemlash::find()->where(['vid' => $vacid])->andWhere(['<=', 'ege_from', $months])->andWhere(['>=', 'ege_to', $months])->andWhere(['animal' => $data->atype])->All();												
						
						if($months > 0){
							$vl = "Ёши ".$months." ой<br/>";							
						}
						else{
							$vl = $interval->d." кун<br/>";							
						}
						$n = 0;
						foreach($vaccine as $val){	
							if($val->unitname['id'] == 1){
									$qt = $val->quantity*2;									
									$vl .= 'Эмлашга тавсия: '.$val->quantity.' '.$val->unitname['name_uz'].' ёки '.$qt.' мл <br/>'."<a href='/vaccination/emlash?aid=".$data->id."&vid=".$vid."&vacemlash=".$val->id."&addid=".$model->id."' class='btn btn-success'>Эмлаш</a><br/>";								
							}
							else{
									$vl .= 'Эмлашга тавсия: '.$val->quantity.' '.$val->unitname['name_uz'].'<br/>'."<a href='/vaccination/emlash?aid=".$data->id."&vid=".$vid."&vacemlash=".$val->id."&addid=".$model->id."' class='btn btn-success'>Эмлаш</a><br/>";								
							}						
							$n++;												
						}
						
									
						if($n == 0){
							$vl .= "Эмлашга тавсия этилмайди";
						}
						return $vl;
					}
					else{
						return '-';
					}
				}				
				},
			],			
            //'created',
            //'updated',
            //'user',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> 
</div>
</div>
<?php endif; ?>
<?php 

$this->registerJs(<<<JS
function getLocation(){	
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPositionw);
	} else { 
		x.innerHTML = "Сизда GPS маълумотларини аниқлаштириш имкони мавжуд эмас";		
	}
}
 

function showPositionw(position) {
  lat = position.coords.latitude;
  lng = position.coords.longitude;
  if(lat == null){
    document.write('<center><div class="alert alert-info" role="alert"> Please Turn On Your GPS </div></center>')	
  }
  else{	
	$( "div.main" ).hide();
  }
 
}
getLocation();

$('#vaccine').on('change', function() {				
		if(this.value > 0){												
			document.getElementById("datacontent").innerHTML = window.location.href = "/address/vaccination?id={$model->id}&vaccine="+this.value+"&pinfl={$model->owner['pinfl']}";																		
		}
		else{
			document.getElementById("datacontent").innerHTML = "";			
		}
	    		
	});

JS
);
?>
