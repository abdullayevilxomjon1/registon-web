<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ownersfiz */

$this->title = 'Update Ownersfiz: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ownersfizs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ownersfiz-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
