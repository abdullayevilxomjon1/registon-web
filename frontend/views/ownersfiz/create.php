<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ownersfiz */

$this->title = 'Хонадон эгасини киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Ownersfizs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-balance-area">
<div class="container">

    <?= $this->render('_form', [
        'model' => $model,
        'addid' => $addid,
    ]) ?>

</div>
</div>
<?php 
$this->registerJs(<<<JS
	$('#ownersfiz-serial').on('change', function() { 		
		var pinfl = document.getElementById("ownersfiz-pinfl").value;
		var serial = document.getElementById("ownersfiz-serial").value;	 
		if(pinfl.length == 14 && serial.length == 9){			
			checkPinfl();	
		}		
	});
	
	$('#ownersfiz-pinfl').on('change', function() { 				
		var pinfl = document.getElementById("ownersfiz-pinfl").value;
		var serial = document.getElementById("ownersfiz-serial").value;	 		
		if(pinfl.length == 14 && serial.length == 9){			
			checkPinfl();	
		}		
	});
	
	// check pinfl
	function checkPinfl(){
		$(".preloader").show();
		var pinfl = document.getElementById("ownersfiz-pinfl");
		var serial = document.getElementById("ownersfiz-serial");	 	
		$.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){			
			if(status == 'success'){
				if(data == ''){
					$(".preloader").hide();	
					document.getElementById("linktext").innerHTML = "<br/>маълумот топилмади";	
				}
				else{					
					var jsondata = JSON.parse(data);		
					$("#owner").val(jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin);
					var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;
					document.getElementById("linktext").innerHTML = "";	
					document.getElementById("btn").innerHTML = "<br/><button class='btn btn-purple' style='width:100%;'>" + text + "</>";						
					document.getElementById("ownersfiz-data").value = data;						
					document.getElementById("ownersfiz-full_name").value = text;						
					$(".preloader").hide();	
				}
			}
			else{
				$(".preloader").hide();
			}				
        });	
	}	

JS
);
?>