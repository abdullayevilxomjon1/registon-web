<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ownersfiz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ownersfiz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'addid')->hiddenInput(['value' => $addid])->label(false) ?>

    <?= $form->field($model, 'pinfl')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'full_name')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'data')->hiddenInput()->label(false) ?>
	
	<div id="linktext"></div>    

    <div class="form-group" id="btn">
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
