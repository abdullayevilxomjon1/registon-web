<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use app\models\Profiles;
use app\models\Vuch;
use app\models\Uchastkanames;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

AppAsset::register($this);

$profiles = Profiles::findOne(Yii::$app->user->identity->id);
if($profiles->uchastka == 0){
	return Yii::$app->getResponse()->redirect("/site/noaccess");
}
elseif($profiles->account_type == 2){
	
}
else{
	return Yii::$app->getResponse()->redirect("/site/noaccess");
}
$uchastka = Uchastkanames::findOne($profiles->uchastka);
$model = new Vuch();
// Список инструкции		
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 1])->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?> 
<?php if(Yii::$app->request->pathInfo == "" OR Yii::$app->request->pathInfo == "index"): ?>
<div class="main" style="width:100%; height:100%; position:fixed; z-index:1000; background-color:#fff;"><center style="padding-top:150px; height:100%;" onclick="getLocation();"><br/><img src="/assets/img/icon/gps.gif"><br/><br/><b>Илтимос!</b><br/>GPSни ёқинг<br/></center></div>
<?php endif; ?>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->

    <!-- header start -->
	
    <div class="header-area" style="background-image: url(/assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <div class="menu-bar">
                        <i class="fa fa-bars"></i>
                    </div>
                </div>
                <div class="col-sm-4 col-4 text-center">
                    <a href="/" class="logo">
                        <img src="/assets/img/logo.png" alt="logo">
                    </a>
                </div>
                <div class="col-sm-4 col-5 text-right">
                    <ul class="header-right">
                        <li>
                            <a href="/messages">
                                <i class="fa fa-envelope"></i>
                                <span class="badge">0</span>
                            </a>
                        </li>
                        <li>
                            <a href="/notifications/">
                                <i class="fa fa-bell animated infinite swing"></i>
                                <span class="badge">0</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- header end -->

    <!-- navbar end -->
    <div class="ba-navbar">
        <div class="ba-navbar-user">
            <div class="menu-close">
                <i class="la la-times"></i>
            </div>            
            <div class="details">
                <h5><?php print $profiles->full_name; ?></h5>                
				<p><?php print $uchastka->vil['Name']; ?></p>
				<p><?php print $uchastka->tum['Name']; ?></p>
				<p><?php print $uchastka->NAME_UZ; ?> участкаси</p>
				
            </div>
        </div>
		<a href="/vaccination/create">
		<div class="ba-add-balance-title style-two">
            <h5>Вакцина ишлатиш</h5>
            <i class="fa fa-plus"></i>
        </div>
		</a>
		<a href="/suspicion/create">
        <div class="ba-add-balance-title style-two">
            <h5>Касалликни киритиш</h5>
            <i class="fa fa-plus"></i>
        </div>
		</a>        
        <div class="ba-main-menu">
            <h5>Меню</h5>
            <ul>
                <li><a href="/address">Манзиллар</a></li>
				<li><a href="/vuch/new">Янги вакциналар</a></li>
                <li><a href="/vuch/recieved">Қабул қилинган вакциналар</a></li>
                <li><a href="/vaccination/spended">Ишлатилган вакциналар</a></li>
                <li><a href="/vaccination/balance">Захирадаги вакциналар </a></li>
                <li><a href="/vuch/tasks">Вазифалар</a></li>
                <li><a href="/vuch/lab">Лабаратория натижалари</a></li>
                <li><a href="#">Созламалар</a></li>
            </ul>
            <?= Html::a('<i class="feather icon-power text-danger"></i> Тизимдан чиқиш', ['site/logout'], ['data' => ['method' => 'POST'], 'class' => 'btn btn-danger']) ?>			
        </div>
    </div>
    <!-- navbar end --> 
    <?php if(Yii::$app->request->pathInfo == "" OR Yii::$app->request->pathInfo == "index"): ?>
    <!-- navbar end -->
    <div class="add-balance-inner-wrap">
        <div class="container">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Вакцинани қабул қилиш</h5>
                    </div>					
                    <div class="modal-body">
                        <div class="action-sheet-content">								
								 <?= $this->render('/site/_form', [
									'model' => $model,
								]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- navbar end -->
	<?php endif; ?>
	

    <!-- balance start -->
    <div class="balance-area mg-top-50">
        <div class="container">
            <div class="balance-area-bg balance-area-bg-home">
                <div class="balance-title text-center" style="padding:15px;">
                    <h6><?= $this->title; ?></h6></br>
                </div>
			</div>
        </div>
    </div>
    <!-- balance End -->
	<?= Alert::widget() ?>
	<?= $content ?><br/>
    <!-- Footer Area -->
    <div class="footer-area">
        <div class="container">
            <div class="footer-bottom text-center">
                <ul>
                    <li>
                        <a href="/">
                            <i class="fa fa-home"></i>
                            <p>Бош саҳифа</p>
                        </a>
                    </li>                    
                    <li>
                        <a class="menu-bar" href="#">
                            <i class="fa fa-bars"></i>
                            <p>Меню</p>
                        </a>
                    </li>
                    <li>
                        <a href="/cards">
                            <i class="fa fa-home"></i>
                            <p>Карталар</p>
                        </a>
                    </li> 
					 <li>
                        <a href="<?= Url::current(); ?>">
                            <i class="fa fa-spinner"></i>
                            <p>Янгилаш</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/assets/js/vendor.js'); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/assets/js/main.js'); ?>

<?php if(Yii::$app->request->pathInfo == "" OR Yii::$app->request->pathInfo == "index"): ?>
<?php 

$this->registerJs(<<<JS
function getLocation(){	
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPositionw);
	} else { 
		x.innerHTML = "Сизда GPS маълумотларини аниқлаштириш имкони мавжуд эмас";		
	}
}


function showPositionw(position) {
  lat = position.coords.latitude;
  lng = position.coords.longitude;
  if(lat == null){
    document.write('<center><div class="alert alert-info" role="alert"> Please Turn On Your GPS </div></center>')	
  }
  else{	
	//alert(lat+'/'+lng);
	$( "div.main" ).hide();
  }
 
}
getLocation();

JS
);
?>
<?php endif; ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>