<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.s7template.com/tf/bankapp/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 08:34:57 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- Stylesheet File -->
    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>
<?php $this->beginBody() ?>
    
    <!-- page-title stary -->
    <div class="page-title">
        <div class="container" style="text-align:center;">
			<img src="http://registon.vetgov.uz/assets/images/registon-logo.png" alt="" width="200">
        </div>
    </div>
    
    <!-- page-title end -->
<br/>
    <!-- singin-area start -->
    <div class="signin-area">
        <div class="container">
            <div class="row contact-form-inner">			
				<div class="col-12">
					<?= Alert::widget() ?>
					<?= $content ?>               
				</div> 				
            </div> 
        </div>
    </div>
    <!-- singin-area End -->

    <!-- Footer Area -->
    <div class="footer-area mg-top-40">
        <div class="footer-top text-center" style="background-image: url(/assets/img/bg/7.png);">
            <div class="container">
                <p>Copyright © Госкомветеринарии РУз. Все права защещены.</p>
            </div>
        </div>        
    </div>

    <!-- All Js File here -->
    <script src="/assets/js/vendor.js"></script>
    <script src="/assets/js/main.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>