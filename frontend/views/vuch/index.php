<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VuchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
 <!-- goal area Start -->
    <div class="goal-area pd-top-36">
        <div class="container">
            <div class="section-title">
                <h3 class="title"><?= $this->title; ?></h3>                
            </div>
			<?php foreach($vuch as $val):?>
			<div class="single-goal <?php if($val->status == 1): ?>single-goal-three<?php elseif($val->status == 2): ?>single-goal-one<?php endif; ?>">
                <div class="row">
                    <div class="col-7 pr-0">
                        <div class="details">
                            <h6><?php echo $val->vaccine['name']; ?> [ <?php echo $val->vaccine['code']; ?> ]</h6>
                            <?php if($val->status == 1): ?><p>Қабул қилинмаган</p><?php else: ?><p>Қабул қилинган санаси: <?= $val->date_recieved; ?></p><?php endif; ?>
                        </div>
                    </div>
                    <div class="col-5 pl-0">
                        <div class="circle-inner circle-inner-one">
                            <h6 class="goal-amount"> <?php echo $val->vac_all; ?> доза</h6>                            
                        </div>
                    </div>
                </div>
            </div>
			<?php endforeach; ?>            
        </div>
    </div>
    <!-- goal area End -->
