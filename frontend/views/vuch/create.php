<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vuch */

$this->title = 'Create Vuch';
$this->params['breadcrumbs'][] = ['label' => 'Vuches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vuch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
