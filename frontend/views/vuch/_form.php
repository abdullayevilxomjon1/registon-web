<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vuch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vuch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vid')->textInput() ?>

    <?= $form->field($model, 'vresid')->textInput() ?>

    <?= $form->field($model, 'vvid')->textInput() ?>

    <?= $form->field($model, 'vtum')->textInput() ?>

    <?= $form->field($model, 'uchastka')->textInput() ?>

    <?= $form->field($model, 'vilsoato')->textInput() ?>

    <?= $form->field($model, 'tumsoato')->textInput() ?>

    <?= $form->field($model, 'driver')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vac_all')->textInput() ?>

    <?= $form->field($model, 'vac_recieved')->textInput() ?>

    <?= $form->field($model, 'car')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'user')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'user_recieved')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_recieved')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
