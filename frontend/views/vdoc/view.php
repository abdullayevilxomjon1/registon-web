<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vdoc */

$this->title = "Қўлланма: ".$model->vaccine['name']." [".$model->vaccine['code']."]";
$this->params['breadcrumbs'][] = ['label' => 'Vdocs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vdoc-view">
     <!-- goal area Start -->
    <div class="goal-area pd-top-36">
        <div class="container">
            <div class="section-title">
                <br/>
            </div>
           <?= $model->document; ?> 
        </div>
    </div>

</div>
