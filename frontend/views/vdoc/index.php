<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Vdoc;

$vdocs = Vdoc::find()->all();
/* @var $this yii\web\View */
/* @var $searchModel app\models\VdocSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Қўлланмалар рўйхати';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goal-area">
        <div class="container">
			<?php foreach($vdocs as $val): ?>
			<a href="/vdoc/view?id=<?= $val->id; ?>">
			<div class="single-goal single-goal-one">
                <div class="row">
                    <div style="padding-left:15px;">
                        <div class="details">
                            <h6> <?= $val->vaccine['name']; ?> [<?= $val->vaccine['code']; ?>]</h6> 
							<p>вакцинасини эмлаш учун қўлланма</p>							
						</div>	
                    </div>                   
                </div>
            </div>
			</a>
			<?php endforeach; ?>	                            
        </div>    
</div>
