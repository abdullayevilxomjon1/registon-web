<?php
use app\models\Vdoc;
use yii\helpers\Html;

$vdocs = Vdoc::find()->all();
$this->title = 'VIS-REGISTON - Эпизоотияга қарши курашиш тадбирлари мониторинг тизими';
?>
<!-- add balance start -->
    <div class="add-balance-area">
        <div class="container">
			Сизда ушбу тизимдан фойдаланиш ваколати мавжуд эмас<br/>
<?= Html::a('<i class="feather icon-power text-danger"></i> Тизимдан чиқиш', ['site/logout'], ['data' => ['confirm' => 'Сиз ушбу тизимдан чиқиб бошқа логин билан кириш имконига эгасиз!', 'method' => 'POST'], 'class' => 'btn btn-danger']) ?>			
        </div>
    </div>
    <!-- add balance End -->
