<?php
use app\models\Vdoc;
/* @var $this yii\web\View */
$vdocs = Vdoc::find()->all();
$this->title = 'VIS-REGISTON - Эпизоотияга қарши курашиш тадбирлари мониторинг тизими';
?>
<!-- add balance start -->
    <div class="add-balance-area">
        <div class="container">
		
			<a href="/vaccination/create">
				<div class="ba-add-balance-title style-two">
					<h5>Вакцина ишлатиш</h5>
					<i class="fa fa-plus"></i>
				</div>
			</a></br>
		
            <div class="ba-add-balance-title ba-add-balance-btn">
                <h5>Вакцина қабул қилиш</h5>
                <i class="fa fa-plus"></i>
            </div>			
            <div class="ba-add-balance-inner mg-top-40">
                <div class="row custom-gutters-20">
                    <div class="col-6">
                        <a class="btn btn-blue" href="/vuch/new">Янги вакциналар <i class="fa fa-arrow-down"></i></a>
                    </div>
					<div class="col-6">
                        <a class="btn btn-green" href="/vuch/recieved">Қабул қилинганлари<i class="fa fa-arrow-down"></i></a>
                    </div>
                    <div class="col-6">
                        <a class="btn btn-red" href="/vaccination/spended">Сарфланганлари <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-6">
                        <a class="btn btn-purple" href="/vaccination/balance ">Қолдиқ <i class="fa fa-credit-card-alt "></i></a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- add balance End -->

    <!-- goal area Start -->
    <div class="goal-area pd-top-36">
        <div class="container">
            <div class="section-title">
                <h3 class="title">Вақциналар рўйхати</h3>
                <a href="/vuch/">Тўлиқ рўйхат</a> 
            </div>
			<?php foreach($vuch as $val):?>
			<div class="single-goal <?php if($val->status == 1): ?>single-goal-three<?php elseif($val->status == 2): ?>single-goal-one<?php endif; ?>">
                <div class="row">
                    <div class="col-7 pr-0">
                        <div class="details">
                            <h6><?php echo $val->vaccine['name']; ?> [ <?php echo $val->vaccine['code']; ?> ]</h6>
                            <?php if($val->status == 1): ?><p>Қабул қилинмаган</p><?php else: ?><p>Қабул қилинган санаси: <?= $val->date_recieved; ?></p><?php endif; ?>
                        </div>
                    </div>
                    <div class="col-5 pl-0">
                        <div class="circle-inner circle-inner-one">
                            <h6 class="goal-amount"> <?php echo $val->vac_all; ?> доза</h6>                            
                        </div>
                    </div>
                </div>
            </div>
			<?php endforeach; ?>            
        </div>
    </div>
    <!-- goal area End -->
	
	    <!-- blog-area start -->
    <div class="blog-area pd-top-36 pb-2 mg-top-40" style="background-image: url(/assets/img/bg/6.png);">
        <div class="container">
            <div class="section-title">
                <h3 class="title">Қўлланмалар</h3>
                <a href="/vdoc">Барчасини кўриш</a>
            </div>
            <div class="blog-slider owl-carousel">
			<?php foreach($vdocs as $val): ?>
                <div class="item">
                    <div class="single-blog">
                        <div class="thumb">
                            <img src="/assets/img/blog/1.png" alt="img">
                        </div>
                        <div class="details">
                            <a href="/vdoc/view?id=<?= $val->id; ?>"><?= $val->vaccine['name']; ?> [<?= $val->vaccine['code']; ?>] вакцинасини эмлаш учун қўлланма</a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>	                
            </div>
        </div>
    </div>
    <!-- blog-area End -->