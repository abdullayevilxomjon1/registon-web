<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Саҳифа топилмади';
?>
 
    <div class="balance-area pd-top-40">
        <div class="container">            
					<h1>404 ҳато</h1>
					<div class="alert alert-danger">
						Саҳифа топилмади
					</div>
					<p>        
						Юқоридаги хатолик веб-сервер сизнинг сўровингизга ишлов бераётганда юз берди 
					</p>                
        </div>
    </div>