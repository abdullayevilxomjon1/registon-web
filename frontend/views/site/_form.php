<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Profiles;
use app\models\Vuch;

$profiles = Profiles::findOne(Yii::$app->user->identity->id);
$model = new Vuch();
// Список инструкции		
$vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 1])->all();
/* @var $this yii\web\View */
/* @var $model app\models\Vuch */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin(['action' => '/vuch/recieve', 'method' => 'post']); ?>

	<div class="input-wrapper">
        <label class="label" for="account1">Кимдан ва қанча</label>
        <select class="form-control custom-select" name="Vuch[vtum]" id="vtum">
		<option selected="selected">Вакцинани танланг</option>
			<?php foreach($vuch as $uchval):?>
				<?php $driver = json_decode($uchval->driver, true); ?>					
					<option value="<?= $uchval->id; ?>" id="<?= $uchval->vac_all; ?>"><?= $uchval->vaccine['name']; ?> (<?= $uchval->vac_all; ?> доза) - <?= $driver['data']['inf']['surname_latin']; ?> <?= $driver['data']['inf']['name_latin']; ?> <?= $driver['data']['inf']['patronym_latin']; ?>.</option>
				<?php endforeach;?>
        </select>
    </div></br>    

    <?= $form->field($model, 'vac_recieved')->textInput(['type' => 'number', 'max' => 999, 'min' => 1]) ?>

	<div class="form-group basic">
        <label> <input type="checkbox" id="agreerecieve"> Вакциналарни қабул қилдим ва келгусида ушбу вакциналар жавобгарлигини ўз зиммамга оламан.</label>                                    
    </div>

    <div class="form-group basic">		  
        <?= Html::submitButton('Қабул қилдим', ['class' => 'btn-c btn-primary btn-block btn-lg', 'id' => 'buttonagree', 'disabled' => 'disabled']) ?>    		
    </div>		
	<?php ActiveForm::end(); ?>
	
	
<?php 
$this->registerJs(<<<JS
	$('#vtum').change(function(){
		//alert($(this).val());
		var dropd = document.getElementById("vtum");
		document.getElementById('vuch-vac_recieved').value=dropd.options[dropd.selectedIndex].id;		
		document.getElementById('vuch-vac_recieved').max=dropd.options[dropd.selectedIndex].id;		
	});
	
	$('#agreerecieve').click(function(){
		var checkBox = document.getElementById("agreerecieve");

		if(checkBox.checked == true){
			var vac = document.getElementById('vuch-vac_recieved').value;
			var dropd = document.getElementById("vtum");
			var vacid = dropd.options[dropd.selectedIndex].id;		
			if(vac > 0 && vacid > 0){
				document.getElementById("buttonagree").disabled = false;
			}			
		}else{
			document.getElementById("buttonagree").disabled = true;
		}
	});
	
	

JS
);
?>