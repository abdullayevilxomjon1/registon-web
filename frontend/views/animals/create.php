<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Animals */

$this->title = 'Ҳайвон киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Animals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$pinfl =  Yii::$app->request->get('pinfl'); 
$serial =  Yii::$app->request->get('serial'); 
$ownerdata = file_get_contents('http://m.registon.vetgov.uz/reference/getfizdata?pinfl='.$pinfl.'&serial='.$serial);
$ownerdatajson = json_decode($ownerdata, true);
$tin = "";
?>
 <div class="add-balance-area pd-top-40">
        <div class="container">    
	<?php if(isset($ownerdatajson['data']['inf']['document'])):?>	
	<h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
		'owner' => 	$ownerdata,
		'pinfl' => 	$pinfl,
		'serial' => $serial,
		'tin' => $tin,
    ]) ?>
	<?php else: ?>
	<center>Маълумотлар топилмади.</center>
	<?php endif; ?>

</div>
</div>
