<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Profiles;
use app\models\Animalstype;
use app\models\Breed;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Animals */
/* @var $form yii\widgets\ActiveForm */
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
$request = Yii::$app->request;
$address = $request->get('address', 0);	
?>
<style>
#animals-atype, #animals-color, #animals-sex, #animals-zot, select {
	height: 35px;
}
#animals-atype, #animals-color, #animals-sex, #animals-zot, select, input, #example-date-input {
	font-size: 16px;
}
</style>
<div class="animals-form">

    <?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'address')->hiddenInput(['value' => $address])->label(false) ?>
	<?= $form->field($model, 'owner')->hiddenInput(['value' => $owner])->label(false) ?>
	<?= $form->field($model, 'pinfl')->hiddenInput(['value' => $pinfl])->label(false) ?>
	<?= $form->field($model, 'serial')->hiddenInput(['value' => $serial])->label(false) ?>
	<?= $form->field($model, 'tin')->hiddenInput(['value' => $tin])->label(false) ?>
	<?= $form->field($model, 'ns10_code')->hiddenInput(['value' => $profiles['vil']])->label(false) ?>
	<?= $form->field($model, 'ns_11_code')->hiddenInput(['value' => $profiles['region']])->label(false) ?>	
	
	 <?= $form->field($model, 'atype')->dropDownList(ArrayHelper::map(Animalstype ::find()->all(), 'id', 'NAME_UZ'), ['prompt'=>'Турини танланг',
              'onchange'=>'
                $.get( "/animals/breed?id="+$(this).val(), function(data){
                  $("select#animals-zot").html(data);
                });
            '], ['options'=> ['class' => 'input-bordered']]); ?>
			
	<?= $form->field($model, 'zot')->dropDownList(ArrayHelper::map(Breed::find()->where(['top' => $model->atype])->all(), 'id', 'NAME_UZ'), ['options'=> ['class' => 'input-bordered']]); ?>
	
	<?= $form->field($model, 'color')->dropDownList([
							'1' => 'Қора-ола',
							'2' => 'Қизил-ола',
							'3' => 'Қизил ',
							'4' => 'Қора',
							'5' => 'Қўнғир',
							'6' => 'Кулранг',
							'7' => 'Оқ-кулранг',							
							'0' => 'Бошқа',
						], ['options'=> ['class' => 'input-bordered']]); ?>
    
	<?= $form->field($model, 'birth')->textInput(['class'=>'form-control', 'id'=>'example-date-input', 'type'=>'date']); ?>  

    

<?= $form->field($model, 'sex')->dropDownList([
							'1' => 'Эркак',
							'0' => 'Урғочи'														
						], ['options'=> ['class' => 'input-bordered']]); ?> 

    <?= $form->field($model, 'klichka')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
