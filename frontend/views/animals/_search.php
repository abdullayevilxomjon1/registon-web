<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnimalsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="animals-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'owner') ?>

    <?= $form->field($model, 'atype') ?>

    <?= $form->field($model, 'birth') ?>

    <?= $form->field($model, 'ns10_code') ?>

    <?php // echo $form->field($model, 'ns_11_code') ?>

    <?php // echo $form->field($model, 'zot') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'klichka') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <?php // echo $form->field($model, 'user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
