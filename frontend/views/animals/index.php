<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnimalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ҳайвонлар рўйхати';
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="add-balance-area pd-top-40">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			'animaltype.NAME_UZ',            
            'pinfl',
            'tin',            
            'birth',
            //'ns10_code',
            //'ns_11_code',
            //'zot',
            //'sex',
            //'klichka',
            //'created',
            //'updated',
            //'user',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>
