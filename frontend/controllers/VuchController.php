<?php

namespace frontend\controllers;

use Yii;
use app\models\Vuch;
use app\models\Profiles;
use app\models\VuchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VuchController implements the CRUD actions for Vuch model.
 */
class VuchController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vuch models.
     * @return mixed
     */
    public function actionIndex()
    {
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
        $vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->all();       
        $title = "Барча вакциналар"; 
        return $this->render('index', [
            'vuch' => $vuch,            
            'title' => $title,            
        ]);
    }
	
	public function actionNew()
    {
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
        $vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 1])->all();       
		$title = "Янги вакциналар"; 
        return $this->render('index', [
            'vuch' => $vuch,            
            'title' => $title,            
        ]);
    }
	
	public function actionRecieved()
    {
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
        $vuch = Vuch::find()->where(['uchastka' => $profiles->uchastka])->andWhere(['status' => 2])->all();       

        $title = "Қабул қилинган вакциналар"; 
		
        return $this->render('index', [
            'vuch' => $vuch,            
            'title' => $title,            
        ]);
    }

    /**
     * Displays a single Vuch model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vuch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vuch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	
	public function actionRecieve()
    {
		$data = Yii::$app->request->post();
		
		if($data['Vuch']['vtum'] > 0 && $data['Vuch']['vac_recieved']){
			$model = Vuch::find()->where(['id' => $data['Vuch']['vtum']])->andWhere(['status' => 1])->One();
			
			if($data['Vuch']['vac_recieved'] > $model->vac_all){
				return $this->goHome();
			}
			
			if($model->status == 1) {
				$profiles = Profiles::findOne(Yii::$app->user->identity->id);
				$model->status = 2;				
				$model->vac_recieved = $model->vac_all;				
				$model->vac_all = $data['Vuch']['vac_recieved'];				
				$model->user_recieved = $profiles->data;
				$model->date_recieved = date('Y-m-d H:i:s');
				$model->cartype = '-';
				$model->carnumber = '-';
				if($model->save()){
					return $this->goHome();				
				}
				else{
					print_r($model->errors);
					die();					
				}
			}
			else{
				return $this->goHome();				
			}
		}	
		
		die();
        $model = new Vuch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vuch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	
	public function actionTasks()
    {
        return $this->render('tasks');
    }
	
	public function actionLab()
    {
        return $this->render('lab');
    }		

    /**
     * Deletes an existing Vuch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vuch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vuch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vuch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
