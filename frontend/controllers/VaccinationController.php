<?php

namespace frontend\controllers;

use Yii;
use app\models\Vaccination;
use app\models\Animals;
use app\models\Address;
use app\models\Vacemlash;
use app\models\Vuch;
use app\models\VaccinationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VaccinationController implements the CRUD actions for Vaccination model.
 */
class VaccinationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vaccination models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VaccinationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }  


    /**
     * Displays a single Vaccination model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionSpended()
    {
        return $this->render('spended');
    }
	
	public function actionBalance()
    {
        return $this->render('balance');
    }
	
	public function actionVuchrecieve($id)
    {
		print_r(Yii::$app->request->post());
		die();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	

    /**
     * Creates a new Vaccination model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vaccination();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	
	public function actionEmlash()
    {
        $model = new Vaccination();
		$request = Yii::$app->request;

		$addid = $request->get('addid');
		$vid = $request->get('vid');
		$aid = $request->get('aid', 0);
		$animalbirka = $request->get('animalbirka', '-');
		$vemlashid = $request->get('vacemlash');
        // Хонадон
		$address = Address::findOne($addid);
		// Хайвон
		$animals = Animals::findOne($aid);		
		//Вакцина кирими
		$vuchid = Vuch::findOne($vid);
		//Эмлаш тартиби
		$vacemlash = Vacemlash::findOne($vemlashid);
		
		$model->vilsoato =  substr($address->soato, 0, 4);
		$model->tumsoato = $address->soato;
		$model->uchid = $address->uchastka;
		$model->addid = $address->id;		
		$model->vid = $vuchid->vid;
		$model->vresid = $vuchid->vresid;
		$model->vvilid = $vuchid->vvid;
		$model->vtumid = $vuchid->vtum;
		$model->serial = $vuchid->serialv;
		$model->vuchid = $vid;
		$model->animalid = $aid;
		$model->animalinfo = json_encode($animals, true);
		$model->animalbirka = $animalbirka;
		$model->vac_all = $vacemlash->quantity;
		$model->owner_type = 1;
		$model->owner_pinfl = '30406867190015';
		$model->owner_tin = '-';
		$model->owner_info = '-';
		$model->phone = '+998946346617';
		$model->status = 1;
		$model->save();
		 if($model->save()) {
            return $this->redirect(['/address/vaccination?id='.$addid.'&vaccine='.$vid]);
        }
    }
	
	
	public function actionProcess()
    {        
        return $this->render('process');
    }
	
	public function actionGetanimallist()
    {        
        $pinfl =  Yii::$app->request->get('pinfl'); 
		$serial =  Yii::$app->request->get('serial'); 
		// вакцина
		$vid =  Yii::$app->request->get('vaccine'); 
		
		
		$emlash = Vacemlash::find()->where(['vid' => $vid])->groupBy('quantity')->All();
		$atype = Vacemlash::find()->where(['vid' => $vid])->groupBy('animal')->All();
		$fr = "<option value='0'>0</option>";
		foreach($emlash as $vl){
			$fr .= "<option value='".$vl->quantity."'>".$vl->quantity."</option>";
		}
		$alist = '';
		foreach($atype as $avl){
			$alist .= $avl->animal.",";
		}
		
		$animals = Animals::find()->where(['pinfl' => $pinfl])->andWhere(['IN', 'atype', $alist])->All();
		
		$table = "<form action='/test' method='post'><table class='table'><tr><th>№</th><th>Ҳайвон</th><th>Эмланганлиги</th><th></th></tr>";
		$n = 0;
		
		foreach($animals as $val){			
			$table .= "<tr><td>".++$n."</td><td>".$val->animaltype['NAME_UZ']."</td><td>йўқ</td><td><select>".$fr."</select></td></tr>";
		}
		$table .= "</table>
		<input type='button' value='Эмлаш' class='btn btn-success' style='width:100%;'>		
		</form>";
		
		return $table;
		
    }

	
    /**
     * Updates an existing Vaccination model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vaccination model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vaccination model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vaccination the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vaccination::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
