<?php

namespace frontend\controllers;

use Yii;
use app\models\Animals;
use app\models\Breed;
use app\models\AnimalsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AnimalsController implements the CRUD actions for Animals model.
 */
class AnimalsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Animals models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnimalsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Animals model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Animals model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Animals();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/address/view?id='.$model->address]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Animals model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	
	
	public function actionBreed($id)
    {		
	   $this->enableCsrfValidation = false;
       $countTema = Breed::find()
			  ->where(['top' => $id])
			  ->count();
			  
		$Temas =  Breed::find()
			  ->where(['top' => $id])
			  ->all();  
			  
		$data ="";	  
		if($countTema > 0 AND $id != 0) {			
			$data .= "<option value=''>Турини танланг</option>";
			foreach ($Temas as $tema) {
				$data .= "<option value='".$tema->id."'>".$tema->NAME_RU."</option>";					
			}						
		}
		else {
			 // echo "<select style='background-color:#fff;'>";
			 // echo "<option value=''>Tumanni tanlang</option>";
			 // echo "</select>"; 
		}
		$data .= "<option value='19'>Бошқа</option>"; 
		return $data;
    }

    /**
     * Deletes an existing Animals model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Animals model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Animals the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Animals::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
