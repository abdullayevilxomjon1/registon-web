<?php

namespace frontend\controllers;

use Yii;
use app\models\Address;
use app\models\Animals;
use app\models\AnimalsSearch;
use app\models\Locfiz;
use app\models\AddressSearch;
use app\models\Profiles;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddressSearch();
		$data = Yii::$app->request->queryParams;
		$profiles = Profiles::find()->where(['id' => Yii::$app->user->identity->id])->One();
		$request = Yii::$app->request;
		$top = $request->get('top', 0);		
		$topmodel = Address::find()->where(['id' => $top])->One();				
		$data['AddressSearch']['top'] = $top;
		$data['AddressSearch']['soato'] = $profiles->region;		
        $dataProvider = $searchModel->search($data);
		

        return $this->render('indexfiz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'top' => $top,
            'topmodel' => $topmodel,
        ]);
    }
	
	public function actionJur()
    {
        $searchModel = new AddressSearch();
		$data = Yii::$app->request->queryParams;
		$profiles = Profiles::find()->where(['id' => Yii::$app->user->identity->id])->One();
		$request = Yii::$app->request;
		$top = $request->get('top', 0);		
		$topmodel = Address::find()->where(['id' => $top])->One();				
		$data['AddressSearch']['top'] = $top;
		$data['AddressSearch']['type'] = 16;
		$data['AddressSearch']['soato'] = $profiles->region;		
        $dataProvider = $searchModel->search($data);

        return $this->render('indexjur', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'top' => $top,
            'topmodel' => $topmodel,
        ]);
    }
	
	
	
	 public function actionLoc($id)
    {
		$request = Yii::$app->request;
		$lat = $request->get('lat', 0);	
		$long = $request->get('long', 0);	
        	 
		$model = new Locfiz;
		$model->aid = $id;
		$model->latitude = $lat;
		$model->longitude = $long;
		$model->user = Yii::$app->user->identity->id;
		$model->status = 1; 
		$model->udata = json_encode(Profiles::find()->where(['id' => Yii::$app->user->identity->id])->asArray()->One());		
        Locfiz::updateAll(['status' => 0], 'status = 1 and aid = '.$id);
		if($model->save()){
			return $this->redirect(['/address/view', 'id' => $id]);
		}
		else{
			print_r($model->errors);
		}
		

       
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {	
		$searchModel = new AnimalsSearch();
		$data = Yii::$app->request->queryParams;
		$data['AnimalsSearch']['address'] = $id;	
        $dataProvider = $searchModel->search($data);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $this->findModel($id),
        ]);	
    }
	 
	 public function actionVaccination($id)
    {	
		$pinfl =  Yii::$app->request->get('pinfl');	
		$vid =  Yii::$app->request->get('vaccine'); 
		
		$searchModel = new AnimalsSearch();
		$data = Yii::$app->request->queryParams;
		$data['AnimalsSearch']['address'] = $id;	
        $dataProvider = $searchModel->search($data);
		
        return $this->render('vaccination', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $this->findModel($id),			
			'vid' => $vid,
        ]);	
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Address();
		$request = Yii::$app->request;
		$top = $request->get('top');
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		$model->user = Yii::$app->user->identity->id;
		$model->soato = $profiles->region;
		$model->uchastka = $profiles->uchastka;
		$model->status = 1;
		$model->top = 1;
		

		if($top > 0){
			$topmodel = Address::find()->where(['id' => $top])->andWhere(['uchastka' => $profiles->uchastka])->One();				
			if(!isset($topmodel)){
				return $this->redirect(['/address']);
			}
			$model->top = $topmodel->id;
		}
		else{
			$topmodel = 0;
			$model->top = 0;
		}
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'top' => $model->top]);
        }

        return $this->render('create', [
            'model' => $model,
            'topmodel' => $topmodel,            
        ]);
    }
	
	public function actionCreatejur()
    {
        $model = new Address();
		$request = Yii::$app->request;
		$top = $request->get('top');
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		$model->user = Yii::$app->user->identity->id;
		$model->soato = $profiles->region;
		$model->uchastka = $profiles->uchastka;
		$model->status = 1;
		$model->top = 1;
		

		if($top > 0){
			$topmodel = Address::find()->where(['id' => $top])->andWhere(['uchastka' => $profiles->uchastka])->One();				
			if(!isset($topmodel)){
				return $this->redirect(['/address']);
			}
			$model->top = $topmodel->id;
		}
		else{
			$topmodel = 0;
			$model->top = 0;
		}
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'top' => $model->top]);
        }

        return $this->render('createjur', [
            'model' => $model,
            'topmodel' => $topmodel,            
        ]);
    }
	
	public function actionCreatefiz()
    {
        $model = new Address();
		$request = Yii::$app->request;
		$top = $request->get('top');
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		$model->user = Yii::$app->user->identity->id;
		$model->soato = $profiles->region;
		$model->uchastka = $profiles->uchastka;
		$model->status = 1;
		$model->top = 1;
		

		if($top > 0){
			$topmodel = Address::find()->where(['id' => $top])->andWhere(['uchastka' => $profiles->uchastka])->One();				
			if(!isset($topmodel)){
				return $this->redirect(['/address']);
			}
			$model->top = $topmodel->id;
		}
		else{
			$topmodel = 0;
			$model->top = 0;
		}
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'top' => $model->top]);
        }

        return $this->render('createfiz', [
            'model' => $model,
            'topmodel' => $topmodel,            
        ]);
    }


    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
