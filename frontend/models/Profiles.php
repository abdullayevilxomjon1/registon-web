<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property int $id
 * @property string $full_name
 * @property string $phone
 * @property int $account_type
 * @property int $vil
 * @property int $region
 * @property int $uchastka
 * @property int $status
 * @property string $position
 */
class Profiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone', 'account_type', 'vil', 'region', 'uchastka', 'position'], 'required'],
            [['account_type', 'vil', 'region', 'uchastka', 'status'], 'integer'],
            [['position'], 'string'],
            [['full_name'], 'string', 'max' => 256],
            [['phone'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Тўлик номи',
            'phone' => 'Phone',
            'account_type' => 'Account Type',
            'vil' => 'Vil',
            'region' => 'Region',
            'uchastka' => 'Uchastka',
            'status' => 'Status',
            'position' => 'Position',
        ];
    }
}
