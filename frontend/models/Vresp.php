<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vresp".
 *
 * @property int $id
 * @property int $vid
 * @property string $dognum
 * @property string $dogdate
 * @property int $vac_all
 * @property int $vac_doz
 * @property int $status
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Vresp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vresp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'dognum', 'vac_all', 'vac_doz', 'status', 'user'], 'required'],
            [['vid', 'vac_all', 'vac_doz', 'status', 'user'], 'integer'],
            [['dogdate', 'created', 'updated'], 'safe'],
            [['dognum'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Вакцина ID'),
            'dognum' => Yii::t('app', 'Шартнома рақами'),
            'dogdate' => Yii::t('app', 'Шартнома санаси'),
            'vac_all' => Yii::t('app', 'Вакцина сони'),
            'vac_doz' => Yii::t('app', 'Вакцина ўлчами'),
            'status' => Yii::t('app', 'Ҳолати'),
            'user' => Yii::t('app', 'Киритган шаҳс'),
            'created' => Yii::t('app', 'Киритилган санаси'),
            'updated' => Yii::t('app', 'Янгиланди'),
        ];
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }
	
	public function getVinfo()
    {
        return $this->hasOne(Vinfo::className(), ['id' => 'vac_doz']);
    }
	
	public function getContainername()
    {
        return $this->hasOne(Container::className(), ['id' => 'container'])->via('vinfo');
    }
	
	public function getUnitname()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit'])->via('vinfo');
    }
}
