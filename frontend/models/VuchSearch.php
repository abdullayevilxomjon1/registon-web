<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vuch;

/**
 * VuchSearch represents the model behind the search form of `app\models\Vuch`.
 */
class VuchSearch extends Vuch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vid', 'vresid', 'vvid', 'vtum', 'uchastka', 'vilsoato', 'tumsoato', 'vac_all', 'vac_recieved', 'status', 'user'], 'integer'],
            [['driver', 'car', 'created', 'updated', 'user_recieved', 'date_recieved'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vuch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vid' => $this->vid,
            'vresid' => $this->vresid,
            'vvid' => $this->vvid,
            'vtum' => $this->vtum,
            'uchastka' => $this->uchastka,
            'vilsoato' => $this->vilsoato,
            'tumsoato' => $this->tumsoato,
            'vac_all' => $this->vac_all,
            'vac_recieved' => $this->vac_recieved,
            'status' => $this->status,
            'user' => $this->user,
            'created' => $this->created,
            'updated' => $this->updated,
            'date_recieved' => $this->date_recieved,
        ]);

        $query->andFilterWhere(['like', 'driver', $this->driver])
            ->andFilterWhere(['like', 'car', $this->car])
            ->andFilterWhere(['like', 'user_recieved', $this->user_recieved]);

        return $dataProvider;
    }
}
