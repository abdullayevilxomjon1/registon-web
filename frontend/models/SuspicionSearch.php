<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Suspicion;

/**
 * SuspicionSearch represents the model behind the search form of `app\models\Suspicion`.
 */
class SuspicionSearch extends Suspicion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vil', 'tum', 'uch', 'deseasid', 'owner_pinfl', 'owner_tin', 'animal_type', 'user', 'status', 'labstatus', 'notified', 'animalid', 'animalbirka', 'oid'], 'integer'],
            [['animal', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Suspicion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vil' => $this->vil,
            'tum' => $this->tum,
            'uch' => $this->uch,
            'deseasid' => $this->deseasid,
            'owner_pinfl' => $this->owner_pinfl,
            'owner_tin' => $this->owner_tin,
            'animal_type' => $this->animal_type,
            'created' => $this->created,
            'updated' => $this->updated,
            'user' => $this->user,
            'status' => $this->status,
            'labstatus' => $this->labstatus,
            'notified' => $this->notified,
            'animalid' => $this->animalid,
            'animalbirka' => $this->animalbirka,
            'oid' => $this->oid,
        ]);

        $query->andFilterWhere(['like', 'animal', $this->animal]);

        return $dataProvider;
    }
}
