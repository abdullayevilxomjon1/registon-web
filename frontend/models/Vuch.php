<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vuch".
 *
 * @property int $id
 * @property int $vid
 * @property int $vresid
 * @property int $vvid
 * @property int $vtum
 * @property int $uchastka
 * @property int $vilsoato
 * @property int $tumsoato
 * @property string $driver
 * @property int $vac_all
 * @property string $car
 * @property int $status
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Vuch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vuch';
    }

	public $pinfl;
	public $serial;
	
	public $cartype;
	public $carnumber;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'vresid', 'vvid', 'vtum', 'uchastka', 'vilsoato', 'tumsoato', 'vac_all', 'car', 'status', 'user', 'cartype', 'carnumber'], 'required'],
			[['driver'], 'required', 'message' => 'Хайдовчи маълумотлари нотўғри киритилган'],
            [['vid', 'vresid', 'vvid', 'vtum', 'vac_recieved', 'uchastka', 'vilsoato', 'tumsoato', 'vac_all', 'status', 'user'], 'integer'],
            [['driver', 'car', 'user_recieved'], 'string'],
            [['created', 'updated', 'date_recieved'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Вакцина'),
            'vresid' => Yii::t('app', 'Республикаи кирим ID'),
            'vvid' => Yii::t('app', 'Вилоят кирим ID'),
            'vtum' => Yii::t('app', 'Туман кирим ID'),
            'uchastka' => Yii::t('app', 'Участка'),
            'vilsoato' => Yii::t('app', 'Вилоят'),
            'tumsoato' => Yii::t('app', 'Туман'),
            'driver' => Yii::t('app', 'Eтказиб бериш учун маъсул'),
            'vac_all' => Yii::t('app', 'Вакцина миқдори'),
            'vac_recieved' => Yii::t('app', 'Қабул қилинган вакцина миқдори'),
            'car' => Yii::t('app', 'Транспорт воситаси'),
            'status' => Yii::t('app', 'Ҳолат'),
            'user' => Yii::t('app', 'Тарқатди'),
            'created' => Yii::t('app', 'Тарқатилди'),
            'updated' => Yii::t('app', 'Янгиланди'),
			'pinfl' => Yii::t('app', 'Хайдовчи ЖШШИР'), 
            'serial' => Yii::t('app', 'Хайдовчи паспорт серияси ва рақами'), 
            'cartype' => Yii::t('app', 'Вакцина ташувчи тарнспорт тури'), 
            'carnumber' => Yii::t('app', 'Тарнспорт рақами'),
            'user_recieved' => Yii::t('app', 'Қабул қилувчи шахс'),
            'date_recieved' => Yii::t('app', 'Қабул қилинган сана'),
        ];
    }
	
	public function getTuman()
    {
        return $this->hasOne(Regions::className(), ['code' => 'tumsoato']);
    }
	
	public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['code' => 'vilsoato']);
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }
	
	public function getUch()
    {
        return $this->hasOne(Uchastkanames::className(), ['id' => 'uchastka']);
    }
	
	public function getVresp()
    {
        return $this->hasOne(Vresp::className(), ['id' => 'vresid']);
    }
}
