<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vacemlash;

/**
 * VacemlashSearch represents the model behind the search form of `app\models\Vacemlash`.
 */
class VacemlashSearch extends Vacemlash
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vid', 'animal', 'ege_from', 'ege_to', 'emlash_type', 'quantity', 'unit', 'imun_on_date', 'user'], 'integer'],
            [['emlash_period', 'imun_for_time', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacemlash::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vid' => $this->vid,
            'animal' => $this->animal,
            'ege_from' => $this->ege_from,
            'ege_to' => $this->ege_to,
            'emlash_type' => $this->emlash_type,
            'quantity' => $this->quantity,
            'unit' => $this->unit,
            'imun_on_date' => $this->imun_on_date,
            'created' => $this->created,
            'updated' => $this->updated,
            'user' => $this->user,
        ]);

        $query->andFilterWhere(['like', 'emlash_period', $this->emlash_period])
            ->andFilterWhere(['like', 'imun_for_time', $this->imun_for_time]);

        return $dataProvider;
    }
}
