<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ownersfiz".
 *
 * @property int $id
 * @property int $addid
 * @property string $full_name
 * @property int $pinfl
 * @property string $serial
 * @property string $data
 * @property string $created
 */
class Ownersfiz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ownersfiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['addid', 'full_name', 'pinfl', 'serial', 'data'], 'required'],
            [['addid', 'pinfl'], 'integer'],
            [['data'], 'string'],
            [['created'], 'safe'],
            [['full_name'], 'string', 'max' => 256],
            [['serial'], 'string', 'max' => 9],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'addid' => 'Манзил рақами',
            'full_name' => 'ЖШ И.Ш.О',
            'pinfl' => 'ЖШШИР',
            'serial' => 'паспорт серияси ва рақами',
            'data' => 'Маълумотлар',
            'created' => 'Яратилди',
        ];
    }
}
