<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vresp;

/**
 * VrespSearch represents the model behind the search form of `app\models\Vresp`.
 */
class VrespSearch extends Vresp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vid', 'vac_all', 'vac_doz', 'status', 'user'], 'integer'],
            [['dognum', 'dogdate', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vresp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vid' => $this->vid,
            'dogdate' => $this->dogdate,
            'vac_all' => $this->vac_all,
            'vac_doz' => $this->vac_doz,
            'status' => $this->status,
            'user' => $this->user,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'dognum', $this->dognum]);

        return $dataProvider;
    }
}
