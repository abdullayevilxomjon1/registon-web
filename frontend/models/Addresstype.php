<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresstype".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class Addresstype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresstype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тури',
            'status' => 'Ҳолати',
        ];
    }
}
