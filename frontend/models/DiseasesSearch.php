<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Diseases;

/**
 * DiseasesSearch represents the model behind the search form of `app\models\Diseases`.
 */
class DiseasesSearch extends Diseases
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'virus', 'bakteria', 'protozoy', 'eraxno_entamologiya', 'gelmintalogiya', 'dgroup'], 'integer'],
            [['name_ru', 'name_uz'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diseases::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'virus' => $this->virus,
            'bakteria' => $this->bakteria,
            'protozoy' => $this->protozoy,
            'eraxno_entamologiya' => $this->eraxno_entamologiya,
            'gelmintalogiya' => $this->gelmintalogiya,
            'dgroup' => $this->dgroup,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_uz', $this->name_uz]);

        return $dataProvider;
    }
}
