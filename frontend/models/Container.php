<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "container".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property int $status
 */
class Container extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'container';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru'], 'required'],
            [['status'], 'integer'],
            [['name_uz', 'name_ru'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Қадоқ ID рақами'),
            'name_uz' => Yii::t('app', 'Қадок номи'),
            'name_ru' => Yii::t('app', 'Қадок номи русча'),
            'status' => Yii::t('app', 'Ҳолати'),
        ];
    }
}
