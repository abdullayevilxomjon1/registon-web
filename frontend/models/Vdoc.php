<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vdoc".
 *
 * @property int $id
 * @property int $vid
 * @property string $document
 * @property int $status
 */
class Vdoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdoc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'document', 'status'], 'required'],
            [['vid', 'status'], 'integer'],
			[['created', 'updated'], 'safe'],
            [['document'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Vid'),
            'document' => Yii::t('app', 'Қўлланма матни'),
            'status' => Yii::t('app', 'Status'),
			'created' => Yii::t('app', 'Яратилди'),
            'updated' => Yii::t('app', 'Янгиланди'),
        ];
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }
}
