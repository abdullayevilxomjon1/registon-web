<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property int $top
 * @property int $status
 * @property int $soato
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name', 'soato', 'user'], 'required', 'message' => 'Ушбу қаторни тўлдирилиши шарт'],
            [['tin'], 'required', 'message' => 'Юридик шахс СТИР рақамини киритиш шарт'],
            [['type', 'top', 'status', 'soato', 'user', 'uchastka'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Манзил гурухи',
            'name' => 'Манзил номи',
            'top' => 'Top',
            'status' => 'Status',
            'soato' => 'Soato',
            'user' => 'User',
            'uchastka' => 'Участка',
            'created' => 'Сана',
            'updated' => 'Updated',
            'tin' => 'Юридик шахс стир',
        ];
    }
	
	public function getAtype()
    {
        return $this->hasOne(Addresstype::className(), ['id' => 'type']);
    }
	
	public function getAuthor()
    {
        return $this->hasOne(Profiles::className(), ['id' => 'user']);
    }
	
	public function getOwner()
    {
        return $this->hasOne(Ownersfiz::className(), ['addid' => 'id']);
    }
	
	public function getLocation()
    {
        return $this->hasOne(Locfiz::className(), ['aid' => 'id'])->andOnCondition(['status' => 1]);
    }
}
