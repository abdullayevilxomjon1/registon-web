<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vtum".
 *
 * @property int $id
 * @property int $vid
 * @property int $tumsoato
 * @property int $vresid
 * @property string $driver
 * @property int $vac_all
 * @property string $car
 * @property int $status
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Vtum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtum';
    }
	
	public $pinfl;
	public $serial;
	
	public $cartype;
	public $carnumber;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vvid', 'vid', 'vilsoato', 'tumsoato', 'vresid', 'vac_all', 'car', 'status', 'user', 'cartype', 'carnumber'], 'required'],
			[['driver'], 'required', 'message' => 'Хайдовчи маълумотлари нотўғри киритилган'],
            [['vvid', 'vid', 'vilsoato', 'tumsoato', 'vresid', 'vac_all', 'status', 'user'], 'integer'],
            [['driver', 'car'], 'string'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vvid' => Yii::t('app', 'Вилоятга кирим рақами'),
            'vid' => Yii::t('app', 'Вацина'),
            'tumsoato' => Yii::t('app', 'Туман'),
            'vilsoato' => Yii::t('app', 'Туман'),
            'vresid' => Yii::t('app', 'Кирим рақами'),
            'driver' => Yii::t('app', 'Eтказиб бериш учун маъсул'),
            'vac_all' => Yii::t('app', 'Вакцина миқдори'),
            'car' => Yii::t('app', 'Транспорт'),
            'status' => Yii::t('app', 'Ҳолат'),
            'user' => Yii::t('app', 'Тарқатди'),
            'created' => Yii::t('app', 'Тарқатилган сана'),
            'updated' => Yii::t('app', 'Янгиланган сана'),
			'pinfl' => Yii::t('app', 'Хайдовчи ЖШШИР'), 
            'serial' => Yii::t('app', 'Хайдовчи паспорт серияси ва рақами'), 
            'cartype' => Yii::t('app', 'Вакцина ташувчи тарнспорт тури'), 
            'carnumber' => Yii::t('app', 'Тарнспорт рақами'), 
        ];
    }
	
	public function getTuman()
    {
        return $this->hasOne(Regions::className(), ['code' => 'tumsoato']);
    }
	
	public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['code' => 'vilsoato']);
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }
}
