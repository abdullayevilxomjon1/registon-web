<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vvil".
 *
 * @property int $id
 * @property int $vid
 * @property int $vilsoato
 * @property int $vresid
 * @property string $driver
 * @property int $vac_all
 * @property string $car
 * @property int $status
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Vvil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vvil';
    }
	public $pinfl;
	public $serial;
	
	public $cartype;
	public $carnumber;
	

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'vilsoato', 'vresid', 'vac_all', 'car', 'status', 'user', 'cartype', 'carnumber'], 'required'],
            [['driver'], 'required', 'message' => 'Хайдовчи маълумотлари нотўғри киритилган'],
            [['vid', 'vilsoato', 'vresid', 'vac_all', 'status', 'user'], 'integer'],
            [['driver', 'car'], 'string'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Вакцина'),
            'vilsoato' => Yii::t('app', 'Вилоят'),
            'vresid' => Yii::t('app', 'Кирим рақами'),
            'driver' => Yii::t('app', 'Хайдовчи'),
            'vac_all' => Yii::t('app', 'Вакцина миқдори'),
            'car' => Yii::t('app', 'Транспорт'),
            'status' => Yii::t('app', 'Ҳолати'),
            'user' => Yii::t('app', 'Киритди'),
            'created' => Yii::t('app', 'Яратилди'),
            'updated' => Yii::t('app', 'Янгиланди'), 
            'pinfl' => Yii::t('app', 'Хайдовчи ЖШШИР'), 
            'serial' => Yii::t('app', 'Хайдовчи паспорт серияси ва рақами'), 
            'cartype' => Yii::t('app', 'Вакцина ташувчи тарнспорт тури'), 
            'carnumber' => Yii::t('app', 'Тарнспорт рақами'), 
        ];
    }
	
	public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['code' => 'vilsoato']);
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }	
	
	
}
