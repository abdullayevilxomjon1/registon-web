<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distype".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_uz
 * @property int $atype
 */
class Distype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_uz', 'atype'], 'required'],
            [['atype'], 'integer'],
            [['name_ru', 'name_uz'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Касаллик гурухи ID рақами'),
            'name_ru' => Yii::t('app', 'Касаллик гурухи русча'),
            'name_uz' => Yii::t('app', 'Касаллик гурухи ўзбеча'),
            'atype' => Yii::t('app', 'Гурух тури'),
        ];
    }
}
