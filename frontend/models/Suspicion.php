<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suspicion".
 *
 * @property int $id
 * @property int $vil
 * @property int $tum
 * @property int $uch
 * @property int $deseasid
 * @property string $animal
 * @property int $owner_pinfl
 * @property int $owner_tin
 * @property int $animal_type
 * @property string $created
 * @property string $updated
 * @property int $user
 * @property int $status
 * @property int $labstatus
 * @property int $notified
 * @property int|null $animalid
 * @property int|null $animalbirka
 * @property int $oid
 */
class Suspicion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suspicion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vil', 'tum', 'uch', 'deseasid', 'animal', 'owner_pinfl', 'owner_tin', 'animal_type', 'user', 'oid'], 'required'],
            [['vil', 'tum', 'uch', 'deseasid', 'owner_pinfl', 'owner_tin', 'animal_type', 'user', 'status', 'labstatus', 'notified', 'animalid', 'animalbirka', 'oid'], 'integer'],
            [['animal'], 'string'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vil' => Yii::t('app', 'Вилоят'),
            'tum' => Yii::t('app', 'Туман'),
            'uch' => Yii::t('app', 'Участка'),
            'deseasid' => Yii::t('app', 'Гумонланаётган касаллик'),
            'animal' => Yii::t('app', 'Ҳайвон'),
            'owner_pinfl' => Yii::t('app', 'Ҳайвон эгаси ЖШШИР'),
            'owner_tin' => Yii::t('app', 'Ҳайвон эгаси стири'),
            'animal_type' => Yii::t('app', 'Ҳайвон тури'),
            'created' => Yii::t('app', 'Яратилди'),
            'updated' => Yii::t('app', 'Янгиланди'),
            'user' => Yii::t('app', 'Киритди'),
            'status' => Yii::t('app', 'Ҳолати'),
            'labstatus' => Yii::t('app', 'Лабаратория холати'),
            'notified' => Yii::t('app', 'Огохлантирилиш холати'),
            'animalid' => Yii::t('app', 'Ҳайвон рақами'),
            'animalbirka' => Yii::t('app', 'Ҳайвон бирка рақами'),
            'oid' => Yii::t('app', 'Объект'),
        ];
    }
}
