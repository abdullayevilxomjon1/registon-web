<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property int $status
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'status'], 'required'],
            [['status'], 'integer'],
            [['name_uz', 'name_ru'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ўлчов ID рақами'),
            'name_uz' => Yii::t('app', 'Ўлчов номи'),
            'name_ru' => Yii::t('app', 'Ўлчов номи'),
            'status' => Yii::t('app', 'Ҳолати'),
        ];
    }
}
