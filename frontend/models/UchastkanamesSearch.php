<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Uchastkanames;

/**
 * UchastkanamesSearch represents the model behind the search form of `app\models\Uchastkanames`.
 */
class UchastkanamesSearch extends Uchastkanames
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ns10_code', 'ns11_code', 'status', 'soato'], 'integer'],
            [['NAME_UZ', 'NAME_RU'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Uchastkanames::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ns10_code' => $this->ns10_code,
            'ns11_code' => $this->ns11_code,
            'status' => $this->status,
            'soato' => $this->soato,
        ]);

        $query->andFilterWhere(['like', 'NAME_UZ', $this->NAME_UZ])
            ->andFilterWhere(['like', 'NAME_RU', $this->NAME_RU]);

        return $dataProvider;
    }
}
