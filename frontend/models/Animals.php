<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "animals".
 *
 * @property int $id
 * @property string|null $owner
 * @property int $atype
 * @property string $birth
 * @property int|null $ns10_code
 * @property int|null $ns_11_code
 * @property string|null $zot
 * @property string|null $sex
 * @property string|null $klichka
 * @property string $created
 * @property string $updated
 * @property int $user
 */
class Animals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['atype', 'birth', 'address'], 'required'],
            [['atype', 'ns10_code', 'ns_11_code', 'user', 'color', 'address'], 'integer'],
            [['birth', 'created', 'updated'], 'safe'],
            [['zot'], 'string', 'max' => 12],
            [['sex', 'klichka', 'tin', 'pinfl', 'serial'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Ҳайвон сақланадиган жой',
            'owner' => 'Ҳайвон эгаси',
            'pinfl' => 'ЖШШИР',
            'serial' => 'Серияси ва рақами',
            'tin' => 'СТИР',
            'atype' => 'Ҳайвон тури',
            'birth' => 'Туғилган куни',
            'ns10_code' => 'Вилоят',
            'ns_11_code' => 'Туман',
            'zot' => 'Ҳайвон зоти',
            'sex' => 'Ҳайвон жинси',
            'klichka' => 'Ҳайвон кличкаси',
            'created' => 'Яратилди',
            'updated' => 'Янгиланди',
            'user' => 'Фойдаланувчи',
            'color' => 'Ҳайвон туси',
        ];
    }
	
	public function getAnimaltype()
    {
        return $this->hasOne(Animalstype::className(), ['id' => 'atype']);
    }	
	
	public function getAnimalbreed()
    {
        return $this->hasOne(Breed::className(), ['id' => 'zot']);
    }	
	
}
