<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diseases".
 *
 * @property int $id
 * @property int|null $type
 * @property string|null $name_ru
 * @property string|null $name_uz
 * @property int $virus
 * @property int $bakteria
 * @property int $protozoy
 * @property int $eraxno_entamologiya
 * @property int $gelmintalogiya
 * @property int $dgroup
 */
class Diseases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diseases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'virus', 'bakteria', 'protozoy', 'eraxno_entamologiya', 'gelmintalogiya', 'dgroup'], 'integer'],
            [['name_ru', 'name_uz'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Касаллик гурухи'),
            'name_ru' => Yii::t('app', 'Касаллик номи (Руссча)'),
            'name_uz' => Yii::t('app', 'Касалик номи (Ўзбекча)'),
            'virus' => Yii::t('app', 'Virus'),
            'bakteria' => Yii::t('app', 'Bakteria'),
            'protozoy' => Yii::t('app', 'Protozoy'),
            'eraxno_entamologiya' => Yii::t('app', 'Eraxno Entamologiya'),
            'gelmintalogiya' => Yii::t('app', 'Gelmintalogiya'),
            'dgroup' => Yii::t('app', 'Dgroup'),
        ];
    }
	
	public function getDistypea()
    {
        return $this->hasOne(Distype::className(), ['id' => 'type']);
    }
}
