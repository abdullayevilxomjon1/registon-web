<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locfiz".
 *
 * @property int $id
 * @property string $latitude
 * @property string $longitude
 * @property int $aid
 * @property int $user
 * @property string $created
 * @property int $status
 * @property string $udata
 */
class Locfiz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locfiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'aid', 'user', 'status', 'udata'], 'required'],
            [['aid', 'user', 'status'], 'integer'],
            [['created'], 'safe'],
            [['udata'], 'string'],
            [['latitude', 'longitude'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'aid' => 'Aid',
            'user' => 'User',
            'created' => 'Created',
            'status' => 'Status',
            'udata' => 'Udata',
        ];
    }
}
