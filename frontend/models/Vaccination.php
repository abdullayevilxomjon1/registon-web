<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vaccination".
 *
 * @property int $id
 * @property int $vilsoato
 * @property int $tumsoato
 * @property int $uchid
 * @property int $vid
 * @property int $vresid
 * @property int $vvilid
 * @property int $vtumid
 * @property int $animalid
 * @property string $animalinfo
 * @property string $animalbirka
 * @property int $vac_all
 * @property int $owner_type
 * @property int $owner_pinfl
 * @property string $owner_tin
 * @property int $status
 * @property string $owner_info
 * @property string $phone
 */
class Vaccination extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vaccination';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vilsoato', 'tumsoato', 'uchid', 'vid', 'vresid', 'vvilid', 'vtumid', 'animalid', 'animalinfo', 'vac_all', 'owner_type', 'vuchid'], 'required'],
            [['id', 'vilsoato', 'tumsoato', 'uchid', 'vid', 'vresid', 'vvilid', 'vtumid', 'animalid', 'vac_all', 'owner_type', 'owner_pinfl', 'status', 'addid', 'vuchid'], 'integer'],
            [['animalinfo', 'owner_info'], 'string'],
            [['animalbirka'], 'string', 'max' => 128],
            [['owner_tin', 'phone', 'serial'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vilsoato' => Yii::t('app', 'Вилоят'),
            'tumsoato' => Yii::t('app', 'Туман'),
            'uchid' => Yii::t('app', 'Участка'),
            'vid' => Yii::t('app', 'Вакцина'),
            'addid' => Yii::t('app', 'Хонадон'),
            'vresid' => Yii::t('app', 'Вакцина республикага кирими'),
            'vvilid' => Yii::t('app', 'Вакцина вилоятга кирими'),
            'vtumid' => Yii::t('app', 'Вакцина туманга кирими'),
            'animalid' => Yii::t('app', 'Хайвон ID рақами'),
            'animalinfo' => Yii::t('app', 'Ҳайвон маълумотлари'),
            'animalbirka' => Yii::t('app', 'Ҳайвон бирка рақами'),
            'vac_all' => Yii::t('app', 'Вакцина дозаси'),
            'owner_type' => Yii::t('app', 'Ҳайвон эгаси тури'),
            'owner_pinfl' => Yii::t('app', 'Ҳайвон эгаси ЖШШИР'),
            'owner_tin' => Yii::t('app', 'Ҳайвон эгаси СТИР'),
            'status' => Yii::t('app', 'Ҳолати'),
            'owner_info' => Yii::t('app', 'Ҳайвон эгаси маълумотлари'),
            'phone' => Yii::t('app', 'Ҳайвон эгаси телефон рақами'),
            'vuchid' => Yii::t('app', 'Ҳайвон эгаси телефон рақами'),
            'time' => Yii::t('app', 'Ҳайвон эгаси телефон рақами'),
            'serial' => Yii::t('app', 'Вакцина серия рақами'),
        ];
    }
}
