<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Animals;

/**
 * AnimalsSearch represents the model behind the search form of `app\models\Animals`.
 */
class AnimalsSearch extends Animals
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'atype', 'ns10_code', 'ns_11_code', 'user', 'address'], 'integer'],
            [['owner', 'birth', 'zot', 'sex', 'klichka', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Animals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'atype' => $this->atype,
            'birth' => $this->birth,
            'ns10_code' => $this->ns10_code,
            'ns_11_code' => $this->ns_11_code,
            'created' => $this->created,
            'updated' => $this->updated,
            'user' => $this->user,
            'address' => $this->address,
        ]);

        $query->andFilterWhere(['like', 'owner', $this->owner])
            ->andFilterWhere(['like', 'zot', $this->zot])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'klichka', $this->klichka]);

        return $dataProvider;
    }
}
