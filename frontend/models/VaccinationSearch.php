<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vaccination;

/**
 * VaccinationSearch represents the model behind the search form of `app\models\Vaccination`.
 */
class VaccinationSearch extends Vaccination
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vilsoato', 'tumsoato', 'uchid', 'vid', 'vresid', 'vvilid', 'vtumid', 'animalid', 'vac_all', 'owner_type', 'owner_pinfl', 'status'], 'integer'],
            [['animalinfo', 'animalbirka', 'owner_tin', 'owner_info', 'phone'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vaccination::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vilsoato' => $this->vilsoato,
            'tumsoato' => $this->tumsoato,
            'uchid' => $this->uchid,
            'vid' => $this->vid,
            'vresid' => $this->vresid,
            'vvilid' => $this->vvilid,
            'vtumid' => $this->vtumid,
            'animalid' => $this->animalid,
            'vac_all' => $this->vac_all,
            'owner_type' => $this->owner_type,
            'owner_pinfl' => $this->owner_pinfl,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'animalinfo', $this->animalinfo])
            ->andFilterWhere(['like', 'animalbirka', $this->animalbirka])
            ->andFilterWhere(['like', 'owner_tin', $this->owner_tin])
            ->andFilterWhere(['like', 'owner_info', $this->owner_info])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
