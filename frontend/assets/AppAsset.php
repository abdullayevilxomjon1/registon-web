<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/assets';
    public $baseUrl = '@web/assets';    
    public $css = [
        'css/vendor.css',
        'css/style.css',
        'css/responsive.css',
    ];   
	public $js = [			    
        'js/main.js',		
    ];  
    public $depends = [		
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',		     
    ];
}
