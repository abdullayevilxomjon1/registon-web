<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property int $id
 * @property int|null $code
 * @property string|null $Name_uzl
 * @property string|null $Center_uzl
 * @property string|null $Name
 * @property string|null $Center
 * @property string|null $Name_ru
 * @property string|null $Center_ru
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'integer'],
            [['Name_uzl'], 'string', 'max' => 66],
            [['Center_uzl'], 'string', 'max' => 27],
            [['Name'], 'string', 'max' => 61],
            [['Center'], 'string', 'max' => 23],
            [['Name_ru'], 'string', 'max' => 51],
            [['Center_ru'], 'string', 'max' => 38],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ҳудуд ID рақами'),
            'code' => Yii::t('app', 'Махсус код'),
            'Name_uzl' => Yii::t('app', 'Худуд номи лотинча'),
            'Center_uzl' => Yii::t('app', 'Ҳудуд маркази лотинча'),
            'Name' => Yii::t('app', 'Худуд номи'),
            'Center' => Yii::t('app', 'Худуд маркази'),
            'Name_ru' => Yii::t('app', 'Худуд номи русча'),
            'Center_ru' => Yii::t('app', 'Худуд маркази русча'),
        ];
    }
}
