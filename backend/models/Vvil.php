<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vvil".
 *
 * @property int $id
 * @property int $vid
 * @property int $vilsoato
 * @property int $vresid
 * @property string $driver
 * @property int $vac_all
 * @property string $car
 * @property int $status
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Vvil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vvil';
    }
	public $pinfl;
	public $serial;
	
	public $cartype;
	public $carnumber;
	

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'vilsoato', 'vresid', 'vac_all', 'car', 'status', 'user', 'cartype', 'carnumber', 'serialv'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['driver'], 'required', 'message' => 'Махсул маълумотлари нотўғри киритилган'],
            [['vid', 'vilsoato', 'vresid', 'vac_all', 'status', 'user'], 'integer'],
            [['driver', 'car', 'user_recieved', 'serialv'], 'string'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Вакцина'),
			'serialv' => Yii::t('app', 'Серия рақами'),
            'vilsoato' => Yii::t('app', 'Вилоят'),
            'vresid' => Yii::t('app', 'Кирим рақами'),
            'driver' => Yii::t('app', 'Хайдовчи'),
            'vac_all' => Yii::t('app', 'Вакцина миқдори'),
            'car' => Yii::t('app', 'Транспорт'),
            'status' => Yii::t('app', 'Ҳолати'),
            'user' => Yii::t('app', 'Киритди'),
            'created' => Yii::t('app', 'Яратилди'),
            'updated' => Yii::t('app', 'Янгиланди'), 
            'pinfl' => Yii::t('app', 'Маъсул ЖШШИР'), 
            'serial' => Yii::t('app', 'Паспорт серияси ва рақами'), 
            'cartype' => Yii::t('app', 'Вакцина ташувчи транспорт тури'), 
            'carnumber' => Yii::t('app', 'Транспорт рақами'), 
            'user_recieved' => Yii::t('app', 'Қабул қилган шахс'), 
        ];
    }
	
	public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['code' => 'vilsoato']);
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }	
		
	
	
}
