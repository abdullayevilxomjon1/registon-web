<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "animalstype".
 *
 * @property int $id
 * @property string $NAME_UZ
 * @property string $NAME_RU
 * @property int $code
 */
class Animalstype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animalstype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAME_UZ', 'NAME_RU', 'code'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['code'], 'integer'],
            [['NAME_UZ', 'NAME_RU'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'NAME_UZ' => Yii::t('app', 'Ҳайвон тури (Ўзбекча)'),
            'NAME_RU' => Yii::t('app', 'Ҳайвон тури (Русча)'),
            'code' => Yii::t('app', 'Ҳайвон гуруҳи коди'),
        ];
    }
}
