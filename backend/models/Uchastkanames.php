<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uchastkanames".
 *
 * @property int $id
 * @property string $NAME_UZ
 * @property string $NAME_RU
 * @property int $ns10_code
 * @property int $ns11_code
 * @property int $status
 * @property int $soato
 */
class Uchastkanames extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uchastkanames';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAME_UZ', 'NAME_RU', 'ns10_code', 'ns11_code', 'soato'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['ns10_code', 'ns11_code', 'status', 'soato'], 'integer'],
            [['NAME_UZ', 'NAME_RU'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Участка ID рақами'),
            'NAME_UZ' => Yii::t('app', 'Участка номи'),
            'NAME_RU' => Yii::t('app', 'Участка номи русча'),
            'ns10_code' => Yii::t('app', 'Вилоят'),
            'ns11_code' => Yii::t('app', 'Туман'),
            'status' => Yii::t('app', 'Ҳолати'),
            'soato' => Yii::t('app', 'СОАТО'),
        ];
    }
}
