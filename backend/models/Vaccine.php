<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vaccine".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $country
 * @property string $manufacturer
 * @property int $user
 * @property string $created
 * @property string $updated
 * @property int $status
 */
class Vaccine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vaccine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'country', 'manufacturer', 'user', 'status'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['user', 'status'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name', 'manufacturer'], 'string', 'max' => 256],
            [['code'], 'string', 'max' => 128],
            [['country'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Вакцина номи'),
            'code' => Yii::t('app', 'Штамм'),
            'country' => Yii::t('app', 'Ишлаб чиқарилган давлат'),
            'manufacturer' => Yii::t('app', 'Ишлаб чиқарувчи'),
            'user' => Yii::t('app', 'Киритди'),
            'created' => Yii::t('app', 'Ярпатилди'),
            'updated' => Yii::t('app', 'Янгиланди'),
            'status' => Yii::t('app', 'Холати'),
        ];
    }
	
	 public function getCountries()
    {
        return $this->hasOne(Countries::className(), ['CODE' => 'country']);
    }
}
