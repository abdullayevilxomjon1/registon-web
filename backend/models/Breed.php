<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breed".
 *
 * @property int $id
 * @property string $NAME_UZ
 * @property string $NAME_RU
 * @property int $top
 */
class Breed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'breed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAME_UZ', 'NAME_RU', 'top'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['top'], 'integer'],
            [['NAME_UZ', 'NAME_RU'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Зот махсус ID рақами'),
            'NAME_UZ' => Yii::t('app', 'Ҳайвон зоти'),
            'NAME_RU' => Yii::t('app', 'Ҳайвон зоти'),
            'top' => Yii::t('app', 'Зот гурухи'),
        ];
    }
}
