<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emlash".
 *
 * @property int $id
 * @property int $animal
 * @property int $ege_from
 * @property int $ege_to
 * @property int $emlash_type
 * @property string $emlash_period
 * @property string $emlash_place
 * @property string $recommended_period
 * @property string $revaccination
 * @property int $quantity
 * @property int $unit
 * @property string $diagnostika
 * @property string $requirements
 * @property int $imun_on_date
 * @property string $imun_for_time
 */
class Emlash extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emlash';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['animal', 'ege_from', 'ege_to', 'emlash_type', 'emlash_period', 'emlash_place', 'recommended_period', 'revaccination', 'quantity', 'unit', 'diagnostika', 'requirements', 'imun_on_date', 'imun_for_time'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['animal', 'ege_from', 'ege_to', 'emlash_type', 'quantity', 'unit', 'imun_on_date'], 'integer'],
            [['emlash_period'], 'string'],
            [['emlash_place', 'recommended_period', 'revaccination', 'diagnostika', 'requirements', 'imun_for_time'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'animal' => Yii::t('app', 'Animal'),
            'ege_from' => Yii::t('app', 'Ege From'),
            'ege_to' => Yii::t('app', 'Ege To'),
            'emlash_type' => Yii::t('app', 'Emlash Type'),
            'emlash_period' => Yii::t('app', 'Emlash Period'),
            'emlash_place' => Yii::t('app', 'Emlash Place'),
            'recommended_period' => Yii::t('app', 'Recommended Period'),
            'revaccination' => Yii::t('app', 'Revaccination'),
            'quantity' => Yii::t('app', 'Quantity'),
            'unit' => Yii::t('app', 'Unit'),
            'diagnostika' => Yii::t('app', 'Diagnostika'),
            'requirements' => Yii::t('app', 'Requirements'),
            'imun_on_date' => Yii::t('app', 'Imun On Date'),
            'imun_for_time' => Yii::t('app', 'Imun For Time'),
        ];
    }
}
