<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property int $id
 * @property string $full_name
 * @property string $phone
 * @property int $account_type
 * @property int $vil
 * @property int $region
 * @property int $uchastka
 * @property int $status
 * @property string $position
 */
class Profiles extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone', 'account_type', 'vil', 'position', 'pinfl', 'serial', 'data'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
			['region', 'default', 'value' => 0],
			['uchastka', 'default', 'value' => 0],
            [['account_type', 'vil', 'region', 'uchastka', 'status', 'pinfl'], 'integer'],
            [['position', 'data'], 'string'],
            [['full_name'], 'string', 'max' => 256],
            [['phone'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'full_name' => Yii::t('app', 'И.Ш.О'),
            'phone' => Yii::t('app', 'Телефон'),
            'account_type' => Yii::t('app', 'Account Type'),
            'vil' => Yii::t('app', 'Vil'),
            'region' => Yii::t('app', 'Region'),
            'uchastka' => Yii::t('app', 'Uchastka'),
            'status' => Yii::t('app', 'Status'),
            'position' => Yii::t('app', 'Position'),
            'pinfl' => Yii::t('app', 'ЖШ ШИР'),
            'serial' => Yii::t('app', 'Серияси ва рақами'),
            'data' => Yii::t('app', 'Паспортные данные'),
        ];
    }
	
	public function getAccounttype()
    {
        return $this->hasOne(Accounttype::className(), ['id' => 'account_type']);
    }
	
	public function getVilcode()
    {
        return $this->hasOne(Regions::className(), ['code' => 'vil']);
    }
	
	public function getTumcode()
    {
        return $this->hasOne(Regions::className(), ['code' => 'region']);
    }
	
	public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

}
