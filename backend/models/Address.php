<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property int $top
 * @property int $status
 * @property int $soato
 * @property int $uchastka
 * @property int $user
 * @property string $created
 * @property string $updated
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name', 'soato', 'uchastka', 'user'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['type', 'top', 'status', 'soato', 'uchastka', 'user'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Объект'),
            'top' => Yii::t('app', 'Top'),
            'status' => Yii::t('app', 'Status'),
            'soato' => Yii::t('app', 'Soato'),
            'uchastka' => Yii::t('app', 'Uchastka'),
            'user' => Yii::t('app', 'User'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }
}
