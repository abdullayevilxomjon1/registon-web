<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vtum;

/**
 * VtumSearch represents the model behind the search form of `app\models\Vtum`.
 */
class VtumSearch extends Vtum
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vid', 'vvid', 'tumsoato', 'vresid', 'vac_all', 'status', 'user'], 'integer'],
            [['driver', 'car', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vtum::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vid' => $this->vid,
            'vvid' => $this->vvid,
            'tumsoato' => $this->tumsoato,
            'vresid' => $this->vresid,
            'vac_all' => $this->vac_all,
            'status' => $this->status,
            'user' => $this->user,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'driver', $this->driver])
            ->andFilterWhere(['like', 'car', $this->car]);

        return $dataProvider;
    }
}
