<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vinfo".
 *
 * @property int $id
 * @property int $vid ID вакцины
 * @property int $container Контейнер
 * @property int $quantity Количество
 * @property int $unit Единица измирения
 * @property int $retreat Отход
 * @property string $created Дата создания
 * @property string $updated Дата обнавления
 * @property int $user
 */
class Vinfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vinfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'container', 'quantity', 'unit', 'retreat', 'user'], 'required', 'message' => '{attribute} тўлдирилиши лозим'],
            [['vid', 'container', 'quantity', 'unit', 'retreat', 'user', 'doztomil'], 'integer'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Вакцина'),
            'container' => Yii::t('app', 'Идиш'),
            'quantity' => Yii::t('app', 'Идиш ҳажми'),
            'unit' => Yii::t('app', 'Ўлчов бирлиги'),
			'doztomil' => Yii::t('app', 'Бир доза мл.да'),
            'retreat' => Yii::t('app', 'Табиий йўқотиш'),
            'created' => Yii::t('app', 'Яратилди'),
            'updated' => Yii::t('app', 'Янгиланди'),
            'user' => Yii::t('app', 'Фодаланувчи'),
        ];
    }
	
	public function getContainern()
    {
        return $this->hasOne(Container::className(), ['id' => 'container']);
    }
	
	public function getUnitn()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit']);
    }
}
