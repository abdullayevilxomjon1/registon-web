<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacemlash".
 *
 * @property int $id
 * @property int $vid
 * @property int $animal
 * @property int $ege_from
 * @property int $ege_to
 * @property int $emlash_type
 * @property string $emlash_period
 * @property int $quantity
 * @property int $unit
 * @property int $imun_on_date
 * @property string $imun_for_time
 * @property string $created
 * @property string $updated
 * @property int $user
 */
class Vacemlash extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacemlash';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vid', 'animal', 'ege_from', 'ege_to', 'emlash_type', 'emlash_period', 'quantity', 'unit', 'imun_on_date', 'imun_for_time', 'user'], 'required', 'message' => 'Ушбу қатор тўлдирилиши шарт'],
            [['vid', 'animal', 'ege_from', 'ege_to', 'emlash_type', 'unit', 'imun_on_date', 'user', 'revacvination'], 'integer'],           
            [['created', 'updated', 'quantity', 'revac_quantity', 'revac_date'], 'safe'],
            [['imun_for_time'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vid' => Yii::t('app', 'Vid'),
            'animal' => Yii::t('app', 'Ҳайвон тури'),
            'ege_from' => Yii::t('app', 'Ёши ой (дан)'),
            'ege_to' => Yii::t('app', 'Ёши ой (гача)'),
            'emlash_type' => Yii::t('app', 'Эмлаш тури'),
            'emlash_period' => Yii::t('app', 'Эмлаш даври'),
            'quantity' => Yii::t('app', 'Миқдори'),
            'unit' => Yii::t('app', 'Ўлчов бирлиги'),
            'imun_on_date' => Yii::t('app', 'Иммунитет пайдо бўлиши(кун)'),
            'imun_for_time' => Yii::t('app', 'Иммунитет даврийлиги(ой)'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
            'user' => Yii::t('app', 'User'),
            'revacvination' => Yii::t('app', 'Ревакцинация'),
            'revac_date' => Yii::t('app', 'куни'),
            'revac_quantity' => Yii::t('app', 'миқдори'),
        ];
    }
	
	public function getAnimaltype()
    {
        return $this->hasOne(Animalstype::className(), ['id' => 'animal']);
    }
	
	public function getVaccine()
    {
        return $this->hasOne(Vaccine::className(), ['id' => 'vid']);
    }
	
	
	public function getUnitname()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit']);
    }
	
}
