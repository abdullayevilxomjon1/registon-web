<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $ID
 * @property string|null $NAME
 * @property string|null $NAME_FULL
 * @property string|null $CODE
 * @property string|null $CODE_NUM
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 41],
            [['NAME_FULL'], 'string', 'max' => 58],
            [['CODE'], 'string', 'max' => 2],
            [['CODE_NUM'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'NAME' => Yii::t('app', 'Давлатнинг қисқа номи'),
            'NAME_FULL' => Yii::t('app', 'Давлатнинг тўлиқ номи'),
            'CODE' => Yii::t('app', 'Махсус код'),
            'CODE_NUM' => Yii::t('app', 'Махсус номер'),
        ];
    }
}
