<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
	public $password_repeat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message'=>'Ушбу қатор тўлдирилиши шарт'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ушбу логин банд қилинган.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'message'=>'Ушбу қатор тўлдирилиши шарт'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ушбу электрон манзил бошқа шахс томонидан рўйхатга олинган.'],

            ['password', 'required', 'message'=>'Ушбу қатор тўлдирилиши шарт'],
            ['password', 'string', 'min' => 6],
			
			['password_repeat', 'required', 'message'=>'Ушбу қатор тўлдирилиши шарт'],
			['password_repeat', 'compare', 'compareAttribute'=>'password', 'skipOnEmpty' => false, 'message'=>"Калит сўзлар бир-бирига мос келмади."],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
	
}
