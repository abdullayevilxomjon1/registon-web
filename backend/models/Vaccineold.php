<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vaccine".
 *
 * @property int $id
 * @property string $name
 * @property string|null $data_contarct
 * @property string|null $data_vacine
 * @property string|null $data_usage
 * @property string $code
 * @property int $user_recieved
 * @property string|null $data_series
 * @property int $recieved_amount
 * @property int $used_amount
 * @property int $user_created
 * @property string $created
 * @property string $updated
 * @property int $status
 */
class Vaccine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vaccine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'user_recieved', 'recieved_amount', 'used_amount', 'user_created'], 'required'],
            [['data_contarct', 'data_vacine', 'data_usage', 'data_series'], 'string'],
            [['user_recieved', 'recieved_amount', 'used_amount', 'user_created', 'status'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 256],
            [['code'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'data_contarct' => Yii::t('app', 'Data Contarct'),
            'data_vacine' => Yii::t('app', 'Data Vacine'),
            'data_usage' => Yii::t('app', 'Data Usage'),
            'code' => Yii::t('app', 'Code'),
            'user_recieved' => Yii::t('app', 'User Recieved'),
            'data_series' => Yii::t('app', 'Data Series'),
            'recieved_amount' => Yii::t('app', 'Recieved Amount'),
            'used_amount' => Yii::t('app', 'Used Amount'),
            'user_created' => Yii::t('app', 'User Created'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
