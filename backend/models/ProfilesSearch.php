<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profiles;

/**
 * ProfilesSearch represents the model behind the search form of `app\models\Profiles`.
 */
class ProfilesSearch extends Profiles
{
    /**
     * {@inheritdoc}
     */
	public $tum; 
	 
    public function rules()
    {
        return [
            [['id', 'pinfl', 'account_type', 'vil', 'region', 'uchastka', 'status'], 'integer'],
            [['serial', 'data', 'full_name', 'phone', 'position', 'tum'], 'safe'],
        ];
    }
	
	

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		$this->load($params);
		
		if($this->tum == 1){
			$query = Profiles::find()->where(['>', 'region', '0'])->orderBy(['id' => SORT_DESC]);			
		}
		else{
			$query = Profiles::find()->orderBy(['id' => SORT_DESC]);			
		}
       
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

       

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pinfl' => $this->pinfl,
            'account_type' => $this->account_type,
            'vil' => $this->vil,
            'region' => $this->region,
            'uchastka' => $this->uchastka,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'position', $this->position]);

        return $dataProvider;
    }
}
