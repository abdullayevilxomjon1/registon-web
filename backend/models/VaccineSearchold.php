<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vaccine;

/**
 * VaccineSearch represents the model behind the search form of `app\models\Vaccine`.
 */
class VaccineSearch extends Vaccine
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_recieved', 'recieved_amount', 'used_amount', 'user_created', 'status'], 'integer'],
            [['name', 'data_contarct', 'data_vacine', 'data_usage', 'code', 'data_series', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vaccine::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_recieved' => $this->user_recieved,
            'recieved_amount' => $this->recieved_amount,
            'used_amount' => $this->used_amount,
            'user_created' => $this->user_created,
            'created' => $this->created,
            'updated' => $this->updated,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'data_contarct', $this->data_contarct])
            ->andFilterWhere(['like', 'data_vacine', $this->data_vacine])
            ->andFilterWhere(['like', 'data_usage', $this->data_usage])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'data_series', $this->data_series]);

        return $dataProvider;
    }
}
