<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Regions;
use app\models\Vaccine;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VvilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вилоятларга тарқатиш';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vvil-index">

    
	<div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="col-sm"></div>
	<div class="col-sm"></div>

	<div class="align-self-end">	
	</div>
	</div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'vid',            
			[
				'attribute'=>'vilsoato',
				'label' => Yii::t('app', 'Вилоят'),
				'filter' => false,
				'content'=>function($data){					
					return $data->region['Name'];
				},
			],
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => Html::activeDropDownList($searchModel, 'vid', ArrayHelper::map(Vaccine::find()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			
            //'vresid',
			[
				'attribute'=>'driver',
				'label' => Yii::t('app', 'Масъул шахс'),
				'filter' => false,
				'content'=>function($data){
					$jsondata = json_decode($data->driver, true);					
					return $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$data->car;
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'content'=>function($data){					
					return $data->vac_all." дона";
				},
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),				
				'content'=>function($data){					
					return $data->serialv;
				},
			],
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			],             
            //'user',            
			[
				'attribute'=>'created',
				'label' => Yii::t('app', 'Киритилди'),	
				'filter' => false,
				'content'=>function($data){					
					return $data->created;
				},
			],
			[
				'attribute'=>'name1', 
				'label' => Yii::t('app', ' '),				
				'content'=>function($data){
					return "<a href='/vvil/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			],  
            //'updated',            
        ],
    ]); ?>


</div>
