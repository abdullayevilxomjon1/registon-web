<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vvil */

$this->title = 'Create Vvil';
$this->params['breadcrumbs'][] = ['label' => 'Vvils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vvil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
