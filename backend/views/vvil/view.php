<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Regions;
use app\models\Vtum;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
/* @var $this yii\web\View */
/* @var $model app\models\Vvil */

$this->title = $model->vaccine['name']." [".$model->vaccine['code']."]";
$this->params['breadcrumbs'][] = ['label' => 'Vvils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vvil-view">

   
   
<?php 
$jsondata = json_decode($model->driver, true); 
if($model->status > 1){
	$jsondatareciever = json_decode($model->user_recieved, true);
	$reciever = $jsondatareciever['data']['inf']['surname_latin']." ".$jsondatareciever['data']['inf']['name_latin']." ".$jsondatareciever['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondatareciever['data']['inf']['document']."]"."<br/>";	
}
else{
	$reciever = "-";
}
?>

<?php  ?>
<?php 
switch($model->status){
						case 1: 
						$status = "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						if($profiles->account_type == 1 AND $profiles->vil == $model->vilsoato){
							$status .= Html::a('Қабул қилиш', ['accept', 'id' => $model->id], ['class' => 'btn btn-success']);
						}
						break;	
						case 2: 
						$status = "Қабул қилинди";
						break;	
}	
?>
<?php
$tarqatildi = Vtum::find()->select('SUM(vac_all) as sum')->where(['vvid' => $model->id])->asArray()->All();
if(!isset($tarqatildi[0]['sum'])){
	$tarqatildi[0]['sum'] = 0;
}
?>
 <h1><?= Html::encode($this->title) ?> <span class="btn btn-primary">Қолдиқ: <?= $model->vac_all - $tarqatildi[0]['sum']; ?></span></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',            
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),				
				'value'=> "<a href='/vaccine/view?id=".$model->vid."'>".$model->vaccine['name']." [".$model->vaccine['code']."]</a>", 
				'format' => 'HTML'
			],	
			[
				'attribute'=>'vilsoato',
				'label' => Yii::t('app', 'Қабул қилувчи вилоят'),				
				'value'=> $model->region['Name'], 				
			],	
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Юбориш рақами ва санаси'),				
				'value'=> $model->created." санадаги № ".$model->vresid." жўнатма", 				
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Масъул шахс'),				
				'value'=> $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$model->car, 				
				'format' => 'HTML',
			],
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Ҳолати'),				
				'value'=> $status, 	
				'format' => 'HTML',				
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Қабул қилган шахс'),				
				'value'=> $reciever, 				
				'format' => 'HTML',
			],
            [
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'value'=> $model->vac_all." та ".$vresp->containername['name_uz'], 								
			],		
			
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Тарқатилди'),				
				'value'=> $tarqatildi[0]['sum']." та ".$vresp->containername['name_uz'], 
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Қолдиқ'),				
				'value'=> $model->vac_all - $tarqatildi[0]['sum']." та ".$vresp->containername['name_uz'], 
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),				
				'value'=> $model->serialv, 
			],
        ],
    ]) ?>
	
	<h2>Вакцина тарқатилиши тарихи</h2>
	<?php if($model->status == 2 AND $profiles->account_type == 1 AND $profiles->vil == $model->vilsoato):?>
	<p>    
	    <?php if($model->vac_all - $tarqatildi[0]['sum'] > 0): ?>    
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Туманларга тарқатиш</button>
		<?php endif; ?>
    </p>
	<?php 
	$request = Yii::$app->request;
	$error = $request->get('error');
	if($error == 1){
		echo "<div class='alert alert-danger' style='margin:15px;'>Хатолик! тарқатилаётган вакцина умумий миқдори ".$model->vac_all." тадан ошмаслиги керак. <a href='/vvil/view?id=".$model->id."' class='close'><span>×</span></a></div>";		
	}
	?>
	<?php endif; ?>
	
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
				'attribute'=>'tumsoato',
				'label' => Yii::t('app', 'Туман'),
				'filter' => Html::activeDropDownList($searchModel, 'tumsoato', ArrayHelper::map(Regions::find()->where(['>', 'code', $model->vilsoato.'200'])->andWhere(['<', 'code', $model->vilsoato.'400'])->all(), 'code', 'Name'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){					
					return $data->tuman['Name'];
				},
			],
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => false,
				'content'=>function($data){
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),
				'filter' => false,
				'content'=>function($data){
					return $data->serialv; 
				},
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Кирим рақами ва санаси'),
				'filter' => false,
				'content'=>function($data){					
					return $data->created." санасидаги № Т-".$data->id." рақамли";
				},
			],
			[
				'attribute'=>'driver',
				'label' => Yii::t('app', 'Етказиб бериш'),
				'filter' => false,
				'content'=>function($data){
					$jsondata = json_decode($data->driver, true);					
					return $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$data->car;
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вақцина миқдори'),				
				'content'=>function($data){					
					return $data->vac_all." та";
				},
			], 
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			], 
			[
				'attribute'=>'name1', 
				'label' => Yii::t('app', ' '), 				
				'content'=>function($data){
					return "<a href='/vtum/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			],			
        ],
    ]); ?>
	
	<!--MODAL FORM --> 
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Вакциналарни тарқатиш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														 <?= $this->render('_tumform', [
															'model' => $tum,															
															'vid' => $model->vid,															
															'vvid' => $model->id,															
															'serial' => $model->serialv,															
															'vresid' => $model->vresid,															
															'vilsoato' => $model->vilsoato,															
														]) ?> 
												</div>
											</div>
										</div> 
</div>

</div>
