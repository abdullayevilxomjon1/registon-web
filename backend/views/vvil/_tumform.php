<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\models\Vtum */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['action' => '/vtum/create']); ?>
<?= $form->field($model, 'vid')->hiddenInput(['value' => $vid])->label(false) ?>
<?= $form->field($model, 'vvid')->hiddenInput(['value' => $vvid])->label(false) ?>
<?= $form->field($model, 'vresid')->hiddenInput(['value' => $vresid])->label(false) ?>														
<?= $form->field($model, 'vilsoato')->hiddenInput(['value' => $vilsoato])->label(false) ?>	
<div class="modal-body">													
	<div class="row">
		<div class="col-lg-4">	
		<?= $form->field($model, 'tumsoato')->dropDownList(ArrayHelper::map(Regions::find()->where(['>', 'code', $vilsoato.'200'])->andWhere(['<', 'code', $vilsoato.'400'])->all(), 'code', 'Name'), 
             ['prompt'=>'Туманни танланг']); ?>	
		</div>
		<div class="col-lg-4">																					
		<?= $form->field($model, 'vac_all')->textInput() ?>
		</div>
		<div class="col-lg-4">												
		<?php 
		$serial = explode(',', $serial);
		foreach($serial as $key => $value){
			$sr[$key]['id'] = $value;			
			$sr[$key]['name'] = $value;			
		}
		?>		
		<?= $form->field($model, 'serialv')->dropDownList(ArrayHelper::map($sr, 'id', 'name'), 
             ['prompt'=>'Серия рақамини танланг']); ?>
		</div>			
	</div>

	<div class="row">
		<div class="col-lg-4">	
		<?= $form->field($model, 'pinfl')->textInput() ?>
		</div>
		<div class="col-lg-4">																					
		<?= $form->field($model, 'serial')->textInput() ?>
		</div>	
		<div class="col-lg-4" id="driver">																					
		
		</div>	
	</div>	
	
	<div class="row">
		<div class="col-lg-6">	
		<?= $form->field($model, 'cartype')->textInput() ?>
		</div>
		<div class="col-lg-6">																					
		<?= $form->field($model, 'carnumber')->textInput() ?>
		</div>	
	</div>

    <?= $form->field($model, 'driver')->hiddenInput()->label(false) ?>    

    <?= $form->field($model, 'car')->hiddenInput()->label(false) ?>
	
	<div class="modal-footer">
	<?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-light" data-dismiss="modal">Ёпиш</button>													
</div>
  <?php ActiveForm::end(); ?>
  </div>
  <?php 
$this->registerJs(<<<JS
	$('#vtum-pinfl').on('change', function() { 
		var pinfl = document.getElementById("vtum-pinfl");
		var serial = document.getElementById("vtum-serial");
		document.getElementById("driver").innerHTML = '<br/>изланмоқда...';
		$("#vtum-driver").val();
	 		    
			 $.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){								 
                if(status == 'success'){
				  if(data == ''){
						$("#vtum-driver").val();					  
						document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади';
				  }
				  else{
					  $("#vtum-driver").val(data);
					  var jsondata = JSON.parse(data);							
					  var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;				  				  					  					
					  document.getElementById("driver").innerHTML = '<br/>'+text;						
					}	
				}
				else{
					$("#vtum-driver").val();	
					document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади...';
				}	
                });
	});
	
	$('#vtum-serial').on('change', function() { 
		var pinfl = document.getElementById("vtum-pinfl");
		var serial = document.getElementById("vtum-serial");
		document.getElementById("driver").innerHTML = '<br/>изланмоқда...';
		$("#vtum-driver").val();
	 		  
			 $.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){	
				if(status == 'success'){
				  if(data == ''){
						$("#vtum-driver").val();					  
						document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади';
				  }
				  else{
					  $("#vtum-driver").val(data);
					  var jsondata = JSON.parse(data);							
					  var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;				  				  					  					
					  document.getElementById("driver").innerHTML = '<br/>'+text;						
					}	
				}
				else{
					$("#vtum-driver").val();
					document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади...';
					
				}				
			});	
	});
	
	$('#vtum-cartype').on('change', function() { 
		var cartype = document.getElementById("vtum-cartype");
		var carnumber = document.getElementById("vtum-carnumber");
            $("#vtum-car").val(cartype + ", " + carnumber);            
	});
	
	$('#vtum-carnumber').on('change', function() { 
		var cartype = document.getElementById("vtum-cartype");
		var carnumber = document.getElementById("vtum-carnumber");
            $("#vtum-car").val(cartype.value + ", " + carnumber.value);            
	});
JS
);
?>