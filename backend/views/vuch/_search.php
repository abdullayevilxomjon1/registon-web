<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VuchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vuch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vid') ?>

    <?= $form->field($model, 'vresid') ?>

    <?= $form->field($model, 'vvid') ?>

    <?= $form->field($model, 'vtum') ?>

    <?php // echo $form->field($model, 'uchastka') ?>

    <?php // echo $form->field($model, 'vilsoato') ?>

    <?php // echo $form->field($model, 'tumsoato') ?>

    <?php // echo $form->field($model, 'driver') ?>

    <?php // echo $form->field($model, 'vac_all') ?>

    <?php // echo $form->field($model, 'car') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'user') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
