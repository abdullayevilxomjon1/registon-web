<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VuchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вакциналарнинг участкаларга тарқатилиши';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vuch-index">

     <div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>

	<div class="align-self-end" style="padding-right:15px;">	
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Саралаш</button>
	</div>
<!--MODAL FORM -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Рўйхатни саралаш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>	
													<div class="modal-body">													
														<?php echo $this->render('_search', ['model' => $searchModel]); ?>
													</div>
												</div>
											</div>
										</div> 
</div>

   

   <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'emptyText' => 'маълумот топилмади',
		'layout' => "{items}\n{pager}",			
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
				'attribute'=>'vvid',
				'label' => Yii::t('app', 'Вилоят'),
				'filter' => false,
				'content'=>function($data){					
					return $data->region['Name'];
				},
			],
			[
				'attribute'=>'tumid',
				'label' => Yii::t('app', 'Туман'),
				'filter' => false,
				'content'=>function($data){					
					return $data->tuman['Name'];
				},
			],
			[
				'attribute'=>'uchastka',
				'label' => Yii::t('app', 'Участка'),
				'filter' => false,
				'content'=>function($data){					
					return $data->uch['NAME_UZ'];
				},
			],
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => false,
				'content'=>function($data){					
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'content'=>function($data){					
					return $data->vac_all." та";
				},
			],	
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),				
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle;'>Етказиб берилмоқда</span> <img src='/assets/images/car.png' width='35'>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			],			
            //'vilsoato',
            //'tumsoato',
            //'driver:ntext',
            //'vac_all',
            //'car:ntext',
            //'status',
            //'user',
			[
				'attribute'=>'created',
				'label' => Yii::t('app', 'Тарқатилди'),				
				'filter' => false,
				'content'=>function($data){					
					return $data->created;			
				},
			],
            //'updated',
            
        ],
    ]); ?>

</div>
