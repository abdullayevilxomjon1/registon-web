<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vuch */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vuches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vuch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vid',
            'vresid',
            'vvid',
            'vtum',
            'uchastka',
            'vilsoato',
            'tumsoato',
            'driver:ntext',
            'vac_all',
            'car:ntext',
            'status',
            'user',
            'created',
            'updated',
        ],
    ]) ?>

</div>
