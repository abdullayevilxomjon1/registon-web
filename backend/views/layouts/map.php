<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta name="begin" content="بسم الله الرحمن الرحيم">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    <link href="/map/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/map/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/map/css/bootstrap-select.min.css" type="text/css">
    <link rel="stylesheet" href="/map/css/osm.css" type="text/css">
    <link rel="stylesheet" href="/map/css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="/map/css/MarkerCluster.css" type="text/css">
    <link rel="stylesheet" href="/map/css/style.css" type="text/css">
	
</head>
<body>
<?php $this->beginBody() ?>
<body class="page-homepage navigation-fixed-top has-fullscreen-map map-osm" id="page-top" data-spy="scroll" data-target=".navigation" data-offset="90">
<!-- Wrapper -->
<div class="wrapper">
    <div class="navigation">
        <div class="container">
            <header class="navbar" id="top" role="banner">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand nav" id="brand">
                        <a href="/"><img src="http://registon.vetgov.uz/assets/images/logogeo.png" alt="brand"></a>
                    </div>
                </div>
                <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                    <ul class="nav navbar-nav">
                        <li class="active has-child"><a href="#">Вакциналар</a>
                            <ul class="child-navigation">
                              <li class=""><a href="/vaccine/" class="">Вакциналар реестри</a></li>							
							<li class=""><a href="/vresp/" class="">Вакциналар кирими</a></li>							
							<li class=""><a href="/vvil/" class="">Вилоятларга тарқатиш</a></li>
							<li class=""><a href="/vtum/" class="">Туманларга тарқатиш</a></li>
							<li class=""><a href="/vuch/" class="">Участкаларга тарқатиш</a></li>                 
                            </ul>
                        </li>
                        <li class="has-child"><a href="#">Вакцинация</a>
                            <ul class="child-navigation">
                                <li><a href="#">Профилактик вакцинациялари</a></li>
                                <li><a href="#">Мажбурий вакцинациялар</a></li>                                
                            </ul>
                        </li>
                        <li class="active has-child"><a href="#">Касалликлар</a>
                            <ul class="child-navigation">
                            <li ><a href="#" >Касаллик гумонлар</a></li>
							<li ><a href="#" >Лабараторияга юборилган анализлар</a></li>
							<li ><a href="#" >Тасдикланмаган анализлар</a></li>
							<li ><a href="#" >Тасдикланган анализлар</a></li>		
                          </ul>
                        </li>
                        <li class="has-child"><a href="#">Маълумотномалар</a>
                            <ul class="child-navigation">
                           <li class=""><a href="/countries" class="">Давлатлар</a></li>
							<li class=""><a href="/animalstype " class="">Ҳайвонлар</a></li>
							<li class=""><a href="/breed" class="">Ҳайвон турлари</a></li>							
							<li class=""><a href="/container" class="">Қадоқ</a></li>							
							<li class=""><a href="/unit" class="">Қадоқ ўлчамлари</a></li>							
							<li class=""><a href="/diseases" class="">Касалликлар</a></li>							
							<li class=""><a href="/distype" class="">Касалликлар турлари</a></li>							
							<li class=""><a href="/regions" class="">Ҳудудлар</a></li>							
							<li class=""><a href="/uchastkanames" class="">Участкалар</a></li>		
                            </ul>
                        </li>                        
                        <li class="has-child"><a href="#">Чиқиндилар</a>
                            <ul class="child-navigation">
                            <li ><a href="#" >Ўта хавфли чиқиндилар реестри</a></li>
							<li ><a href="#" >Геоахборот модули</a></li>
							<li ><a href="#" >Хисоботлар</a></li>	
                            </ul>
                        </li>                        
                    </ul>
                </nav><!-- /.navbar collapse-->
                <!--<div class="add-your-property">
                    <a href="submit.html" class="btn btn-default"><i class="fa fa-plus"></i><span class="text">Қўшиш</span></a>
                </div>-->
            </header><!-- /.navbar -->
        </div><!-- /.container -->
    </div><!-- /.navigation --> 
    <!-- Map -->
    <div id="map" class="has-parallax"></div>
    <!-- end Map -->

    <!-- Search Box -->
    <div class="search-box-wrapper">
        <div class="search-box-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="search-box map">
                            <form role="form" id="form-map" class="form-map form-search" action="/">
                                <h2>Объектни излаш</h2>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="search-box-property-id" placeholder="Объект номи">
                                </div>
                                <div class="form-group">
                                    <select name="type">
                                        <option value="">Холати</option>
                                        <option value="1">Амалдаги</option>
                                        <option value="2">Тўхтатилган</option>
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <select name="country">
                                        <option value="">Вилоят</option>                                        
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <select name="city">
                                        <option value="">Туман</option>
                                       
                                    </select>
                                </div><!-- /.form-group -->                               
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default">Излаш</button>
                                </div><!-- /.form-group -->
                            </form><!-- /#form-map -->
                        </div><!-- /.search-box.map -->
                    </div><!-- /.col-md-3 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.search-box-inner -->
    </div>
    <!-- end Search Box -->
</div>

<div id="overlay"></div>

<script type="text/javascript" src="/map/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="/map/js/leaflet.js"></script>
<script type="text/javascript" src="/map/js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="/map/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/map/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/map/js/custom-map.js"></script>
<script type="text/javascript" src="/map/js/custom.js"></script>
<!--[if gt IE 8]>
<script type="text/javascript" src="/map/js/ie.js"></script>
<![endif]-->
<script>
    _latitude = 41.311081;
    _longitude = 69.240562;
    createHomepageOSM(_latitude,_longitude);
    $(window).load(function(){
        initializeOwl(false);
    });
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
