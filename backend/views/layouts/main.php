<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use app\models\Profiles;
use yii\helpers\Url;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
AppAsset::register($this);
// формируем наименовение роли
if($profiles->account_type == 1){
	if($profiles->region > 0){
		$position = "Туман ".$profiles->accounttype['name'];
	}
	elseif($profiles->vil > 0){
		$position = "Вилоят ".$profiles->accounttype['name'];
	}
	else{
		$position = "Республика ".$profiles->accounttype['name'];
	}
}
else{
	$position = $profiles->accounttype['name'];
}
$reslink = 0;
$villink = 0;
$tumlink = 0;
$adm = array(3, 4, 10);
if(in_array($profiles->account_type, $adm)) {	
	$reslink = 1;
}
elseif($profiles->account_type == 1){
	if($profiles->vil == 0){
		$reslink = 1;	
	}
	elseif($profiles->region == 0){
		$villink = 1;
	}
	else{
		$tumlink = 1;
	}
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta name="begin" content="بسم الله الرحمن الرحيم">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<!-- Favicon icon -->
	<link rel="icon" href="/assets/images/favicon.svg" type="image/x-icon">
	<!-- fontawesome icon -->
	<link rel="stylesheet" href="/assets/fonts/fontawesome/css/fontawesome-all.min.css">
	<!-- animation css -->
	<link rel="stylesheet" href="/assets/plugins/animation/css/animate.min.css">


	<!-- notification css -->
	<link rel="stylesheet" href="/assets/plugins/notification/css/notification.min.css">

	<!-- vendor css -->
	<link rel="stylesheet" href="/assets/css/style.css">
	
</head>
<body>
<?php $this->beginBody() ?>


	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menupos-fixed menu-dark icon-colored">
		<div class="navbar-wrapper ">
			<div class="navbar-brand header-logo">
				<a href="/" class="b-brand">	<img src="/assets/images/registon-logo-white.png" alt="" class="logo images">
					<img src="/assets/images/registon-logo-white.png" alt="" class="logo-thumb images">
				</a>
				<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
			</div>
			<div class="navbar-content scroll-div" >
				
				
				
				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
						<label>Меню</label>
					</li>					
					<li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Вакциналар</span></a>
						<ul class="pcoded-submenu">
							<?php if($reslink == 1): ?>
							<li class=""><a href="/vaccine/" class="navlink">Вакциналар реестри</a></li>							
							<li class=""><a href="/vresp/" class="navlink">Вакциналар кирими</a></li>														
							<li class=""><a href="/vvil/" class="navlink">Вилоятларга тарқатиш</a></li>
							<?php endif; ?>
							<?php if($villink == 1): ?>
							<li class=""><a href="/vaccine/" class="navlink">Вакциналар реестри</a></li>							
							<li class=""><a href="/vvil/recieve" class="navlink">Янги вакциналар</a></li>
							<li class=""><a href="/vvil/inbase" class="navlink">Қабул қилинган вакциналар</a></li>
							<li class=""><a href="/vtum/" class="navlink">Туманларга тарқатиш</a></li>							
							<?php endif; ?>
							<?php if($tumlink == 1): ?>
								<li class=""><a href="/vaccine/" class="navlink">Вакциналар реестри</a></li>							
								<li class=""><a href="/vtum/recieve" class="navlink">Янги вакциналар</a></li>
							<li class=""><a href="/vtum/inbase" class="navlink">Қабул қилинган вакциналар</a></li>
								<li class=""><a href="/vuch/" class="navlink">Участкаларга тарқатиш</a></li>
							<?php endif; ?>
						</ul>
					</li>
					<li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Вакцинация</span></a>
						<ul class="pcoded-submenu">							
							<li class=""><a href="/vaccination/prophylactic" class="">Профилактик вакцинациялари</a></li>
							<li class=""><a href="/vaccination/mandatory" class="">Мажбурий вакцинациялар</a></li>							
						</ul>
					</li>
					<li data-username="widget statistic data chart" class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Касалликлар</a>
						<ul class="pcoded-submenu">
							<li class=""><a href="/suspicion/status1" class="navlink">Касаллик гумонлар</a></li>
							<li class=""><a href="/suspicion/status2" class="navlink">Лабараторияга юборилган анализлар</a></li>
							<li class=""><a href="/suspicion/status3" class="navlink">Тасдикланмаган анализлар</a></li>
							<li class=""><a href="/suspicion/status4" class="navlink">Тасдикланган анализлар</a></li>							
						</ul>
					</li>
					<?php if($reslink == 1): ?>
					<li data-username="widget statistic data chart" class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Дорихона мудирлари</a>
						<ul class="pcoded-submenu">
							<li class=""><a href="/profiles/vil" class="">Вилоят дорихона мудирлари</a></li>
							<li class=""><a href="/profiles/tum" class="">Туман дорихона мудирлари</a></li>													
						</ul>
					</li>
					<?php endif; ?>
					<li class="nav-item pcoded-menu-caption">
						<label>Статистика</label>
					</li>
					<li data-username="basic components button alert badges breadcrumb pagination progress tooltip popovers carousel cards collapse tabs pills modal spinner grid system toasts typography extra shadows embeds" class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Маълумотномалар</span></a>
						<ul class="pcoded-submenu">
							<li class=""><a href="/countries" class="">Давлатлар</a></li>
							<li class=""><a href="/animalstype " class="">Ҳайвонлар</a></li>
							<li class=""><a href="/breed" class="">Ҳайвон турлари</a></li>							
							<li class=""><a href="/container" class="">Қадоқ</a></li>							
							<li class=""><a href="/unit" class="">Қадоқ ўлчамлари</a></li>							
							<li class=""><a href="/diseases" class="">Касалликлар</a></li>							
							<li class=""><a href="/distype" class="">Касалликлар турлари</a></li>							
							<li class=""><a href="/regions" class="">Ҳудудлар</a></li>							
							<li class=""><a href="/uchastkanames" class="">Участкалар</a></li>							
						</ul>
					</li>	
					<li class="nav-item pcoded-menu-caption">
						<label>Захарли чиқиндилар</label>
					</li>
					<li data-username="basic components button alert badges breadcrumb pagination progress tooltip popovers carousel cards collapse tabs pills modal spinner grid system toasts typography extra shadows embeds" class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Чиқиндилар</span></a>
						<ul class="pcoded-submenu">
							<li class=""><a href="#" class="">Ўта хавфли чиқиндилар реестри</a></li>
							<li class=""><a href="/geolocation/map/" class="">Геоахборот модули</a></li>
							<li class=""><a href="#" class="">Хисоботлар</a></li>							
						</ul>
					</li>	
					
				</ul>
			</div>
			
		</div>
	</nav>
	<!-- [ navigation menu ] end -->

	

	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed">
		
			<div class="m-header">
				<a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
				<a href="/" class="b-brand">					
					<img src="/assets/images/registon-logo-white.png" alt="" class="logo images">
					<img src="/assets/images/registon-logo-white.png" alt="" class="logo-thumb images">
				</a>
			</div>
			<a class="mobile-menu" id="mobile-header" href="#!">
				<i class="feather icon-more-horizontal"></i>
			</a>
			<div class="collapse navbar-collapse">
				<a href="#!" class="mob-toggler"></a>				
				<ul class="navbar-nav ml-auto">					
					<li><a href="#!" class="displayChatbox"><i class="icon feather icon-mail"></i></a></li>
					<li>
						<div class="dropdown drp-user">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon feather icon-settings"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right profile-notification">
								<div class="pro-head">									
									<span>
										<span class="text-muted"><?= $profiles->full_name; ?></span>
										<span class="h6"><?= $position; ?></span>
									</span>
								</div>
								<ul class="pro-body">
									<li><a href="/user" class="dropdown-item"><i class="feather icon-settings"></i> Менинг созламаларим</a></li>																									
									<li><?= Html::a('<i class="feather icon-power text-danger"></i> Тизимдан чиқиш', ['site/logout'], ['data' => ['confirm' => 'Сиз тизимдан чиқиш тугмасини босдингиз!', 'method' => 'POST'], 'class' => 'dropdown-item']) ?></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
			
	</header>
	<!-- [ Header ] end -->
	


<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div>
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<div class="main-body">
					<div class="page-wrapper">												
						<div class="row">
							<!-- sessions-section start -->
							<div class="col-xl-12 col-md-6">
								<div class="card table-card">
										
											<?= Alert::widget() ?>											
											<?= $content ?>
										
									
								</div>
							</div>
							<!-- sessions-section end -->
						</div>

						<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


    <!-- Required Js -->
    <script src="/assets/js/vendor-all.min.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/pcoded.min.js"></script>
<?php 
$url = Url::current();

$this->registerJs(<<<JS

document.getElementsByTagName("a.navlink[href='{$url }']").classList.add('activeyes');

JS
);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
