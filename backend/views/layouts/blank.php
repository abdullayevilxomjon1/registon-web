<?php
use backend\assets\BlankAsset;
use yii\helpers\Html;

BlankAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta name="begin" content="بسم الله الرحمن الرحيم">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<!-- Favicon icon -->
	<link rel="icon" href="/assets/images/favicon.svg" type="image/x-icon">
	
	<!-- fontawesome icon -->
	<link rel="stylesheet" href="/assets/fonts/fontawesome/css/fontawesome-all.min.css">
	<!-- animation css -->
	<link rel="stylesheet" href="/assets/plugins/animation/css/animate.min.css">

	<!-- vendor css -->
	<link rel="stylesheet" href="/assets/css/style.css">
</head>
<body> 
<!-- [ signin-img ] start --> 
<?php $this->beginBody() ?>
<div class="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch">
	<div class="row align-items-center w-100 align-items-stretch bg-white">
		<div class="col-md-4 align-items-stret h-100 ad-flex justify-content-center">			
			<div class="auth-content">
				<a href="/"><img src="/assets/images/registon-logo.png" alt="" class="img-fluid mb-4"></a>
				 <?= $content ?>
			</div>
		</div>
		<div class="d-none d-lg-flex col-md-8 aut-bg-img d-md-flex justify-content-center">

		</div>		
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
