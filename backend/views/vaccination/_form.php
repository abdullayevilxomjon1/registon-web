<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vaccination-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'vilsoato')->textInput() ?>

    <?= $form->field($model, 'tumsoato')->textInput() ?>

    <?= $form->field($model, 'uchid')->textInput() ?>

    <?= $form->field($model, 'vid')->textInput() ?>

    <?= $form->field($model, 'vresid')->textInput() ?>

    <?= $form->field($model, 'vvilid')->textInput() ?>

    <?= $form->field($model, 'vtumid')->textInput() ?>

    <?= $form->field($model, 'animalid')->textInput() ?>

    <?= $form->field($model, 'animalinfo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'animalbirka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vac_all')->textInput() ?>

    <?= $form->field($model, 'owner_type')->textInput() ?>

    <?= $form->field($model, 'owner_pinfl')->textInput() ?>

    <?= $form->field($model, 'owner_tin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'owner_info')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
