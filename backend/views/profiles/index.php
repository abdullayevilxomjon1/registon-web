<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вилоят аптека мудирлари';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profiles-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'pinfl',				
            'full_name',						
			[
				'attribute'=>'vil',
				'label' => Yii::t('app', 'Вилоят'),	
				'filter' => false,				
				'content'=>function($data){
					if($data->account_type == 1){
						if($data->vil > 0){
							return $data->vilcode['Name']." ".$data->accounttype['name'];
						}else{
							return "Республика ".$data->accounttype['name'];
						}
					}
					else{
						return $data->accounttype['name'];
					}
					
				},
			],
						'serial',
            //'data:ntext',            
            'phone',
            //'region',
            //'uchastka',            
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Ҳолати'),	
				'filter' => false,				
				'content'=>function($data){
					if($data->user->status == 10){						
						return "Актив";						
					}
					else{
						return "Блокланган";
					}
					
				},
			],
			[
				'attribute'=>'status',
				'label' => Yii::t('app', ''),	
				'filter' => false,				
				'content'=>function($data){
					if($data->user->status == 10){						
						return "<a href='/user/block?id=".$data->id."' class='btn btn-danger'><i class='feather icon-power'></i> Блоклаш </a>";						
					}
					else{
						return "<a href='' class='btn btn-success'><i class='feather icon-plus'></i> Активлаштириш</a>";						
					}
					
				},
			],
            //'position:ntext',
        ],
    ]); ?>


</div>
