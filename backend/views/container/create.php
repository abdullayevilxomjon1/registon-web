<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Container */

$this->title = 'Идиш киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pc-wizard-subtitle card" id="numwizard">
	<div class="card-body">
										<div class="row">
											<div class="col-sm-2">	
											</div>	
											<div class="col-lg-4">
											<h2><?= $this->title; ?></h2>
											</div>
											<div class="col-lg-4">
											
											</div>
										</div>
										<div class="row">
											<div class="col-sm-2">	
											</div>	
											<div class="col-lg-4">
											<?= $this->render('_form', [
												'model' => $model,
											]) ?>
											</div>
											<div class="col-lg-4">
											
											</div>
										</div>
	</div>
</div>
