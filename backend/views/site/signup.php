<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use app\models\Regions;
use app\models\Uchastkanames;
use app\models\Accounttype;

$this->title = 'Тизимга фойдаланувчини киритиш';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1> 

    <p>Тизимдан фойдаланиш чегараланган:</p>
            <?php $form = ActiveForm::begin(); ?>
			<?php echo $form->field($profiles, 'data')->hiddenInput()->label(false); ?>  	 
<div class="input-item" id="username">
				 <?php echo $form->field($model, 'username')->textInput(['placeholder' => "Логин"])->label("Логин"); ?>  	 
				 <div id="username_text" class="help-block help-block-error"></div>
</div>
 
<div class="row">
<div class="col-sm-6" id="pinfl"> <?= $form->field($profiles, 'pinfl')->widget(MaskedInput::className(), [
                    'name' => 'pinfl',
					'mask' => '99999999999999'
                ]) ?> <div id="pinfl_text" class="help-block help-block-error"></div></div>
<div class="col-lg-6" id="serial"><?= $form->field($profiles, 'serial')->widget(MaskedInput::className(), [
                    'name' => 'serial',
					'mask' => 'AA9999999'
                ]) ?><div id="serial_text" class="help-block help-block-error"></div></div>
	
</div> 

<div class="input-item" id="full-name">
				 <div id="fname"></div>
				 <?php echo $form->field($profiles, 'full_name')->hiddenInput()->label(false); ?>   
				 <div id="fullname_text" class="help-block help-block-error"></div>
</div> 
 
<div class="input-item" id="mail">
				 <?php echo $form->field($model, 'email')->textInput(['placeholder' => "Фойдаланувчи электрон манзили"])->label("Фойдаланувчи электрон манзили"); ?>	
	 <div id="mail_text" class="help-block help-block-error"></div>
</div>                    
                
<div class="input-item">
				 <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => "Калит сўзни киритинг"])->label("Калит сўзни киритинг"); ?>	
</div>

<div class="input-item">
				 <?php echo $form->field($model, 'password_repeat')->passwordInput(['placeholder' => "Калит сўзни қайта киритинг"])->label("Калит сўзни қайта киритинг"); ?>	
</div>



<div class="input-item">				 
				 <?= $form->field($profiles, 'phone')->widget(MaskedInput::className(), [
                    'name' => 'phone',
					'mask' => '+999999999999'
                ]) ?>
</div>          

<div class="input-item">   
						<?= $form->field($profiles, 'account_type')->dropDownList(ArrayHelper::map(Accounttype::find()->where(['status' => 1])->all(), 'id', 'name'), 
						 ['prompt'=>'Танланг'])->label('Тизимдаги вазифаси'); ?>	
</div>		 		
								
				
						
			<?= $form->field($profiles, 'vil')->dropDownList(ArrayHelper::map(Regions::find()->where(['between', 'code', '1700', '9999'])->all(), 'code', 'Name'), 
             ['prompt'=>'Вилоятни танланг',
              'onchange'=>'
                $.get( "/reference/reg?id="+$(this).val(), function(data){
                  $("select#profiles-region").html(data);
                });
            '])->label('Фойдаланувчи фаолият кўрсатадиган вилояти'); ?>				
				
				
				 
				 <?= 
					$form->field($profiles, 'region')->dropDownList(ArrayHelper::map(Regions::find()->where(['between', 'code', $profiles->vil."201", $profiles->vil."999"])->all(), 'code', 'Name'),
					['prompt'=>'Туманни танланг',
					'onchange'=>'
					$.get( "/reference/uchastka?id=" + $(this).val(), function(data){
					$("select#profiles-uchastka").html(data);
					});
            '])->label('Фойдаланувчи фаолият кўрсатадиган тумани') 
			?>
				
				<?= $form->field($profiles, 'uchastka')->dropDownList(['0' => 'Участкани танланг'])->label('Участка') ?>							
				
<div class="input-item">
				 <?php echo $form->field($profiles, 'position')->textInput(['placeholder' => "Фойдаланувчи мансаби"])->label("Фойдаланувчи мансаби"); ?>					 
</div> 
                <div class="form-group">
                    <?= Html::submitButton('Рўйхатдан ўтиш', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
			
<?php
$script = <<< JS
	$("#profiles-pinfl").change(function(){	
		var pinfl = this.value;
		var serial = document.getElementById("profiles-serial").value;			
		if(pinfl.length != 14) {			
					var element = document.getElementById("pinfl");		 
					element.classList.add("has-error");
					$("#pinfl_text").html('ЖШШИР нотўгри киритилган');								
		}
		else if(pinfl.indexOf('_') > -1){
					var element = document.getElementById("pinfl");		 
					element.classList.add("has-error");
					$("#pinfl_text").html('ЖШШИР нотўгри киритилган');
		}
		else{
			var element = document.getElementById("pinfl");		 
			element.classList.remove("has-error");	
			$("#pinfl_text").html('');	
			checkpinfl(pinfl, serial);					
		}
	});

	$("#profiles-serial").change(function(){	
		var serial = this.value;
		var pinfl = document.getElementById("profiles-pinfl").value;			
		if(serial.length != 9) {			
					var element = document.getElementById("serial");		 
					element.classList.add("has-error");
					$("#serial_text").html('Паспорт серияси ва рақами нотўгри киритилган');								
		}
		else if(serial.indexOf('_') > -1){
					var element = document.getElementById("serial");		 
					element.classList.add("has-error");
					$("#serial_text").html('Паспорт серияси ва рақами нотўгри киритилган');	
		}
		else{
			var element = document.getElementById("serial");		 
			element.classList.remove("has-error");	
			$("#serial_text").html('');	
			checkpinfl(pinfl, serial);					
		}
	});		
	
	function checkpinfl(pinfl, serial) {
	  $.get( "/reference/getfizdata?pinfl="+ pinfl + "&serial=" + serial, function(data){
		  var jsondata = JSON.parse(data);			 		  
		  if(jsondata.data.inf.document == serial){			
			var element = document.getElementById("full-name");		 
			element.classList.remove("has-error");
			$("#profiles-full_name").val(jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin + ' ' + jsondata.data.inf.patronym_latin);	
			$("#fname").html(jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin + ' ' + jsondata.data.inf.patronym_latin);	
			$("#profiles-data").val(data);					
			$("#fullname_text").html('');					
		  }
		  else{
			var element = document.getElementById("full-name");
			element.classList.add("has-error");						
			$("#fullname_text").html(data);
			$("#profiles-data").val('');					
			$("#fullname_text").html('Жисмоний шахс маълумотлари топилмади ёки нотўғри тақдим этилган');	
		  }		  
		  
				
        });	
	}


	$("#signupform-username").change(function(){	
		var value = this.value;
		  $.get( "/site/checklogin?username="+$(this).val(), function(data){
				if(data == 1){
					var element = document.getElementById("username");		 
					element.classList.remove("has-error");
					$("#username_text").html('');					
				}
				else{
					var element = document.getElementById("username");
					element.classList.add("has-error");	
					$("#username_text").html(data);
				}
        });		
	});	
	
	$("#signupform-email").change(function(){					
		var value = this.value;
		  $.get( "/site/checklogin?email="+$(this).val(), function(data){
				if(data == 1){
					var element = document.getElementById("mail");		 
					element.classList.remove("has-error");
					$("#mail_text").html('');					
				}
				else{
					var element = document.getElementById("mail");
					element.classList.add("has-error");	
					$("#mail_text").html(data);
				}
        });		
	});	 
	
	
	
JS;
$this->registerJs($script);
?>
</div>
