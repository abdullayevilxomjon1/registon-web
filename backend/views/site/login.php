<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Тизимга кириш';
?>
		
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1> 

    <p>Тизимдан фойдаланиш чегараланган:</p>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Логин') ?>

                <?= $form->field($model, 'password')->passwordInput()->label('Калит сўз') ?>

                <div class="form-group">
                    <?= Html::submitButton('Кириш', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
					<a href="/site/signup" class="btn btn-warning btn-block" style="color:#fff;">Рўйхатдан ўтиш</a>
                </div>
				

            <?php ActiveForm::end(); ?>
</div>