<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Uchastkanames */

$this->title = 'Update Uchastkanames: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Uchastkanames', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="uchastkanames-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
