<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Uchastkanames */

$this->title = 'Create Uchastkanames';
$this->params['breadcrumbs'][] = ['label' => 'Uchastkanames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uchastkanames-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
