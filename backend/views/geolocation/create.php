<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */

$this->title = 'Вакцинани киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Вакциналар', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#!'];
?>

 <div class="pcoded-main-container">
        <div class="pcoded-wrapper container">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-block">
                                    <div class="row align-items-center">
                                        <div class="col-md-12">
                                            <div class="page-header-title">
                                                <h5 class="m-b-10"><?= Html::encode($this->title) ?></h5>
                                            </div>
                                            <?= Breadcrumbs::widget([
												'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>",
												'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],												
											]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <!-- [ horizontal-layout ] start -->
                                <div class="col-sm-12">                                    
                                    <div class="card">                                                                                
                                            <?= $this->render('_form', [
												'model' => $model,
											]) ?>                                        
                                    </div>
                                </div>
                                <!-- [ horizontal-layout ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>