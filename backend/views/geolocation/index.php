<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VaccineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вакциналар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-header">
	<h1><?= Html::encode($this->title) ?></h1>
	<p>
        <?= Html::a('Вакциналарни киритиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="card-body">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'data_contarct:ntext',
            'data_vacine:ntext',
            'data_usage:ntext',
            //'code',
            //'user_recieved',
            //'data_series:ntext',
            //'recieved_amount',
            //'used_amount',
            //'user_created',
            //'created',
            //'updated',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>