<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
.first {
	
}
</style>

                               <div class="pc-wizard-subtitle card" id="detailswizard3v">
							   <ul class="nav justify-content-center card-header pb-0 nav-tabs">
                                        <li class="nav-item"><a href="#b-w3-tab1" class="nav-link active" data-toggle="tab"><span class="num-icon">1</span><span>
                                                    <h6>Шартнома</h6>Вакцина учун тузилган <br />шартнома маълумотлари
                                                </span></a></li>
                                        <li class="nav-item"><a href="#b-w3-tab2" class="nav-link" data-toggle="tab"><span class="num-icon">2</span><span>
                                                    <h6>Вакцина</h6>Вакцина тўғрисидаги <br />асосий маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a href="#b-w3-tab3" class="nav-link" data-toggle="tab"><span class="num-icon">3</span><span>
                                                    <h6>Профилактик эмлаш</h6>Профилактик эмлаш <br />тўғрисидаги маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a href="#b-w3-tab4" class="nav-link" data-toggle="tab"><span class="num-icon">4</span><span>
                                                    <h6>Мажбурий эмлаш</h6>Мажбурий эмлаш <br />тўғрисидаги маълумотлар
                                                </span></a></li>
										<li class="nav-item"><a href="#b-w3-tab5" class="nav-link" data-toggle="tab"><span class="num-icon">5</span><span>
                                                    <h6>Cерия рақамлари</h6>Вакциналарнинг <br/>серия рақамлари
                                                </span></a></li>																					
                                    </ul>
                                    <!--<ul class="nav justify-content-center card-header pb-0">
                                        <li class="nav-item active"><a href="#b-w3-tab1" class="nav-link" data-toggle="tab"><i class="feather icon-user"></i><span>
                                                    <h6>Шартнома</h6>Вакцина учун тузилган <br />шартнома маълумотлари
                                                </span></a></li>
                                        <li class="nav-item"><a href="#b-w3-tab2" class="nav-link" data-toggle="tab"><span>
                                                    <h6>Вакцина</h6>Вакцина тўғрисидаги <br />асосий маълумотлар
                                                </span></a></li>
										 <li class="nav-item"><a href="#b-w3-tab3" class="nav-link" data-toggle="tab"><span>
                                                    <h6>Профилактик эмлаш</h6>Профилактик эмлаш <br />тўғрисидаги маълумотлар
                                                </span></a></li>
										 <li class="nav-item"><a href="#b-w3-tab4" class="nav-link" data-toggle="tab"><span>
                                                    <h6>Мажбурий эмлаш</h6>Мажбурий эмлаш <br />тўғрисидаги маълумотлар
                                                </span></a></li>												
                                        <li class="nav-item"><a href="#b-w3-tab5" class="nav-link" data-toggle="tab"><span>
                                                    <h6>Cерия рақамлари</h6>Вакциналарнинг <br/>серия рақамлари
                                                </span></a></li>
                                        <li class="nav-item"><a href="#b-w3-tab6" class="nav-link" data-toggle="tab"><span>
                                                    <h6>Қўлланмалар</h6>Вакцинага доир <br/>қўлланмалар
                                                </span></a></li>
                                    </ul>-->
                                    <div class="card-body">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-8 col-md-10">
                                                <div class="tab-content">
                                                    <div class="tab-pane active show" id="b-w3-tab1">													
                                                        <?php $form = ActiveForm::begin(); ?>
															<div class="form-group row">
                                                                <label for="name" class="col-sm-3 col-form-label">Вакцина номи</label>
                                                                <div class="col-sm-9">
																	<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=>'form-control', 'placeholder' => 'Вакцина номи'])->label(false) ?>
                                                                </div>
                                                            </div>
															<div class="form-group row">
                                                                <label for="name" class="col-sm-3 col-form-label">Вакцина коди</label>
                                                                <div class="col-sm-9">
																	<?= $form->field($model, 'code')->textInput(['maxlength' => true, 'class'=>'form-control', 'placeholder' => 'Вакцина коди'])->label(false) ?>
                                                                </div>
                                                            </div>
															
															<div class="form-group row">
                                                                <label for="b-t-name" class="col-sm-3 col-form-label">Шартнома рақами ва санаси</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" placeholder="Шартнома рақами">
                                                                </div>
																<div class="col-sm-5">
																	<input class="form-control" type="date" value="2021-12-31" id="demo-date-only">                                                                    
                                                                </div>
                                                            </div>															
															<div class="form-group row">
                                                                <label for="b-t-name" class="col-sm-3 col-form-label">Етказиб берувчи</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control"  placeholder="Етказиб берувчи СТИРи ">
                                                                </div>
																<div class="col-sm-5">
																	<input type="text" class="form-control"  placeholder="Етказиб берувчи номи" disabled>                                                                    
                                                                </div>
                                                            </div>
															<div class="form-group row">
                                                                <label for="b-t-name" class="col-sm-3 col-form-label">Етказиб берувчи юридик манзили</label>                                                                
																<div class="col-sm-9">
																	<input type="text" class="form-control" placeholder="Етказиб берувчининг манзили" disabled>                                                                    
                                                                </div>
                                                            </div>

														<?php ActiveForm::end(); ?>
                                                    </div>
                                                    <div class="tab-pane" id="b-w3-tab2">
                                                        <form>
                                                            <div class="form-group row">
                                                                <label for="b-t-sate" class="col-sm-3 col-form-label">State</label>
                                                                <div class="col-sm-9">
                                                                    <select class="form-control" id="b-t-sate">
                                                                        <option>Select State</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="b-t-address" class="col-sm-3 col-form-label">Address</label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control" 	rows="3" spellcheck="false"></textarea>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="b-w3-tab3">
                                                        <form>
                                                            <div class="form-group row">
                                                                <label for="b-t-sate" class="col-sm-3 col-form-label">Year</label>
                                                                <div class="col-sm-9">
                                                                    <select class="form-control" id="b-t-sate">
                                                                        <option>Select Year</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="b-t-address" class="col-sm-3 col-form-label">Details</label>
                                                                <div class="col-sm-9">
                                                                    <textarea class="form-control" id="b-t-address" rows="3" spellcheck="false"></textarea>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="b-w3-tab4">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">Registration Done! . .</h5>
                                                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                                                            <div class="custom-control d-inline-block mb-3">
                                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                                <label class="custom-control-label" for="customCheck1">Subscribe Newslatter</label>
                                                            </div>
                                                        </form>
                                                    </div>
													 <div class="tab-pane" id="b-w3-tab5">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">Registration Done! . .</h5>
                                                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                                                            <div class="custom-control d-inline-block mb-3">
                                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                                <label class="custom-control-label" for="customCheck1">Subscribe Newslatter</label>
                                                            </div>
                                                        </form>
                                                    </div>
													<div class="tab-pane" id="b-w3-tab5">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">Registration Done! . .</h5>
                                                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                                                            <div class="custom-control d-inline-block mb-3">
                                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                                <label class="custom-control-label" for="customCheck1">Subscribe Newslatter</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="row justify-content-between btn-page">
                                                        <div class="col-sm-6">
                                                            <a href="#!" class="btn btn-outline-secondary button-previous">Олдинги бўлим</a>
                                                        </div>
                                                        <div class="col-sm-6 text-md-right">
                                                            <a href="#!" class="btn btn-primary button-next">Кейинги бўлим</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

   


