<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VaccineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vaccine-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'data_contarct') ?>

    <?= $form->field($model, 'data_vacine') ?>

    <?= $form->field($model, 'data_usage') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'user_recieved') ?>

    <?php // echo $form->field($model, 'data_series') ?>

    <?php // echo $form->field($model, 'recieved_amount') ?>

    <?php // echo $form->field($model, 'used_amount') ?>

    <?php // echo $form->field($model, 'user_created') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
