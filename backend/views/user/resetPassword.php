<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Калит сўзни янгилаш:<br/>';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="password-reset">

    <h1><?= $this->title ?></h1>

    <?= $this->render('_formreset', [
        'model' => $model,
    ]) ?>

</div>
