<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Profiles;


/* @var $this yii\web\View */
/* @var $model app\models\User */
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
$this->title = $profiles->full_name;;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
	
    	<table class="table table-striped table-bordered detail-view">
			<tr>
				<td>И.Ш.О</td>
				<td><?= $profiles->full_name; ?></td>										
			</tr>
			<tr>
				<td>ЖШШИР</td>
				<td><?= $profiles->pinfl; ?></td>										
			</tr>
			<tr>
				<td>Логин</td>
				<td><?= $model->username; ?></td>										
			</tr>			
			<tr>
				<td>Вазифаси</td>
				<td><?= $profiles->accounttype['name']; ?></td>										
			</tr>
			<tr>
				<td>Электрон почта</td>
				<td><?= $model->email; ?></td>										
			</tr>
			<tr>
				<td>Телефон рақами</td>
				<td><?= $profiles->phone; ?></td>								
			</tr>
		</table>
		<p>
			<?= Html::a('Маълумотларни янгилаш', ['update'], ['class' => 'btn btn-primary btn-block']) ?>       
		</p>

</div>
