<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


<div class="input-item">
				 <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => "Калит сўзни киритинг"])->label("Калит сўзни киритинг"); ?>	
</div>

<div class="input-item">
				 <?php echo $form->field($model, 'password_repeat')->passwordInput(['placeholder' => "Калит сўзни қайта киритинг"])->label("Калит сўзни қайта киритинг"); ?>	
</div>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-primary btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
