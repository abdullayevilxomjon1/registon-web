<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


<div class="input-item">				 
				 <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'name' => 'phone',
					'mask' => '+999999999999'
                ]) ?>
</div>    
<div class="input-item">
				 <?php echo $form->field($model, 'position')->textInput(['placeholder' => "Фойдаланувчи мансаби"])->label("Фойдаланувчи мансаби"); ?>					 
</div> 

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-primary btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
