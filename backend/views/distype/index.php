<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касаллик турлари';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distype-index">
 <div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="col-sm"></div>
	<div class="col-sm"></div>

	<div class="align-self-end">	
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Саралаш</button>
	</div>
<!--MODAL FORM -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Рўйхатни саралаш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														<?php echo $this->render('_search', ['model' => $searchModel]); ?>
												</div>
											</div>
										</div> 
</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            'name_uz',
            'atype',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
