<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distype */

$this->title = 'Create Distype';
$this->params['breadcrumbs'][] = ['label' => 'Distypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
