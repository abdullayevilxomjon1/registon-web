<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DiseasesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diseases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'name_ru') ?>

    <?= $form->field($model, 'name_uz') ?>

    <?= $form->field($model, 'virus') ?>

    <?php // echo $form->field($model, 'bakteria') ?>

    <?php // echo $form->field($model, 'protozoy') ?>

    <?php // echo $form->field($model, 'eraxno_entamologiya') ?>

    <?php // echo $form->field($model, 'gelmintalogiya') ?>

    <?php // echo $form->field($model, 'dgroup') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
