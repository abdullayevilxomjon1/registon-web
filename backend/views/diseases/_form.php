<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Diseases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diseases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'virus')->textInput() ?>

    <?= $form->field($model, 'bakteria')->textInput() ?>

    <?= $form->field($model, 'protozoy')->textInput() ?>

    <?= $form->field($model, 'eraxno_entamologiya')->textInput() ?>

    <?= $form->field($model, 'gelmintalogiya')->textInput() ?>

    <?= $form->field($model, 'dgroup')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
