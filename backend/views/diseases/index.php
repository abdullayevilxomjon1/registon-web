<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Distype;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiseasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касалликлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diseases-index">
 <div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="col-sm"></div>
	<div class="col-sm"></div>

	<div class="align-self-end">	
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Саралаш</button>
	</div>
<!--MODAL FORM -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Рўйхатни саралаш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														<?php echo $this->render('_search', ['model' => $searchModel]); ?>
												</div>
											</div>
										</div> 
</div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,  
		'filterModel' => $searchModel,		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			[
				'attribute'=>'type',
				'label' => Yii::t('app', 'Касаллик тури'),
				'filter' => Html::activeDropDownList($searchModel, 'type', ArrayHelper::map( Distype::find()->all(), 'id', 'name_uz'),['class'=>'form-control','prompt' => 'Танланг']),
				'content'=>function($data){																					
							return $data->distypea['name_uz'];  
						},
			],	            
            'name_ru',
            'name_uz',
            //'virus',
            //'bakteria',
            //'protozoy',
            //'eraxno_entamologiya',
            //'gelmintalogiya',
            //'dgroup',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
