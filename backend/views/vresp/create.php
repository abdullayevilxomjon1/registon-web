<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vresp */

$this->title = 'Create Vresp';
$this->params['breadcrumbs'][] = ['label' => 'Vresps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vresp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
