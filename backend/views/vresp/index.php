<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Vaccine;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
/* @var $this yii\web\View */
/* @var $searchModel app\models\VrespSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Республика миқёсида вакциналар кирими';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vresp-index">
	
	<div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="align-self-end">
	<?php if($profiles->account_type == 1 AND $profiles->vil == 0): ?>
	<h1><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Кирим қилиш</button><h1>
	<?php endif; ?>
	</div>
	</div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'emptyText' => 'маълумот топилмади',
		'layout' => "{items}\n{pager}",	
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'vid',
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => Html::activeDropDownList($searchModel, 'vid', ArrayHelper::map(Vaccine::find()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			[
				'attribute'=>'dognum',
				'label' => Yii::t('app', 'Шартнома'),				
				'content'=>function($data){
					return "№".$data->dognum." <br/>".date('d.m.Y', strtotime($data->dogdate))." й.";
				},
			],
			[
				'attribute'=>'serial',
				'label' => Yii::t('app', 'Серия рақами'),				
				'content'=>function($data){
					$str = "";
					if(!empty($data->serial)){
						$json = json_decode($data->serial, true);
						foreach($json as $val){
							$str .= $val['serial']."-".$val['num']."&nbsp;".$data->containername['name_uz']."<br/>";
						}
					}
					//$str = str_replace(",", "<br/>", $data->serial);
					return $str; 
				},
			],			
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Миқдори'),				
				'content'=>function($data){
					return $data->vac_all." та ".$data->containername['name_uz'];
				},
			],
			[
				'attribute'=>'container',
				'label' => Yii::t('app', 'Идиш'),				
				'content'=>function($data){
					return $data->containername['name_uz']." ".$data->vinfo['quantity']."&nbsp;".$data->unitname['name_uz'];
				},
			],
            //'status',
            //'user',
            'created',
            //'updated',

            [
				'attribute'=>'vac_all',
				'label' => Yii::t('app', ''),	
				'filter' => false,				
				'content'=>function($data){
					return "<a href='/vresp/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			],
        ],
    ]); ?>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Вакциналарни тарқатиш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														 <?= $this->render('_form', [
															'model' => $model,
														]) ?>
												</div>
											</div>
										</div> 
</div>
