<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vresp */

$this->title = 'Update Vresp: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vresps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vresp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
