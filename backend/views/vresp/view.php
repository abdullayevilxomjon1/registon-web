<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Vvil;
use yii\helpers\ArrayHelper;
use app\models\Regions;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
/* @var $this yii\web\View */
/* @var $model app\models\Vresp */

$this->title = $model->vaccine['name']." [".$model->vaccine['code']."]";
$this->params['breadcrumbs'][] = ['label' => 'Vresps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vresp-view">

    <h1><?= Html::encode($this->title) ?></h1>
	
<?php
$tarqatildi = Vvil::find()->select('SUM(vac_all) as sum')->where(['vresid' => $model->id])->asArray()->All();
$str = "";
if(!empty($model->serial)){
	$json = json_decode($model->serial, true);
	foreach($json as $val){
		$str .= $val['serial']."-".$val['num']."&nbsp;".$model->containername['name_uz']."<br/>";
	}
} 
?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),				
				'value'=> "<a href='/vaccine/view?id=".$model->vid."'>".$model->vaccine['name']." [".$model->vaccine['code']."]</a>", 
				'format' => 'HTML'
			],			
			[
				'attribute'=>'dognum',
				'label' => Yii::t('app', 'Шартнома'),				
				'value'=> date('d.m.Y', strtotime($model->dogdate))." й. №".$model->dognum, 
			],
			[
				'attribute'=>'vac_doz',
				'label' => Yii::t('app', 'Қадоқ'),				
				'value'=> $model->containername['name_uz']." ".$model->vinfo['quantity']." ".$model->unitname['name_uz'], 
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Миқдори'),				
				'value'=> $model->vac_all." та ".$model->containername['name_uz'], 
			],	
			[
				'attribute'=>'serial',
				'label' => Yii::t('app', 'Серия рақами'),				
				'value'=> $str,
				'format' => 'HTML'
			],			
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Тарқатилди'),				
				'value'=> $tarqatildi[0]['sum']." та ".$model->containername['name_uz'], 
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Қолдиқ'),				
				'value'=> $model->vac_all - $tarqatildi[0]['sum']." та ".$model->containername['name_uz'], 
			],
            // 'status',
            //'user',
            //'created',          
        ],
    ]) ?>
	<h2>Вакцина тарқатилиши тарихи</h2>
	 <p>        
	 	<?php if($profiles->account_type == 1 AND $profiles->vil == 0): ?>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Вилоятларга тарқатиш</button>
		<?php endif; ?>
    </p>
	<?php 
	$request = Yii::$app->request;
	$error = $request->get('error');
	if($error == 1){
		echo "<div class='alert alert-danger' style='margin:15px;'>Хатолик! тарқатилаётган вакцина умумий миқдори ".$model->vac_all." тадан ошмаслиги керак. <a href='/vresp/view?id=".$model->id."' class='close'><span>×</span></a></div>";		
	}
	?>
	    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'vid',            
			[
				'attribute'=>'vilsoato',
				'label' => Yii::t('app', 'Вилоят'),
				'filter' => Html::activeDropDownList($searchModel, 'vilsoato', ArrayHelper::map(Regions::find()->where(['>', 'code', '1000'])->andWhere(['<', 'code', '9999'])->all(), 'code', 'Name'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){					
					return $data->region['Name'];
				},
			],			
            //'vresid',
			[
				'attribute'=>'driver',
				'label' => Yii::t('app', 'Масъул шаҳс'),
				'filter' => false,
				'content'=>function($data){
					$jsondata = json_decode($data->driver, true);					
					return $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$data->car;
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'content'=>function($data)use($model){					
					return $data->vac_all." та ".$model->containername['name_uz'];
				},
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),				
				'content'=>function($data){					
					return $data->serialv; 
				},
			],
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),				
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			], 			           
            //'user',
            'created',
            //'updated',            
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', ''),	
				'filter' => false,				
				'content'=>function($data){
					return "<a href='/vvil/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			], 
        ],
    ]); ?> 
	
	
	</div>
<!--MODAL FORM -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Вакциналарни тарқатиш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														 <?= $this->render('_vvilform', [
															'model' => $vvil,
															'vresid' => $model->id,
															'vid' => $model->vid,
															'serial' => $model->serial,
														]) ?>
												</div>
											</div>
										</div> 
</div>

