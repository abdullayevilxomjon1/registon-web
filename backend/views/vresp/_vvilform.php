<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\models\Vvil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
<?php $form = ActiveForm::begin(['action' => '/vvil/create']); ?>
<?= $form->field($model, 'vid')->hiddenInput(['value' => $vid])->label(false) ?>
<?= $form->field($model, 'vresid')->hiddenInput(['value' => $vresid])->label(false) ?>														
	<div class="row">
		<div class="col-lg-4">	
		<?= $form->field($model, 'vilsoato')->dropDownList(ArrayHelper::map(Regions::find()->where(['>', 'code', '1000'])->andWhere(['<', 'code', '9999'])->all(), 'code', 'Name'), 
             ['prompt'=>'Вилоятни танланг']); ?>	
		</div>
		<div class="col-lg-4">																					
		<?= $form->field($model, 'vac_all')->textInput() ?>
		</div>	 
		<div class="col-lg-4">												
		<?php 
		$sr = array();		
		$json = json_decode($serial, true);
		foreach($json as $key => $val){			
			$sr[$key]['id'] = $val['serial'];
			$sr[$key]['name'] = $val['serial'];
		}		
		?>		
		<?= $form->field($model, 'serialv')->dropDownList(ArrayHelper::map($sr, 'id', 'name'), 
             ['prompt'=>'Серия рақамини танланг']); ?>
		</div>	
	</div>

	<div class="row">
		<div class="col-lg-4">	
		<?= $form->field($model, 'pinfl')->textInput() ?>
		</div>
		<div class="col-lg-4">																					
		<?= $form->field($model, 'serial')->textInput() ?>
		</div>	
		<div class="col-lg-4" id="driver">																					
		
		</div>	
	</div>	
	
	<div class="row">
		<div class="col-lg-6">	
		<?= $form->field($model, 'cartype')->textInput() ?>
		</div>
		<div class="col-lg-6">																					
		<?= $form->field($model, 'carnumber')->textInput() ?>
		</div>	
	</div>

    <?= $form->field($model, 'driver')->hiddenInput()->label(false) ?>    

    <?= $form->field($model, 'car')->hiddenInput()->label(false) ?>
	
	<div class="modal-footer">
	<?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-light" data-dismiss="modal">Ёпиш</button>													
</div>

    <?php ActiveForm::end(); ?>

</div></div>
<?php 
$this->registerJs(<<<JS
	$('#vvil-pinfl').on('change', function() { 
		var pinfl = document.getElementById("vvil-pinfl");
		var serial = document.getElementById("vvil-serial");
		document.getElementById("driver").innerHTML = '<br/>изланмоқда...';
		$("#vvil-driver").val();
	 		    
			 $.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){								 
                if(status == 'success'){
				  if(data == ''){
						$("#vvil-driver").val();					  
						document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади';
				  }
				  else{
					  $("#vvil-driver").val(data);
					  var jsondata = JSON.parse(data);							
					  var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;				  				  					  					
					  document.getElementById("driver").innerHTML = '<br/>'+text;						
					}	
				}
				else{
					$("#vvil-driver").val();	
					document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади...';
				}	
                });
	});
	
	$('#vvil-serial').on('change', function() { 
		var pinfl = document.getElementById("vvil-pinfl");
		var serial = document.getElementById("vvil-serial");
		document.getElementById("driver").innerHTML = '<br/>изланмоқда...';
		$("#vvil-driver").val();
	 		  
			 $.get("/reference/getfizdata?pinfl="+pinfl.value+"&serial="+serial.value, function(data, status){	
				if(status == 'success'){
				  if(data == ''){
						$("#vvil-driver").val();					  
						document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади';
				  }
				  else{
					  $("#vvil-driver").val(data);
					  var jsondata = JSON.parse(data);							
					  var text = jsondata.data.inf.surname_latin + ' ' + jsondata.data.inf.name_latin  + ' ' + jsondata.data.inf.patronym_latin;				  				  					  					
					  document.getElementById("driver").innerHTML = '<br/>'+text;						
					}	
				}
				else{
					$("#vvil-driver").val();
					document.getElementById("driver").innerHTML = '<br/>Маълумот топилмади...';
					
				}				
			});	
	});
	
	$('#vvil-cartype').on('change', function() { 
		var cartype = document.getElementById("vvil-cartype");
		var carnumber = document.getElementById("vvil-carnumber");
            $("#vvil-car").val(cartype + ", " + carnumber);            
	});
	
	$('#vvil-carnumber').on('change', function() { 
		var cartype = document.getElementById("vvil-cartype");
		var carnumber = document.getElementById("vvil-carnumber");
            $("#vvil-car").val(cartype.value + ", " + carnumber.value);            
	});
JS
);
?>
