<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Vaccine;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Vresp */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(['action' => '/vresp/create']); ?>
<div class="modal-body">														
<div class="row">
	<!-- sessions-section start -->	 
	<div class="col-lg-4">	
	<?= $form->field($model, 'vid')->dropDownList(ArrayHelper::map(Vaccine::find()->all(), 'id', 'name'), 
             ['prompt'=>'Вакцинани танланг',
              'onchange'=>'
                $.get("/vresp/vacemlash?id="+$(this).val(), function(data){
					if(data){
						$("select#vresp-vac_doz").html(data);	
					}
					else{
						$("select#vresp-vac_doz").html("");	
					}
                  
                });
            ']); ?>	
	</div>
	<div class="col-lg-4">																					
	<?= $form->field($model, 'dognum')->textInput(); ?>
	</div>
	<div class="col-lg-4">		
	<label class="control-label" for="vresp-dogdate">Шартнома санаси</label>	
	<input class="form-control" name="Vresp[dogdate]" type="date" value="<?= date('Y-m-d'); ?>" id="example-date-input">											
	</div>
	
	</div>  

<div class="row">
	<!-- sessions-section start -->	
	<div class="col-lg-4">
	<?= $form->field($model, 'vac_all')->textInput() ?>
	</div>
	<div class="col-lg-4">
	 <?= $form->field($model, 'vac_doz')->dropDownList([
    'prompt'=>'Вакцинани кадоғини танланг',   
]); ?>	
	</div>
	<div class="col-lg-4">																															
	</div>
	
</div>
<div class="row">
	<!-- sessions-section start -->	
	<div class="col-lg-8">  
	 <?= $form->field($model, 'serial')->widget(MultipleInput::className(), [    	
	'max' => 9,
    'columns' => [			 		
        [
            'name'  => 'serial',
            'title' => false,
			'enableError' => true,
            'options' => [
                'placeholder' => 'Серия',
            ]
        ],
		[
            'name'  => 'num',
            'title' => false,
			'enableError' => true,
            'options' => [
                'placeholder' => 'вакцина сони',
            ]
        ]		
    ]
 ])->label(false);
?>
	</div>
</div>	



	<div class="col-lg-4">																															
	</div>
</div>




<div class="modal-footer">
	<?= Html::submitButton('Кирим қилиш', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-light" data-dismiss="modal">Ёпиш</button>													
</div>

    <?php ActiveForm::end(); ?>


