<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Suspicion */

$this->title = 'Create Suspicion';
$this->params['breadcrumbs'][] = ['label' => 'Suspicions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suspicion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
