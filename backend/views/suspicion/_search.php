<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SuspicionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="suspicion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vil') ?>

    <?= $form->field($model, 'tum') ?>

    <?= $form->field($model, 'uch') ?>

    <?= $form->field($model, 'deseasid') ?>

    <?php // echo $form->field($model, 'animal') ?>

    <?php // echo $form->field($model, 'owner_pinfl') ?>

    <?php // echo $form->field($model, 'owner_tin') ?>

    <?php // echo $form->field($model, 'animal_type') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <?php // echo $form->field($model, 'user') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'labstatus') ?>

    <?php // echo $form->field($model, 'notified') ?>

    <?php // echo $form->field($model, 'animalid') ?>

    <?php // echo $form->field($model, 'animalbirka') ?>

    <?php // echo $form->field($model, 'oid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
