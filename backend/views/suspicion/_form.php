<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Suspicion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="suspicion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vil')->textInput() ?>

    <?= $form->field($model, 'tum')->textInput() ?>

    <?= $form->field($model, 'uch')->textInput() ?>

    <?= $form->field($model, 'deseasid')->textInput() ?>

    <?= $form->field($model, 'animal')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'owner_pinfl')->textInput() ?>

    <?= $form->field($model, 'owner_tin')->textInput() ?>

    <?= $form->field($model, 'animal_type')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'user')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'labstatus')->textInput() ?>

    <?= $form->field($model, 'notified')->textInput() ?>

    <?= $form->field($model, 'animalid')->textInput() ?>

    <?= $form->field($model, 'animalbirka')->textInput() ?>

    <?= $form->field($model, 'oid')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
