<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Suspicion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suspicions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="suspicion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vil',
            'tum',
            'uch',
            'deseasid',
            'animal:ntext',
            'owner_pinfl',
            'owner_tin',
            'animal_type',
            'created',
            'updated',
            'user',
            'status',
            'labstatus',
            'notified',
            'animalid',
            'animalbirka',
            'oid',
        ],
    ]) ?>

</div>
