<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vtum */

$this->title = 'Update Vtum: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vta', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vtum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
