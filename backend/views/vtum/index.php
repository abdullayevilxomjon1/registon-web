<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Regions;
use app\models\Vtum;
use app\models\Profiles;
use app\models\Vaccine;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
/* @var $this yii\web\View */
/* @var $searchModel app\models\VtumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Туманларга тарқатиш';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vtum-index">

   <div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="col-sm"></div>
	<div class="col-sm"></div>

	<div class="align-self-end">	
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Саралаш</button>
	</div>
<!--MODAL FORM -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Рўйхатни саралаш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														<?php echo $this->render('_search', ['model' => $searchModel]); ?>
												</div>
											</div>
										</div> 
</div>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'layout' => "{items}\n{pager}",	
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'vilsoato',
				'label' => Yii::t('app', 'Вилоят'),
				'filter' => false,
				'content'=>function($data){					
					return $data->region['Name'];
				},
			],
			[
				'attribute'=>'tumsoato',
				'label' => Yii::t('app', 'Туман'),
				'filter' => Html::activeDropDownList($searchModel, 'tumsoato', ArrayHelper::map(Regions::find()->where(['between', 'code', $profiles->vil."201", $profiles->vil."399"])->all(), 'code', 'Name'),['class'=>'form-control','prompt' => 'Барчаси']),	
				'content'=>function($data){					
					return $data->tuman['Name'];
				},
			],
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => Html::activeDropDownList($searchModel, 'vid', ArrayHelper::map(Vaccine::find()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Кирим рақами ва санаси'),
				'filter' => false,
				'content'=>function($data){					
					return $data->created." санасидаги № Т-".$data->id." рақамли";
				},
			],
			[
				'attribute'=>'driver',
				'label' => Yii::t('app', 'Етказиб бериш'),
				'filter' => false,
				'content'=>function($data){
					$jsondata = json_decode($data->driver, true);					
					return $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$data->car;
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вақцина миқдори'),							
				'filter' => false,
				'content'=>function($data){					
					return $data->vac_all." та";
				},
			], 
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle;'>Етказиб берилмоқда</span> <img src='/assets/images/car.png' width='35'>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			], 
			[
				'attribute'=>'name1', 
				'label' => Yii::t('app', ' '), 				
				'content'=>function($data){
					return "<a href='/vtum/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			],
        ],
    ]); ?>


</div>
