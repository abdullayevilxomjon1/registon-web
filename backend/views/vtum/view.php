<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Vuch;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
/* @var $this yii\web\View */
/* @var $model app\models\Vtum */

$this->title = $model->vaccine['name']." [".$model->vaccine['code']."]";
$this->params['breadcrumbs'][] = ['label' => 'Vta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vtum-view">
  
<?php 
$jsondata = json_decode($model->driver, true); 
if($model->status > 1){
	$jsondatareciever = json_decode($model->user_recieved, true);
	$reciever = $jsondatareciever['data']['inf']['surname_latin']." ".$jsondatareciever['data']['inf']['name_latin']." ".$jsondatareciever['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondatareciever['data']['inf']['document']."]"."<br/>";	
}
else{
	$reciever = "-";
}
?>
<?php 
switch($model->status){
						case 1: 
						$status = "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						if($profiles->account_type == 1 AND $profiles->region == $model->tumsoato){
							$status .= Html::a('Қабул қилиш', ['accept', 'id' => $model->id], ['class' => 'btn btn-success']);
						}
						break;	
						case 2: 
						$status = "Қабул қилинди";
						break;	
}	
?>
<?php
$tarqatildi = Vuch::find()->select('SUM(vac_all) as sum')->where(['vtum' => $model->id])->asArray()->All();
if(!isset($tarqatildi[0]['sum'])){
	$tarqatildi[0]['sum'] = 0;
}
?>
 <h1><?= Html::encode($this->title) ?> <span class="btn btn-primary">Қолдиқ: <?= $model->vac_all - $tarqatildi[0]['sum']; ?></span></h1>
   <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',            
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),				
				'value'=> "<a href='/vaccine/view?id=".$model->vid."'>".$model->vaccine['name']." [".$model->vaccine['code']."]</a>", 
				'format' => 'HTML'
			],	
			[
				'attribute'=>'vilsoato',
				'label' => Yii::t('app', 'Қабул қилувчи вилоят'),				
				'value'=> $model->region['Name'], 				
			],	
			[
				'attribute'=>'tumsoato',
				'label' => Yii::t('app', 'Қабул қилувчи Туман'),				
				'value'=> $model->tuman['Name'], 				
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Юбориш рақами ва санаси'),				
				'value'=> $model->created." санадаги № ".$model->id." жўнатма", 				
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Масъул шахс'),				
				'value'=> $jsondata['data']['inf']['surname_latin']." ".$jsondata['data']['inf']['name_latin']." ".$jsondata['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondata['data']['inf']['document']."]"."<br/>".$model->car, 				
				'format' => 'HTML',
			],
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Ҳолати'),				
				'value'=> $status, 	
				'format' => 'HTML',				
			],
			[
				'attribute'=>'vresid',
				'label' => Yii::t('app', 'Қабул қилган шахс'),				
				'value'=> $reciever, 				
				'format' => 'HTML',
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),				
				'value'=> $model->serialv, 								
			],
            [
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'value'=> $model->vac_all." та ".$vresp->containername['name_uz'], 								
			],						
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Тарқатилди'),				
				'value'=> $tarqatildi[0]['sum']." та ".$vresp->containername['name_uz'], 
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Қолдиқ'),				
				'value'=> $model->vac_all - $tarqatildi[0]['sum']." та ".$vresp->containername['name_uz'], 
			],
        ],
    ]) ?>
<h2>Вакцина тарқатилиши тарихи</h2>
	<?php if($model->status == 2 AND $profiles->account_type == 1 AND $profiles->region == $model->tumsoato):?>
	<p>        
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Участкаларга тарқатиш</button>
    </p>
	<?php 
	$request = Yii::$app->request;
	$error = $request->get('error');
	if($error == 1){
		echo "<div class='alert alert-danger' style='margin:15px;'>Хатолик! тарқатилаётган вакцина умумий миқдори ".$model->vac_all." тадан ошмаслиги керак. <a href='/vvil/view?id=".$model->id."' class='close'><span>×</span></a></div>";		
	}
	?>
	<?php endif; ?>
	
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			[
				'attribute'=>'uchastka',
				'label' => Yii::t('app', 'Участка'),
				'filter' => false,
				'content'=>function($data){					
					return $data->uch['NAME_UZ'];
				},
			],
			[
				'attribute'=>'vid',
				'label' => Yii::t('app', 'Вакцина'),
				'filter' => false,
				'content'=>function($data){					
					return $data->vaccine['name']." [".$data->vaccine['code']."]";
				},
			],
			[
				'attribute'=>'serialv',
				'label' => Yii::t('app', 'Серия рақами'),
				'filter' => false,
				'content'=>function($data){					
					return $data->serialv; 
				},
			],
			[
				'attribute'=>'vac_all',
				'label' => Yii::t('app', 'Вакцина миқдори'),				
				'content'=>function($data){					
					return $data->vac_all." та";
				},
			],							
			[
				'attribute'=>'status',
				'label' => Yii::t('app', 'Холати'),				
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){
						case 1: 
						return "<span style='vertical-align:middle; color:red;'>қабул қилинмаган</span>";
						break;	
						case 2: 
						return "Қабул қилинди";
						break;						
					}					
				},
			],	
			[
				'attribute'=>'user_recieved',
				'label' => Yii::t('app', 'Қабул қилган шахс'),				
				'filter' => false,
				'content'=>function($data){					
					switch($data->status){							
						case 2: 
						$jsondatareciever = json_decode($data->user_recieved, true);
						$reciever = $jsondatareciever['data']['inf']['surname_latin']." ".$jsondatareciever['data']['inf']['name_latin']." ".$jsondatareciever['data']['inf']['patronym_latin']."<br/>[Паспорт рақами: ".$jsondatareciever['data']['inf']['document']."]"."<br/>";	
						return $reciever;
						break;						
					}					
				},
			],
		
            //'vilsoato',
            //'tumsoato',
            //'driver:ntext',
            //'vac_all',
            //'car:ntext',
            //'status',
            //'user',
			[
				'attribute'=>'created',
				'label' => Yii::t('app', 'Тарқатилди'),				
				'filter' => false,
				'content'=>function($data){					
					return $data->created;			
				},
			],
            //'updated',
            
        ],
    ]); ?>
</div>
	<!--MODAL FORM --> 
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Вакциналарни тарқатиш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>												
														 <?= $this->render('_uchform', [
															'model' => $uch,															
															'vid' => $model->vid,															
															'vvid' => $model->vvid,															
															'vtum' => $model->id,															
															'vresid' => $model->vresid,															
															'vilsoato' => $model->vilsoato,															
															'tumsoato' => $model->tumsoato,	
															'serial' => $model->serialv,															
														]) ?> 														
												</div>
											</div>
										</div> 
