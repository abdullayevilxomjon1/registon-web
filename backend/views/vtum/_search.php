<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VtumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vtum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // $form->field($model, 'id') ?>

    <?= $form->field($model, 'vid') ?>

    <?= $form->field($model, 'tumsoato') ?>

    <?= $form->field($model, 'vresid') ?>

    <?= $form->field($model, 'driver') ?>

    <?php // echo $form->field($model, 'vac_all') ?>

    <?php // echo $form->field($model, 'car') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'user') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'updated') ?>

    <div class="form-group">
        <?= Html::submitButton('Ищлаш', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Тозалаш', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
