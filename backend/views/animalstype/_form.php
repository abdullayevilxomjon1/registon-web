<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Animalstype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="animalstype-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NAME_UZ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NAME_RU')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
