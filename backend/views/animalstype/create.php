<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Animalstype */

$this->title = 'Create Animalstype';
$this->params['breadcrumbs'][] = ['label' => 'Animalstypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="animalstype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
