<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vacemlash */

$this->title = 'Create Vacemlash';
$this->params['breadcrumbs'][] = ['label' => 'Vacemlashes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacemlash-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
