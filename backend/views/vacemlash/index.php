<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VacemlashSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vacemlashes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacemlash-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vacemlash', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vid',
            'animal',
            'ege_from',
            'ege_to',
            //'emlash_type',
            //'emlash_period:ntext',
            //'quantity',
            //'unit',
            //'imun_on_date',
            //'imun_for_time',
            //'created',
            //'updated',
            //'user',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
