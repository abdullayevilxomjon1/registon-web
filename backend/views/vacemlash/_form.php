<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacemlash */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacemlash-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vid')->textInput() ?>

    <?= $form->field($model, 'animal')->textInput() ?>

    <?= $form->field($model, 'ege_from')->textInput() ?>

    <?= $form->field($model, 'ege_to')->textInput() ?>

    <?= $form->field($model, 'emlash_type')->textInput() ?>

    <?= $form->field($model, 'emlash_period')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'unit')->textInput() ?>

    <?= $form->field($model, 'imun_on_date')->textInput() ?>

    <?= $form->field($model, 'imun_for_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'updated')->textInput() ?>

    <?= $form->field($model, 'user')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
