<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vacemlash */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vacemlashes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vacemlash-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vid',
            'animal',
            'ege_from',
            'ege_to',
            'emlash_type',
            'emlash_period:ntext',
            'quantity',
            'unit',
            'imun_on_date',
            'imun_for_time',
            'created',
            'updated',
            'user',
        ],
    ]) ?>

</div>
