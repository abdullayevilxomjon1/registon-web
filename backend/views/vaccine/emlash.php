<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Animalstype;
use app\models\Unit;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */

$this->title = 'Эмлаш тартибини шакллантириш';
$this->params['breadcrumbs'][] = ['label' => 'Vaccines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pc-wizard-subtitle card" id="numwizard">
                                    <ul class="nav justify-content-center card-header pb-0 nav-tabs">
                                        <li class="nav-item"><a href="#b-w6-tab1" class="nav-link active"><span class="num-icon">1</span><span>
                                                    <h6>Вакцина киритиш</h6>Асосий маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link active" ><span class="num-icon">2</span><span>
                                                    <h6>Вакцина киритиш</h6>Қўшимча маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link active"><span class="num-icon">3</span><span>
                                                    <h6>Эмлаш</h6>Эмлаш тартибини шакллантириш
                                                </span></a></li>                                        
										<li class="nav-item"><a href="#b-w6-tab5" class="nav-link" data-toggle="tab"><span class="num-icon">5</span><span>
                                                    <h6>Қўлланма</h6>Қўлланма матни
                                                </span></a></li>
                                    </ul>
									
                                    <div class="card-body">

									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-5">
										<h2><?= $this->title; ?></h2>
										</div>
										<div class="col-lg-1">
										<br/>
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Қўшиш</button>										
										</div>
										<div class="col-lg-2">	
																				
										</div>
									</div>
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8">
										<?php if($data->getCount() > 0) :?>
										    <?= GridView::widget([
											'dataProvider' => $data,											
											'columns' => [
												['class' => 'yii\grid\SerialColumn'],
												[
													'attribute'=>'emlash',
													'label' => Yii::t('app', 'Эмлаш тури'),													
													'content'=>function($data){
														switch($data->emlash_type){
															case 0: return 'Профилактика';
															break;
															case 1: return 'Мажбурий';
															break;
														}
													},
												],
												[
													'attribute'=>'vac',
													'label' => Yii::t('app', 'Вакцина'),													
													'content'=>function($data){
														return $data->vaccine['name']." [".$data->vaccine['code']."]";
													},
												],	
												[
													'attribute'=>'vac',
													'label' => Yii::t('app', 'Ҳайвон'),													
													'content'=>function($data){
														return $data->animaltype['NAME_UZ'];
													},
												],
												[
													'attribute'=>'age',
													'label' => Yii::t('app', 'Эмлаш ёши'),													
													'content'=>function($data){
														if($data->ege_from == $data->ege_to){
															return $data->ege_from." ой";
														}
														else{
															return $data->ege_from." - ".$data->ege_to." ой";	
														}
													},
												],																														
												[
													'attribute'=>'quan',
													'label' => Yii::t('app', 'Эмлаш'),													
													'content'=>function($data){
														return $data->quantity." ".$data->unitname['name_uz'];
													},
												],
												[
													'attribute'=>'imun',
													'label' => Yii::t('app', 'Иммунитет'),																			
													'content'=>function($data){														
															return "Иммунитет шаклланиши: ".$data->imun_on_date." кун <br/ > Иммунитет даври: ".$data->imun_for_time." ой";	
													},
												],
												[
													'attribute'=>'imun',
													'label' => Yii::t('app', 'Ревакцинация'),																			
													'content'=>function($data){
														if($data->revacvination ==  1){															
															return $data->revac_date." кун ".$data->revac_quantity." ".$data->unitname['name_uz'];	
														}
														else{
															return "мавжуд эмас";
														}
														
													},
												],
												//'created',
												//'updated',
												//'user',
											],
										]); ?>
										<a href="/vaccine/doc?id=<?= $id; ?>" class="btn btn-primary">Кейинги босқичга ўтиш</a>
										<?php endif; ?>
										</div>
										<div class="col-lg-2">	
																				
										</div>
									</div>
									
                                        	
                                    </div>
                                </div>
								
										<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Эмлаш тартибини киритиш</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
													<div class="modal-body">														
														<!-- BEGIN -->
														<?php $form = ActiveForm::begin(); ?>
														<?= $form->field($model, 'emlash_type')->dropDownList([
															'0' => 'Профилактика',
															'1' => 'Мажбурий',    
														]); ?>
														<div class="row">
														<!-- sessions-section start -->
														<?= $form->field($model, 'vid')->HiddenInput()->label(false) ?>												
														<div class="col-lg-4">																					
														<?= $form->field($model, 'animal')->dropDownList(ArrayHelper::map(Animalstype::find()->All(), 'id', 'NAME_UZ')); ?>
														</div>
														<div class="col-lg-4">																						
														<?= $form->field($model, 'ege_from')->textInput() ?>												
														</div>
														<div class="col-lg-4">
														<?= $form->field($model, 'ege_to')->textInput() ?>
														</div>
														</div>
														<?php 
														$list = [1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель', 5 => 'Май', 6 => 'Июнь', 7 => 'Июль', 8 => 'Август', 9 => 'Сентябрь', 10 => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь'];		
														?>
														<div class="row">	
															<div class="col-lg-12">	 
															<?= $form->field($model, 'emlash_period')->checkboxlist($list);?> 
															</div>	
														</div> 	
													<div class="row">
														<div class="col-lg-2">	
															<?= $form->field($model, 'quantity')->textInput() ?>														
														</div>	
														<div class="col-lg-2">	
															 <?= $form->field($model, 'unit')->dropDownList(ArrayHelper::map(Unit::find()->All(), 'id', 'name_uz')); ?>																
														</div>
														<div class="col-lg-4">	 
															<?= $form->field($model, 'imun_on_date')->textInput() ?>
														</div>
														<div class="col-lg-4">
															<?= $form->field($model, 'imun_for_time')->textInput(['maxlength' => true]) ?>  
														</div>
													</div>   

													<div class="row">														
														<div class="col-lg-4">																					
														<?= $form->field($model, 'revacvination')->dropDownList([
															'0' => 'Йўқ',
															'1' => 'Ҳа',    
														]); ?>
														</div>
														<div class="col-lg-4">																						
														<?= $form->field($model, 'revac_date')->textInput() ?>												
														</div>
														<div class="col-lg-4">
														<?= $form->field($model, 'revac_quantity')->textInput() ?>
														</div>
														</div>												
														<!-- END -->
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-light" data-dismiss="modal">Ёпиш</button>
														<?= Html::submitButton('Сақлаш', ['class' => 'btn btn-primary']) ?>
													</div>
													<?php ActiveForm::end(); ?>
												</div>
											</div>
										</div>
										
									

  
  

