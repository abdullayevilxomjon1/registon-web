	<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */

$this->title = 'Қадоқлаш маълумотларини киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Vaccines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pc-wizard-subtitle card" id="numwizard">
                                    <ul class="nav justify-content-center card-header pb-0 nav-tabs">
                                        <li class="nav-item"><a href="#b-w6-tab1" class="nav-link active"><span class="num-icon">1</span><span>
                                                    <h6>Вакцина киритиш</h6>Асосий маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link active" ><span class="num-icon">2</span><span>
                                                    <h6>Қадоқлашни киритиш</h6>Қадоқлаш маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link"><span class="num-icon">3</span><span>
                                                    <h6>Эмлаш</h6>Рўйхатни шакллантириш
                                                </span></a></li>                                        
										<li class="nav-item"><a href="#b-w6-tab5" class="nav-link" data-toggle="tab"><span class="num-icon">4</span><span>
                                                    <h6>Қўлланма</h6>Қўлланма матни
                                                </span></a></li>
                                    </ul>
									
                                    <div class="card-body">

									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8">
										<h2><?= $this->title; ?></h2>
										</div>
										<div class="col-lg-2">
										
										</div>
									</div>
									
                                        	<?= $this->render('_infoform', [
												'model' => $model,
											]) ?>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8"><br/>
										<?php if($data->getCount() > 0) :?>
										<?= GridView::widget([
											'dataProvider' => $data,											
											'columns' => [
												['class' => 'yii\grid\SerialColumn'],
												[
													'attribute'=>'container',
													'label' => Yii::t('app', 'Идиш'),													
													'content'=>function($data){
														return $data->containern['name_uz'];
													},
												],
												[
													'attribute'=>'quantity',
													'label' => Yii::t('app', 'Идиш ҳажми'),													
													'content'=>function($data){
														if($data->unit == 1){															
															$ml = $data->quantity*$data->doztomil;
															return $data->quantity." ".$data->unitn['name_uz']." ёки ".$ml." мл.";
														}
														else{
															return $data->quantity." ".$data->unitn['name_uz'];
														}
														
													},
												],													
												[
													'attribute'=>'retreat',
													'label' => Yii::t('app', 'Табиий йўқотиш'),													
													'content'=>function($data){
														return $data->retreat." % ";
													},
												],												
											],
										]); ?>
										
										<a href="/vaccine/emlash?id=<?= $id; ?>" class="btn btn-success">Кейиги босқичга ўтиш</a>
										<?php endif; ?>
										</div>
										<div class="col-lg-2">
										
										</div>
									</div>
									
									
											
                                    </div>
									
									
                                </div>



  
  

