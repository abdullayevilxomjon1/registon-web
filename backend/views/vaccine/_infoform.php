<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Unit;
use app\models\Container;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-2">	
	 <?= $form->field($model, 'container')->dropDownList(ArrayHelper::map(Container::find()->All(), 'id', 'name_uz')); ?>		
	</div>
	<div class="col-lg-2">
	<?= $form->field($model, 'quantity')->textInput(['maxlength' => true]) ?>
	</div>	
	<div class="col-lg-1">
	<?= $form->field($model, 'doztomil')->textInput(['maxlength' => true]) ?>
	</div>	
	<div class="col-ls-2">	
		 <?= $form->field($model, 'unit')->dropDownList(ArrayHelper::map(Unit::find()->All(), 'id', 'name_uz')); ?>		
	</div>
	<div class="col-lg-2">
	<?= $form->field($model, 'retreat')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-ls-2"><label class="control-label">&nbsp;</label><br/>
		 <?= Html::submitButton('Давом этиш', ['class' => 'btn btn-success']) ?>
	</div>	
</div>

<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-8">
	 
	</div>	
</div>    
    <?php ActiveForm::end(); ?>


