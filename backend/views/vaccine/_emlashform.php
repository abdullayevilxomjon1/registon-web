<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Unit;
use app\models\Container;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-3">	
	 <?= $form->field($model, 'container')->dropDownList(ArrayHelper::map(Container::find()->All(), 'id', 'name_uz')); ?>		
	</div>
	<div class="col-lg-3">
	<?= $form->field($model, 'quantity')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-lg-2">	
		 <?= $form->field($model, 'unit')->dropDownList(ArrayHelper::map(Unit::find()->All(), 'id', 'name_uz')); ?>		
	</div>
	
</div>

<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-4">	 
	<?= $form->field($model, 'retreat')->textInput(['maxlength' => true]) ?>	 
	</div>
	<div class="col-lg-4">
	
	</div>
</div>    


<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-8">
	 <?= Html::submitButton('Давом этиш', ['class' => 'btn btn-success']) ?>
	</div>	
</div>    
    <?php ActiveForm::end(); ?>


