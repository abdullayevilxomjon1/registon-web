<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */

$this->title = 'Қўлланмани киритиш';
$this->params['breadcrumbs'][] = ['label' => 'Vaccines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('https://cdn.tiny.cloud/1/6sy437qwb3mshwejv825dz5r1mt7leqcuvbiytmo6ouif86d/tinymce/5/tinymce.min.js');
?>
<?php
    $this->registerJs(
    "tinymce.init({selector:'textarea'});"
    );
  ?>

<div class="pc-wizard-subtitle card" id="numwizard">
                                   <ul class="nav justify-content-center card-header pb-0 nav-tabs">
                                        <li class="nav-item"><a href="#b-w6-tab1" class="nav-link active"><span class="num-icon">1</span><span>
                                                    <h6>Вакцина киритиш</h6>Асосий маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link active" ><span class="num-icon">2</span><span>
                                                    <h6>Вакцина киритиш</h6>Қўшимча маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link active"><span class="num-icon">3</span><span>
                                                    <h6>Эмлаш</h6>Эмлаш тартибини шакллантириш
                                                </span></a></li>                                        
										<li class="nav-item"><a class="nav-link active"><span class="num-icon">4</span><span>
                                                    <h6>Қўлланма</h6>Қўлланма матни
                                                </span></a></li>
                                    </ul>
									
                                    <div class="card-body">

									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-4">
										<h2><?= $this->title; ?></h2>
										</div>
										<div class="col-lg-4">
										
										</div>
									</div>
									
                                        	<?= $this->render('_docform', [
												'model' => $model,
											]) ?>
                                    </div>
                                </div>



  
  

