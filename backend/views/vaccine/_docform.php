<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Countries;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>

<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-8">	 
	 <?= $form->field($model, 'document')->textArea(['rows' => '24']) ?>	
	</div>
	<div class="col-lg-2">
	
	</div>
</div>    


<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-8">
	 <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
	</div>	
</div>    
    <?php ActiveForm::end(); ?>


