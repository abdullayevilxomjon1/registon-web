<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Countries;

/* @var $this yii\web\View */
/* @var $model app\models\Vaccine */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-4">
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-lg-4">
	<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-4">	 
	 <?= $form->field($model, 'country')->dropDownList(ArrayHelper::map(Countries::find()->All(), 'CODE', 'NAME')); ?>			
	</div>
	<div class="col-lg-4">
	<?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>
	</div>
</div>    


<div class="row">
	<div class="col-sm-2">	
	</div>	
	<div class="col-lg-8">
	 <?= Html::submitButton('Давом этиш', ['class' => 'btn btn-success']) ?>
	</div>	
</div>    
    <?php ActiveForm::end(); ?>


