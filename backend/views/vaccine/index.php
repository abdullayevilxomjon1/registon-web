<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);

/* @var $this yii\web\View */
/* @var $searchModel app\models\VaccineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вакциналар реестри';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vaccine-index">

    

    <p class="left">
        
    </p>
	
	<div class="row">
	<div class="col-sm">
	<h3><?= Html::encode($this->title) ?></h3>
	</div>
	<div class="col-sm"></div>
	<div class="col-sm"></div>
<?php if($profiles->account_type == 10): ?>
	<div class="align-self-end">
	<h1><?= Html::a('Вакцина киритиш', ['create'], ['class' => 'btn btn-success']) ?><h1>
	</div>
<?php endif; ?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'emptyText' => 'Маълумот топилмади',
		'layout' => "{items}\n{pager}",	
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],			
            'name',			
            'code',
			[
				'attribute'=>'country',
				'label' => Yii::t('app', 'Ишлаб чиқарувчи давлат'),
				'filter' => Html::activeDropDownList($searchModel, 'country', ArrayHelper::map(Countries::find()->all(), 'CODE', 'NAME'),['class'=>'form-control','prompt' => 'Барчаси']),
				'content'=>function($data){
					return $data->countries['NAME'];
				},
			],
            'manufacturer',
            //'user',
			[														
				'attribute' => 'created',	
				'filter'=>false,				
				'label' => Yii::t('app', 'Яратилди'),
				'content'=>function($data){
					return $data->created;			
				},
			], 
            //'updated',
            [														
				'attribute' => 'status',	
				'filter'=>false,				
				'label' => Yii::t('app', 'Ҳолати'),
				'content'=>function($data){
					switch($data->status){
						case 0:
							return "Шакллантирилди";
						case 1:
							return "Шакллантирилмоқда";	
						   
					}					
				},
			],       
			[
				'attribute'=>'name1',
				'label' => Yii::t('app', ' '),				
				'content'=>function($data){
					return "<a href='/vaccine/view?id=".$data->id."' class='btn btn-success'>Танишиш</a>";
				},
			],            
        ],
    ]); ?>


</div>
