<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Profiles;
$profiles = Profiles::findOne(Yii::$app->user->identity->id);
$this->title = $model->name." (".$model->code.")";
$this->params['breadcrumbs'][] = ['label' => 'Vaccines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pc-wizard-subtitle card" id="numwizard">
                                    <ul class="nav justify-content-center card-header pb-0 nav-tabs">
                                        <li class="nav-item"><a class="nav-link active"><span class="num-icon">1</span><span>
                                                    <h6>Вакцина киритиш</h6>Асосий маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link  <?php if($containerProvider->getTotalCount() > 0):?>active<?php endif; ?>" ><span class="num-icon">2</span><span>
                                                    <h6>Қадоқлашни киритиш</h6>Қадоқлаш маълумотлар
                                                </span></a></li>
                                        <li class="nav-item"><a class="nav-link <?php if($emlashProvider->getTotalCount() > 0):?>active<?php endif; ?>"><span class="num-icon">3</span><span>
                                                    <h6>Эмлаш</h6>Рўйхатни шакллантириш
                                                </span></a></li>                                        
										<li class="nav-item"><a class="nav-link <?php if(isset($vinfo->id)):?>active<?php endif; ?>"><span class="num-icon">4</span><span>
                                                    <h6>Қўлланма</h6>Қўлланма матни
                                                </span></a></li>
                                    </ul>
									
									
                                    <div class="card-body">

									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-6">
										<h2><?= Html::encode($this->title) ?></h2>
										</div>
										<div class="col-lg-2"><br/>
										  
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-6">
										<h4>Асосий маълумотлар</h4>
										</div>
										<div class="col-lg-2"><br/>
										<?php if($profiles->account_type == 10): ?>
										<?= Html::a('Маълумотларни янгилаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
										<?php endif; ?>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8">
										<?= DetailView::widget([
											'model' => $model,
											'attributes' => [
												//'id',
												'name',
												'code',
												'countries.NAME_FULL',
												'manufacturer',
												//'user',
												'created',
												//'updated',
												//'status',
											],
										]) ?>
										</div>
										<div class="col-lg-2">										
										</div>
									</div>
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-6">
										<h4>Қадоқлаш маълумотлари</h4>
										</div>
										<div class="col-lg-2"><br/>
										<?php if($profiles->account_type == 10): ?>
											<?= Html::a('Маълумотларни янгилаш', ['info', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
										<?php endif; ?>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8"><br/>
										<?= GridView::widget([
											'dataProvider' => $containerProvider,											
											'columns' => [
												['class' => 'yii\grid\SerialColumn'],
												[
													'attribute'=>'container',
													'label' => Yii::t('app', 'Идиш'),													
													'content'=>function($data){
														return $data->containern['name_uz'];
													},
												],
												[
													'attribute'=>'quantity',
													'label' => Yii::t('app', 'Идиш ҳажми'),													
													'content'=>function($data){
														if($data->unit == 1){															
															$ml = $data->quantity*$data->doztomil;
															return $data->quantity." ".$data->unitn['name_uz']." ёки ".$ml." мл.";
														}
														else{
															return $data->quantity." ".$data->unitn['name_uz'];
														}
													},
												],													
												[
													'attribute'=>'retreat',
													'label' => Yii::t('app', 'Табиий йўқотиш'),													
													'content'=>function($data){
														return $data->retreat." % ";
													},
												],												
											],
										]); ?>										
										</div>
										<div class="col-lg-2">
										
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-6">
										<h4>Эмлаш тартиби</h4>
										</div>
										<div class="col-lg-2"><br/>
										<?php if($profiles->account_type == 10): ?>
											<?= Html::a('Маълумотларни янгилаш', ['emlash', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
										<?php endif; ?>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8">
										    <?= GridView::widget([
											'dataProvider' => $emlashProvider,											
											'columns' => [
												['class' => 'yii\grid\SerialColumn'],
												[
													'attribute'=>'emlash',
													'label' => Yii::t('app', 'Эмлаш тури'),													
													'content'=>function($data){
														switch($data->emlash_type){
															case 1: return 'Мажбурий';
															break;
															case 0: return 'Профилактика';
															break;
														}
													},
												],
												[
													'attribute'=>'vac',
													'label' => Yii::t('app', 'Вакцина'),													
													'content'=>function($data){
														return $data->vaccine['name']." [".$data->vaccine['code']."]";
													},
												],	
												[
													'attribute'=>'vac',
													'label' => Yii::t('app', 'Ҳайвон'),													
													'content'=>function($data){
														return $data->animaltype['NAME_UZ'];
													},
												],
												[
													'attribute'=>'age',
													'label' => Yii::t('app', 'Эмлаш ёши'),													
													'content'=>function($data){
														if($data->ege_from == $data->ege_to){
															return $data->ege_from." ой";
														}
														else{
															return $data->ege_from." - ".$data->ege_to." ой";	
														}
														
													},
												],																														
												[
													'attribute'=>'quan',
													'label' => Yii::t('app', 'Эмлаш'),													
													'content'=>function($data){
														return $data->quantity." ".$data->unitname['name_uz'];
													},
												],
												[
													'attribute'=>'imun',
													'label' => Yii::t('app', 'Иммунитет'),																			
													'content'=>function($data){
														return "Иммунитет шаклланиши: ".$data->imun_on_date." кун <br/ > Иммунитет даври: ".$data->imun_for_time." ой";
													},
												],
												[
													'attribute'=>'imun',
													'label' => Yii::t('app', 'Ревакцинация'),																			
													'content'=>function($data){
														if($data->revacvination ==  1){															
															return $data->revac_date." кун ".$data->revac_quantity." ".$data->unitname['name_uz'];	
														}
														else{
															return "мавжуд эмас";
														}
														
													},
												],
												//'created',
												//'updated',
												//'user',
											],
										]); ?>										
										</div>
										<div class="col-lg-2">	
																				
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-6">
										<h4>Қўлланма</h4>
										</div>
										<div class="col-lg-2"><br/>
										<?php if($profiles->account_type == 10): ?>
										<?= Html::a('Маълумотларни янгилаш', ['doc', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
										<?php endif; ?>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-2">	
										</div>	
										<div class="col-lg-8" style="border:dashed 2px #ccc;">										
										    <?php if(!empty($vinfo->document)){echo $vinfo->document;} ?>										
										</div>
										<div class="col-lg-2">
										
										</div>
									</div>
    

</div>
