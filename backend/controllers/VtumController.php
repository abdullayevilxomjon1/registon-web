<?php

namespace backend\controllers;

use Yii;
use app\models\Vresp;
use app\models\Vtum;
use app\models\Vvil;
use app\models\Vuch;
use app\models\Profiles;
use app\models\VtumSearch;
use app\models\VuchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VtumController implements the CRUD actions for Vtum model.
 */
class VtumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vtum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VtumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionRecieve()
    {
        $searchModel = new VtumSearch();
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if(Null != Yii::$app->request->queryParams){
			$data = Yii::$app->request->queryParams;	
		}
		else{
			$data = array();
		}
		$data['VtumSearch']['tumsoato'] = $profiles->region;			
		$data['VtumSearch']['status'] = 1;
		
        $dataProvider = $searchModel->search($data);

        return $this->render('recieve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionInbase()
    {
        $searchModel = new VtumSearch();
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if(Null != Yii::$app->request->queryParams){
			$data = Yii::$app->request->queryParams;	
		}
		else{
			$data = array();
		}
		$data['VtumSearch']['tumsoato'] = $profiles->region;			
		$data['VtumSearch']['status'] = 2;
		
        $dataProvider = $searchModel->search($data);

        return $this->render('inbase', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


	public function actionAccept($id)
    {
		$model = Vtum::findOne($id);
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if($model->status == 1 AND $profiles->account_type == 1 AND $profiles->region == $model->tumsoato){
			$model->user_recieved = $profiles->data;
			$model->cartype = 1;
			$model->carnumber = 2;
			$model->status = 2;
			if($model->save()){
				return $this->redirect(['/vtum/view/', 'id' => $model->id]); 
			}
			else{
				return print_r($model->errors);
			}
			
		}
		else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
    }


    /**
     * Displays a single Vtum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$searchModel = new VuchSearch();
		$data = Yii::$app->request->queryParams;
		$data['VuchSearch']['vtum'] = $id;
        $dataProvider = $searchModel->search($data);
		$model = $this->findModel($id);		
		$vresp = Vresp::findOne($model->vresid);
		$uch = New Vuch();
        return $this->render('view', [
            'model' => $model,
            'uch' => $uch,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'vresp' => $vresp,
        ]);
    }

    /**
     * Creates a new Vtum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if(NULL != Yii::$app->request->post()){
			$model = new Vtum();
			$model->load(Yii::$app->request->post());
			$model->status = 1;
			$model->user = Yii::$app->user->identity->id;
			
			// Проверяем количесство прихода вакцины
			$vvil = Vvil::findOne($model->vvid);
			
			// Считаем количество прихода вакцины
			$vtum = Vtum::find()->select('SUM(vac_all) as sum')->where(['vvid' => $model->vvid])->asArray()->All();			
			
			// Проверяем 
			if($vvil->vac_all >= $vtum[0]['sum']+$model->vac_all){					
				if ($model->save()) {
					return $this->redirect(['/vvil/view', 'id' => $model->vvid]);
				}
			}
			else{
				return $this->redirect(['/vvil/view?id='.$model->vvid.'&error=1']);
			}

			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Vtum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vtum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vtum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vtum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vtum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
