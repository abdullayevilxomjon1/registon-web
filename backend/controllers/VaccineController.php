<?php

namespace backend\controllers;

use Yii;
use app\models\Vaccine;
use app\models\Vacemlash;
use app\models\Profiles;
use app\models\Vinfo;
use app\models\Vdoc;
use app\models\VaccineSearch;
use app\models\VacemlashSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * VaccineController implements the CRUD actions for Vaccine model.
 */
class VaccineController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	function adminaccess()
    {
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if($profiles->account_type != 10){
			return $this->goHome();
		}
    }

    /**
     * Lists all Vaccine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VaccineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vaccine model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		// Список фарсовок 		
		$containerquery = Vinfo::find()->where(['vid' => $id]);
        $containerProvider = new ActiveDataProvider([
			'query' => $containerquery,
		]);
		
		// Список условий 		
		$emlashquery = Vacemlash::find()->where(['vid' => $id]);
        $emlashProvider = new ActiveDataProvider([
			'query' => $emlashquery,
		]);
		
		// Текст инструкции
		$vinfo = Vdoc::find()->where(['vid' => $id])->One();
		
		
        return $this->render('view', [
            'model' => $this->findModel($id),
            'containerProvider' => $containerProvider,
            'emlashProvider' => $emlashProvider,
            'vinfo' => $vinfo,
        ]);
    }

    /**
     * Creates a new Vaccine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$this->adminaccess();
        $model = new Vaccine();
		if(NULL != \Yii::$app->request->post()){
			$model->load(\Yii::$app->request->post());
			$model->user = Yii::$app->user->identity->id;
			$model->status = 1;
			if ($model->validate()) {
			
			} else {		
				$errors = $model->errors;						
			}
			
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['info', 'id' => $model->id]);
			}		
		}		

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	
	public function actionInfo($id)
    {
		$this->adminaccess();
		// Список фарсовок 		
		$query = Vinfo::find()->where(['vid' => $id]);
        $dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
        $model = new Vinfo();
		if(NULL != \Yii::$app->request->post()){
			$model->load(\Yii::$app->request->post());
			$model->user = Yii::$app->user->identity->id;		
			$model->vid = $id;		 
			if ($model->validate()) {
			
			} else {		
				$errors = $model->errors;						
				print_r($errors);
			}
			
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['info', 'id' => $id]);
			}
		}

        return $this->render('infoc', [
            'model' => $model,
            'data' => $dataProvider,
            'id' => $id,
        ]);
    }
	
	public function actionEmlash($id)
    {
		$this->adminaccess();
		// Список условий 		
		$query = Vacemlash::find()->where(['vid' => $id]);
        $dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

        	
        $model = new Vacemlash();
		$model->user = Yii::$app->user->identity->id;		
		$model->vid = $id;
		
		if(NULL != \Yii::$app->request->post()){
			$dataform = Yii::$app->request->post();
			if(isset($dataform['Vacemlash']['emlash_period'])){
				$dataform['Vacemlash']['emlash_period'] = implode(",", $dataform['Vacemlash']['emlash_period']);			
			}		
			$model->load($dataform);
		
			if ($model->validate()) {
			
			} else {		
				$errors = $model->errors;						
				print_r($errors);
			}
		
		
			if ($model->load($dataform) && $model->save()) {
				return $this->redirect(['emlash', 'id' => $id]);
			}
		}	

			

        return $this->render('emlash', [
            'model' => $model,
            'data' => $dataProvider,
            'id' => $id,
        ]);
    }
	
	public function actionDoc($id)
    {
		$this->adminaccess();
		$model = Vdoc::find()->where(['vid' => $id])->One();		
        if(empty($model->id)){
			$model = new Vdoc();
		}		
        
		$model->vid = $id;
		$model->status = 1;
				
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$vaccine = Vaccine::findOne($id);
			$vaccine->status = 0;
			if($vaccine->save()){
				return $this->redirect(['view', 'id' => $id]);				
			}
			
		}

        return $this->render('vdoc', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vaccine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vaccine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vaccine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vaccine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vaccine::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
