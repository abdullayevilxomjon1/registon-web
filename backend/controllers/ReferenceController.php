<?php

namespace backend\controllers;

use Yii;
use app\models\Regions;
use app\models\Uchastkanames;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl; 
use yii\helpers\Json; 


class ReferenceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['search', 'getfizdata', 'reg', 'uchastka'],
                        'allow' => true,
						'roles' => ['?'],
                    ],
                    [
                        'actions' => ['bank', 'reg', 'getfizdata', 'uchastka'],  
                        'allow' => true, 
                        'roles' => ['@'],
                    ]					
                ],					
            ],
		];
    }
		
	// Справочник ПИНФЛ
	public function actionGetfizdata()
    {
		$request = Yii::$app->request; 		
		
		if ($request->isAjax) { 
			$pinfl =  Yii::$app->request->get('pinfl'); 
			$serialnum =  Yii::$app->request->get('serial'); 
			$url = 'http://hamsa.vetgov.uz/registon/getfizinfo/1';		
			$token = '3l5xoWWbLihIexBgJrpRnAyLxgAmqgT09';
			$genurl = $url.'?pinfl='.$pinfl.'&token='.$token.'&document='.$serialnum;
			$fizinfo = file_get_contents($genurl, true);					
			$data = Json::decode($fizinfo);
			
			if($data['code']['result'] == 2200){
				return Json::encode($data); 
			}
			else{
				return "";
			}
			
			
		}
		else{
			$pinfl =  Yii::$app->request->get('pinfl'); 
			$serialnum =  Yii::$app->request->get('serial'); 
			$url = 'http://hamsa.vetgov.uz/registon/getfizinfo/1';		
			$token = '3l5xoWWbLihIexBgJrpRnAyLxgAmqgT09';
			$genurl = $url.'?pinfl='.$pinfl.'&token='.$token.'&document='.$serialnum;
			$fizinfo = file_get_contents($genurl, true);					
			$data = Json::decode($fizinfo);
			
			if($data['code']['result'] == 2200){
				
			}
			else{
				$data = "";
			}
			
			// тут начинаем проверку по требованиям субсидии 
			// если отвечает всем требование то status = 1 
			// ELSE status=0. msg = почсему не разрешаем
			// тут мы по дефолту определяем что все параметры пройдены. при проверка перебиваем и указываем причину.
			//print_r($data);
			return Json::encode($data); 
		}						
	}
	
	// Центральный справочник для регионов
	 
	public function actionReg($id)
    {	
		$Reg =  Regions::find()->where(['between', 'code', $id."201", $id."999"])->andWhere(['!=', 'code', $id."400"])->asArray()->All();  			 
		    $data ="";
			$data = $data.'<option value="0">Туманни танланг</option>';
			foreach($Reg as $val){
				$data = $data.'<option value="'.$val['code'].'">'.$val['Name'].'</option>';
			}
			
		return $data;
    }
	
	public function actionUchastka($id)
    {	
		$Uchastka =  Uchastkanames::find()->where(['soato' => $id])->asArray()->All();  			 
		    $data ="";
			$data = $data.'<option value="0">Участкани танланг</option>';
			foreach($Uchastka as $val){
				$data = $data.'<option value="'.$val['id'].'">'.$val['NAME_UZ'].'</option>';
			}
			
		return $data;
    }

}
