<?php

namespace backend\controllers;

use Yii;
use app\models\Vvil;
use app\models\Vtum;
use app\models\Vresp;
use app\models\VvilSearch;
use app\models\VtumSearch;
use app\models\Profiles;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VvilController implements the CRUD actions for Vvil model.
 */
class VvilController extends Controller
{
    /** 
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vvil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VvilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionRecieve()
    {
        $searchModel = new VvilSearch();		
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if(Null != Yii::$app->request->queryParams){
			$data = Yii::$app->request->queryParams;	
		}
		else{
			$data = array();
		}
		
		$data['VvilSearch']['vilsoato'] = $profiles->vil;			
		$data['VvilSearch']['status'] = 1;
		
        $dataProvider = $searchModel->search($data);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionInbase()
    {
        $searchModel = new VvilSearch();		
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if(Null != Yii::$app->request->queryParams){
			$data = Yii::$app->request->queryParams;	
		}
		else{
			$data = array();
		}
		
		$data['VvilSearch']['vilsoato'] = $profiles->vil;			
		$data['VvilSearch']['status'] = 2;
		
        $dataProvider = $searchModel->search($data);

        return $this->render('inbase', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionAccept($id)
    {
		$model = Vvil::findOne($id);
		$profiles = Profiles::findOne(Yii::$app->user->identity->id);
		if($model->status == 1 AND $profiles->account_type == 1 AND $profiles->vil == $model->vilsoato){
			$model->user_recieved = $profiles->data;
			$model->cartype = 1;
			$model->carnumber = 2;
			$model->status = 2;
			if($model->save()){
				return $this->redirect(['/vvil/view/', 'id' => $model->id]); 
			}
			else{
				return print_r($model->errors);
			}
			
		}
		else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
    }

    /**
     * Displays a single Vvil model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$searchModel = new VtumSearch();
		$data = Yii::$app->request->queryParams;		
		$data['VtumSearch']['vvid'] = $id;
        $dataProvider = $searchModel->search($data);  
		$model = $this->findModel($id);
		$vresp = Vresp::findOne($model->vresid);		
		$tum = New Vtum();
		
        return $this->render('view', [
            'model' => $model,
            'tum' => $tum,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'vresp' => $vresp,
        ]);
    }

    /**
     * Creates a new Vvil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if(NULL != Yii::$app->request->post()){
			$model = new Vvil();
            $model->load(Yii::$app->request->post());
			$model->status = 1;
			$model->user = Yii::$app->user->identity->id;
			// Проверяем количесство прихода вакцины
			$vresp = Vresp::findOne($model->vresid);
			
			// Считаем количество прихода вакцины
			$rasp = Vvil::find()->select('SUM(vac_all) as sum')->where(['vresid' => $model->vresid])->asArray()->All();
			
			// Проверяем 
			if($vresp->vac_all >= $rasp[0]['sum']+$model->vac_all){
				if($model->save()) {
					return $this->redirect(['/vresp/view', 'id' => $model->vresid]);
				}	
			}
			else{
				return $this->redirect(['/vresp/view?id='.$model->vresid.'&error=1']);
			}
			

			return $this->render('create', [
				'model' => $model,
			]);		
		
		}
        
    }

    /**
     * Updates an existing Vvil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vvil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vvil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vvil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vvil::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
