<?php

namespace backend\controllers;

use Yii;
use app\models\Vresp;
use app\models\Vinfo; 
use app\models\Vvil; 
use app\models\VrespSearch;
use app\models\VvilSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VrespController implements the CRUD actions for Vresp model.
 */
class VrespController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vresp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VrespSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
		$model = new Vresp();
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Vresp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$vvil = new Vvil();
		$searchModel = new VvilSearch();
		$data = Yii::$app->request->queryParams;		
		$data['VvilSearch']['vresid'] = $id;
        $dataProvider = $searchModel->search($data);
		
		
		
        return $this->render('view', [
            'model' => $this->findModel($id),
            'vvil' => $vvil,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Vresp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vresp();		
		if(NULL != Yii::$app->request->post()){
			$data = Yii::$app->request->post();			
			//$serial = implode(",", $data['Vresp']['serial']);
			$serial = json_encode($data['Vresp']['serial']);
			$data['Vresp']['serial'] = $serial;									
			$model->load($data);
			$model->user = Yii::$app->user->identity->id;
			$model->status = 1;			
			if ($model->save()) {
				return $this->redirect(['index']);
			}
			else{
				print_r($model->errors);
			}			
		}
        

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vresp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	
	public function actionVacemlash($id)
    {
	   $this->enableCsrfValidation = false;
	   
	   if($id > 0){ 
			$countVac = Vinfo::find()
			  ->where(['vid' => $id])
			  ->count();
			$emlash = Vinfo::find()
				  ->where(['vid' => $id])
				  ->all();  
				  
			if($countVac > 0) {			
				echo "<option value=''>Қадоқни танланг</option>";
				foreach ($emlash as $tema) {
				echo "<option value='".$tema->id."'>".$tema->containern['name_uz'].' '.$tema->quantity.' '.$tema->unitn['name_uz']."</option>";	
				}			
			}			
	   }
	   else{
		   echo "<option value=''>-</option>";
	   }
		
		
    }

    /**
     * Deletes an existing Vresp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vresp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vresp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vresp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
