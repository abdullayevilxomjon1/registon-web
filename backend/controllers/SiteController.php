<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\SignupForm;
use backend\models\ResendVerificationEmailForm;
use backend\models\VerifyEmailForm;
use app\models\Profiles;
use app\models\Vuch;
use app\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'signup', 'checklogin'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction( $action ) {
		if ( parent::beforeAction ( $action ) ) {

			 //change layout for error action after 
			 //checking for the error action name 
			 //so that the layout is set for errors only
			if ( $action->id == 'error' ) {
				$this->layout = 'error';
			}
			return true;
		} 
	}

	public function actionChecklogin()	
    {	
        if(null !== Yii::$app->request->get('username')){			
			$user = User::find()->where(['username' => Yii::$app->request->get('username')])->asArray()->COUNT();									
		}
		elseif(null !== Yii::$app->request->get('email')){			
			$user = User::find()->where(['email' => Yii::$app->request->get('email')])->asArray()->COUNT();									
		}
		
	  
			  
		if($user == 1){
			if(null !== Yii::$app->request->get('username')){			
				return 	"Ушбу логин тизимда мавжуд. Бошка логинни танланг";  
			}
			elseif(null !== Yii::$app->request->get('email')){			
				return 	"Ушбу электрон манзил тизимда мавжуд. Бошка электрон манзилни танланг";  
			}
		}
		else{
			return 	1;  
		}
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionSignup()
    {
		if(Yii::$app->user->getIsGuest()){
			$this->layout = "blank";
			$model = new SignupForm();
			$profiles = new Profiles();
			if($model->load(Yii::$app->request->post()) AND $profiles->load(Yii::$app->request->post())) {
				if ($user = $model->signup()) {				
					$profiles->id = $user->id;
					if($profiles->save()){
						if (Yii::$app->getUser()->login($user)) {
							return $this->goHome();
						}
					}
					else{
						return print_r($profiles);
					}					
				}
			}

			return $this->render('signup', [
				'model' => $model,
				'profiles' => $profiles,
			]);
		}
		else{
			return $this->goHome();
		}
    }
	
	

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
