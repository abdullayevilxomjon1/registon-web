<?php

namespace backend\controllers;

use Yii;
use app\models\Vuch;
use app\models\Vtum;
use app\models\VuchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VuchController implements the CRUD actions for Vuch model.
 */
class VuchController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vuch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VuchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vuch model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vuch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Vuch();
		$model->load(Yii::$app->request->post());
		$model->status = 1;
		$model->user = Yii::$app->user->identity->id;
        
		// Проверяем количесство прихода вакцины
		$vtum = Vtum::findOne($model->vtum);
		
		// Считаем количество прихода вакцины
		$vuch = Vuch::find()->select('SUM(vac_all) as sum')->where(['vtum' => $model->vtum])->asArray()->All();		
		// Проверяем 
		if($vtum->vac_all >= $vuch[0]['sum']+$model->vac_all){	
			if ($model->save()) {
				return $this->redirect(['/vtum/view', 'id' => $model->vtum]);
			}
		}
		else{
			return $this->redirect(['/vtum/view?id='.$model->vtum.'&error=1']);
		}		

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vuch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vuch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vuch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vuch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vuch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
